package com.megatron.findjobmylife;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.megatron.findjobmylife.db.utils.DbUtils;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

public class App extends Application {
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        context = getApplicationContext();
        Realm.init(context);

        DbUtils.migrate();
    }

    public static Context getContext() {
        return context;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
}
