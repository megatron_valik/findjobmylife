package com.megatron.findjobmylife;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.megatron.findjobmylife.classes.RegionManager;
import com.megatron.findjobmylife.classes.VacancyManager;
import com.megatron.findjobmylife.dialogs.UpdateInfoDialogHelper;
import com.megatron.findjobmylife.rating.RatingHelper;
import com.megatron.findjobmylife.utils.ToastHelper;

import org.jetbrains.annotations.NotNull;

public class MainActivity extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupWithNavController((NavigationView) findViewById(R.id.nav_view), navController);
        NavigationUI.setupActionBarWithNavController(this, navController, drawer);

        RegionManager.getInstance(this);
        VacancyManager.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UpdateInfoDialogHelper(this).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                new RatingHelper(this, MainActivity.super::onBackPressed).show();
                return;
            } else {
                if (navController.navigateUp()) {
                    return;
                }
            }
            this.doubleBackToExitPressedOnce = true;
            ToastHelper.show(findViewById(R.id.nav_host_fragment), getString(R.string.double_exit_msg));
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 1000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (navController.navigateUp()) {
                return true;
            }
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        RegionManager.onDestroy();
        VacancyManager.onDestroy();
        super.onDestroy();
    }

}