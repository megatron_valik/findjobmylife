package com.megatron.findjobmylife.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.rating.RatingHelper;
import com.megatron.findjobmylife.utils.ToastHelper;

import java.util.Objects;

public class MailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
        }
        AppCompatEditText email_comment = findViewById(R.id.et_email_comment);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> sendEmail(Objects.requireNonNull(email_comment.getText()).toString()));

        FloatingActionButton fabPM = findViewById(R.id.fabPlayMarket);
        fabPM.setOnClickListener(v -> sendRating());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    private void sendEmail(String comment) {
        if (isEmpty(comment)) {
            ToastHelper.show(getString(R.string.empty_comment_toast));
            return;
        }

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "dev.megatron@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mail_ocenka_app));
        emailIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.good_day_body) + "\n" +
                comment
        );
        startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.send_mail_title)));

        try {
            startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean isEmpty(String str) {
        return !(str != null && !str.trim().equals(""));
    }

    private void sendRating() {
        RatingHelper.openPlayStore(this);
    }

}
