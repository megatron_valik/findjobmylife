package com.megatron.findjobmylife.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.megatron.findjobmylife.MainActivity;
import com.megatron.findjobmylife.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();
        animate(findViewById(R.id.finger), R.anim.slide0, false);
        animate(findViewById(R.id.im1), R.anim.slide1, false);
        animate(findViewById(R.id.im2), R.anim.slide2, false);
        animate(findViewById(R.id.im3), R.anim.slide3, false);
        animate(findViewById(R.id.im4), R.anim.slide4, false);
        animate(findViewById(R.id.im5), R.anim.slide5, true);
    }

    private void animate(AppCompatImageView im, int resource, boolean addListener) {
        Animation animation = AnimationUtils.loadAnimation(this, resource);
        im.startAnimation(animation);
        if (addListener) {
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                    }, 10);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        // запрещаем закрыть активити
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}
