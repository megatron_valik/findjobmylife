package com.megatron.findjobmylife.activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.databinding.ActivityVacancyBinding;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.dialogs.PhoneDialogHelper;
import com.megatron.findjobmylife.models.DownloadModel;
import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserFabric;
import com.megatron.findjobmylife.ui.HtmlTextView;
import com.megatron.findjobmylife.utils.ToastHelper;
import com.megatron.findjobmylife.utils.Utils;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class VacancyActivity extends AppCompatActivity implements HtmlTextView.HtmlTextViewCallListener {
    private ActivityVacancyBinding binding;
    private Disposable fabricDisposable;
    private DownloadModel downloadModel = new DownloadModel();

    private String uri;
    private int index;// индекс сайта на котором ищем полное описание вакансии
    private boolean isFavorites; // флаг избранного
    private boolean isChangeFavorite; // изменял ли пользователь избранное
    private int positionVacancy = -1; // позиция вакансии в списке
    private String phone = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_vacancy);

        try {
            ActionBar bar = getSupportActionBar();
            if (bar != null) {
                bar.setDisplayHomeAsUpEnabled(true);
                bar.setDisplayShowHomeEnabled(true);
            }
        } catch (NullPointerException e) {
            Utils.logException(e);
        } catch (Exception e) {
            Utils.logException(e);
        }
        findViewById(R.id.btnSearchRepeat).setOnClickListener(view -> parse(uri, index));

        try {
            HtmlTextView textView = binding.getRoot().findViewById(R.id.htmlVacancy);
            textView.setCallListener(this);
        } catch (Exception e) {
            Utils.logException(e);
        }

        Intent intent = getIntent();
        if (intent != null) {
            try {
                uri = intent.getStringExtra("URL");
                index = intent.getIntExtra("INDEX", -1);
                positionVacancy = intent.getIntExtra("POSITION", -1);
                isFavorites = isChangeFavorite = intent.getBooleanExtra("FAVORITE", false);

                parse(uri, index);
            } catch (Exception e) {
                Utils.logException(e);
            }
        } else {
            try {
                Utils.logException(new Exception("VacancyActivity intent is null"));
            } catch (Exception e) {
                Utils.logException(e);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_send_resume, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.favorites_action).setIcon(isChangeFavorite ?
                R.drawable.ic_star :
                R.drawable.ic_star_border);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String mUrl = "";

        VacancyModel model = binding.getVacancy();
        if (model != null && !model.getUrl().equals("")) {
            String url = model.getUrl();
            if (url.endsWith("=")) return false;
            mUrl = url;
        }

        int id = item.getItemId();
        //*** FAVORITE ADD/REMOVE
        if (id == R.id.favorites_action) {
            isChangeFavorite = !isChangeFavorite;
            invalidateOptionsMenu();
            return true;
        }

        //*****
        if (mUrl.equals("")) return false;

        if (id == R.id.action_send_resume) {
            sendResume(mUrl);
            return true;
        }
        if (id == R.id.action_content_copy) {
            copyUrl(uri);
            return true;
        }
        if (id == R.id.action_remove_vacancy) {
            removeVacancy(uri);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void copyUrl(String mUrl) {
        try {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("URL", mUrl);
            clipboard.setPrimaryClip(clip);
            ToastHelper.show(getWindow().getDecorView(), getString(R.string.toast_copy_content));
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (fabricDisposable != null) {
            fabricDisposable.dispose();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("POSITION", positionVacancy);
        intent.putExtra("FAVORITE", isChangeFavorite);
        // если пользователь изменял (добавил / убрал) избранное
        setResult(isChangeFavorite == isFavorites ? RESULT_CANCELED : RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.close_alpha);
    }

    private void parse(String uri, int index) {
        if (uri == null || uri.trim().equals("") || index < 0)
            return;

        new ParserFabric().readVacancy(index, uri)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<VacancyModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        fabricDisposable = d;
                        downloadModel.setDownloading(true);
                        downloadModel.setEmptyShow(false);
                        binding.setDownload(downloadModel);
                    }

                    @Override
                    public void onNext(VacancyModel vacancyModel) {
                        downloadModel.setDownloading(false);
                        downloadModel.setEmptyShow(false);
                        binding.setDownload(downloadModel);
                        set(vacancyModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        downloadModel.setDownloading(false);
                        downloadModel.setEmptyShow(binding.getVacancy() == null);
                        binding.setDownload(downloadModel);
                        fabricDisposable = null;
                    }

                    @Override
                    public void onComplete() {
                        downloadModel.setDownloading(false);
                        downloadModel.setEmptyShow(binding.getVacancy() == null);
                        binding.setDownload(downloadModel);
                        fabricDisposable = null;
                    }
                });
    }

    private void set(VacancyModel model) {
        if (binding != null)
            binding.setVacancy(model);
    }

    private void sendResume(String url) {
        try {
            //Log.e("Vacancy", uri);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            tryWeb(url);
        } catch (Exception e) {
            tryWeb(url);
        }
    }

    private void tryWeb(String url) {
        try {
            Log.e("Vacancy", url);
            Uri webPage = Uri.parse(url);
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                webPage = Uri.parse("https://" + url);
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, webPage);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else ToastHelper.show(getString(R.string.error_open_http));
        } catch (Exception ex) {
            Utils.logException(ex);
            ToastHelper.show(getString(R.string.error_open_http));
        }
    }

    private void removeVacancy(String url) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_remove_vacancy)
                .setMessage(R.string.message_remove_vacancy)
                .setPositiveButton(R.string.ok_btn, (dialogInterface, i) -> {
                    removeOnClose(url);
                    dialogInterface.dismiss();
                })
                .setNegativeButton(R.string.cancel_btn, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void removeOnClose(String url) {
        DbUtils.addRemovedLink(index, url);

        Intent intent = new Intent();
        intent.putExtra("POSITION", positionVacancy);
        intent.putExtra("REMOVE", true);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Utils.PERMISSION_REQUEST) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    ToastHelper.show(getString(R.string.call_permission));
                    return;
                }
            }
        }
        if (phone != null && !phone.equals("")) {
//            VacancyActivity.this.runOnUiThread(() -> );
            phoneHandler.sendEmptyMessage(0);
        }
    }

    @Override
    public void onCall(String phone) {
        this.phone = phone;
    }

    @SuppressLint("HandlerLeak")
    private Handler phoneHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            new PhoneDialogHelper(phone, VacancyActivity.this, null).show();
        }
    };
}
