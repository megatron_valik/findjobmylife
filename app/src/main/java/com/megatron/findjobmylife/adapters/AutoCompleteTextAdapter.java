package com.megatron.findjobmylife.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.widget.ArrayAdapter;

import com.megatron.findjobmylife.classes.RegionManager;
import com.megatron.findjobmylife.models.RegionModel;

import java.util.ArrayList;

public class AutoCompleteTextAdapter extends ArrayAdapter<String> {
    private ArrayList<String> city = new ArrayList<>();
    private ArrayList<RegionModel> citysModel;
    private RegionManager manager;
    private int selectedItem = -1;

    public AutoCompleteTextAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        manager = RegionManager.getInstance(context);
        citysModel = manager.getEmptySearch();
    }

    //***********************************************************

    @Override
    public int getCount() {
        return city.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        try {
            return city.get(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private RegionModel getRegionModelSelected(int pos) {
        if (pos < 0 || pos >= citysModel.size())
            return null;
        return citysModel.get(pos);
    }

    public void search(String query) {
        setDataSource(manager.get(query));
    }

    private void setDataSource(RegionManager.RegionSearchResult source) {
        selectedItem = -1;
        city = source.getRegionCityResult();
        citysModel = source.getRegionModelsResult();
        notifyDataSetChanged();
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }

    public RegionModel getRegionFromPosition(String city) {
        RegionModel selectedModel = getRegionModelSelected(selectedItem);
        if (selectedModel == null) {
            search(city);
            selectedModel = getRegionModelSelected(0);
        }

        return selectedModel;
    }
}
