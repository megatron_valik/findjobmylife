package com.megatron.findjobmylife.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.megatron.findjobmylife.classes.VacancyManager;
import com.megatron.findjobmylife.db.models.FastVacancySearch;
import com.megatron.findjobmylife.db.utils.DbUtils;

import java.util.ArrayList;

public class AutoCompleteTextVacancyAdapter extends ArrayAdapter<String> {
    private ArrayList<String> vacancys = new ArrayList<>();
    private VacancyManager manager = VacancyManager.getInstance();

    public AutoCompleteTextVacancyAdapter(@NonNull Context context, int resource) {
        super(context, resource);
//        manager = VacancyManager.getInstance();
    }

    //***********************************************************

    @Override
    public int getCount() {
        return vacancys.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        try {
            return vacancys.get(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void search(String query) {
        setDataSource(manager.get(query));
    }

    private void setDataSource(ArrayList<String> source) {
        vacancys = source;
        notifyDataSetChanged();
    }

    //***********
    public boolean showFastVacancy() {
        ArrayList<FastVacancySearch> fast = DbUtils.getFastSearch();
        if (fast.size() >= 2) {
            vacancys.clear();
            for (FastVacancySearch search : fast) {
                vacancys.add(search.getVacancy());
            }
        }
        return fast.size() >= 2;
    }
}
