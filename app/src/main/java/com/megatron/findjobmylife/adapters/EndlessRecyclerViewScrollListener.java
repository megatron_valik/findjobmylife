package com.megatron.findjobmylife.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.widget.AbsListView;

public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener {
    private boolean isNetworkConnect = true;

    public void setNetworkConnect(boolean networkConnect) {
        isNetworkConnect = networkConnect;
        ethernetStatus(networkConnect);
    }

    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        StaggeredGridLayoutManager linearLayoutManager = (StaggeredGridLayoutManager) recyclerView.getLayoutManager();
        assert linearLayoutManager != null;
        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = linearLayoutManager.getItemCount();
        int[] firstVisibleItems = linearLayoutManager.findFirstVisibleItemPositions(null);
        int pastVisibleItems = 0;
        if (firstVisibleItems != null && firstVisibleItems.length > 0) {
            pastVisibleItems = firstVisibleItems[0];
        }

        boolean loading = isLoading();
        if (!loading && isDownloadMore() && (totalItemCount - visibleItemCount) <= (pastVisibleItems + 2)) {
            if (isNetworkConnect)
                loadNextPage();
            else {
                ethernetStatus(false);
            }
        }
    }

    @Override
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        switch (newState) {
            case AbsListView.OnScrollListener.SCROLL_STATE_IDLE: // 0
                break;
            case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL://1
                break;
            case AbsListView.OnScrollListener.SCROLL_STATE_FLING://2
                break;
        }

    }

    public abstract void loadNextPage();

    public abstract boolean isDownloadMore();

    public abstract boolean isLoading();

    public abstract void ethernetStatus(boolean isNetworkConnect);

}