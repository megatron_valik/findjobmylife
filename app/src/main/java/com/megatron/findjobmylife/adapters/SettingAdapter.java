package com.megatron.findjobmylife.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.megatron.findjobmylife.fragments.setting.HhUaSettingFragment;
import com.megatron.findjobmylife.fragments.setting.JobsUaSettingFragment;
import com.megatron.findjobmylife.fragments.setting.OlxUaSettingFragment;
import com.megatron.findjobmylife.fragments.setting.RabotaUaSettingFragment;
import com.megatron.findjobmylife.fragments.setting.WorkUaSettingFragment;

public class SettingAdapter extends FragmentStatePagerAdapter {
    private Fragment[] settingTabs = {new WorkUaSettingFragment(), new RabotaUaSettingFragment(),
            new HhUaSettingFragment(), new OlxUaSettingFragment(), new JobsUaSettingFragment()};

    public SettingAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int i) {
        return settingTabs[i];
    }

    @Override
    public int getCount() {
        return 5;
    }

}
