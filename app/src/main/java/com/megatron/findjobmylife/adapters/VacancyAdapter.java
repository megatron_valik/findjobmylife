package com.megatron.findjobmylife.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.megatron.findjobmylife.App;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.activity.VacancyActivity;
import com.megatron.findjobmylife.databinding.ItemVacancyBinding;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.fragments.AbstractUaFragment;
import com.megatron.findjobmylife.models.InterfaceVacancyModel;
import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserFabric;
import com.megatron.findjobmylife.utils.ToastHelper;
import com.megatron.findjobmylife.utils.Utils;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class VacancyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//    private final static String TAG = "VAdapter";

    public interface DownloadAnimateListener {
        void onStartDownload();

        void onCompleteDownload();

        void onDestroy();
    }

    public interface SwipeDownloadListener {
        void onCompleteSwipe(boolean isSuccess);
    }

    private int page = 1;
    private boolean isActiveTab = false;
    private Context context;
    private int indexJobUa = -1;
    private ArrayList<InterfaceVacancyModel> vacancyModels = new ArrayList<>();
    private ParserFabric fabric = new ParserFabric();
    private int indexParser = 0;
    private boolean isLoadingVacancy = false;
    private boolean isDownloadingMore = true;
    private boolean isNetworkConnect = true;
    boolean isRemoved = false; // все вакансии страницы были удалены
    private DownloadAnimateListener animateListener;
    private SwipeDownloadListener swipeDownloadListener;
    private Disposable fabricDisposable = null;
    private AbstractUaFragment uaFragment;
    private int lastAnimatedPosition = 0;
    private boolean animate = Utils.stringToBool(Utils.readPreference(Utils.ANIMATE_LIST), true);

    private String toastTxtTemp = "";

    public VacancyAdapter(Context context, DownloadAnimateListener animateListener) {
        this.context = context;
        this.animateListener = animateListener;
    }

    public VacancyAdapter set(String vacancy, String city, String idCity, int indexParser) {
        fabric.set(vacancy, city, idCity);
        this.indexParser = indexParser;
        return this;
    }

    public VacancyAdapter setParams(String params) {
        fabric.setParameter(params);
        return this;
    }

    public VacancyAdapter setIndexUa(int index) {
        indexJobUa = index;
        return this;
    }

    public VacancyAdapter setUaFragment(AbstractUaFragment fragment) {
        uaFragment = fragment;
        return this;
    }

    public void setAnimateListener(DownloadAnimateListener animateListener) {
        this.animateListener = animateListener;
    }

    public EndlessRecyclerViewScrollListener setScrollListener() {
        return new EndlessRecyclerViewScrollListener() {
            @Override
            public void loadNextPage() {
                isNetworkConnect = true;
                if (isLoadingVacancy) return;
                search(page);
            }

            @Override
            public boolean isDownloadMore() {
                return isDownloadingMore;
            }

            @Override
            public boolean isLoading() {
                return isLoadingVacancy;
            }

            @Override
            public void ethernetStatus(boolean isNetwork) {
                isNetworkConnect = isNetwork;
            }
        };
    }

    public void search() {
        if (page <= 1) {
            page = 1;
            search(0);
        } else
            search(--page);
    }

    public void search(int indexPage) {
//        Log.v("adapter", indexPage + "");
        if (isLoadingVacancy || !isNetworkConnect) {
            if (swipeDownloadListener != null)
                swipeDownloadListener.onCompleteSwipe(false);
            if (animateListener != null)
                animateListener.onCompleteDownload();
            return;
        }
        int currentSize = vacancyModels.size();
        isRemoved = false;
        fabric.readVacancy(indexParser, indexPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<VacancyModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        //Log.e(TAG, "onSubscribe");
                        fabricDisposable = d;
                        isLoadingVacancy = true;
                        if (animateListener != null)
                            animateListener.onStartDownload();
                    }

                    @Override
                    public void onNext(ArrayList<VacancyModel> vm) {
                        //Log.e(TAG, "onNext");
                        if (vm == null) {
                            isDownloadingMore = false;
                        } else {
                            page++;
                            boolean isAddedVacancyToList = false; // добавили вакансии в список без дубликатов

                            if (vm.size() > 0)
                                for (VacancyModel m : vm) {
                                    if (DbUtils.isRemovedLink(indexParser, m.getUrl())) {
                                        isRemoved = true;
                                        continue;
                                    }
                                    if (!vacancyModels.contains(m)) {
                                        isAddedVacancyToList = true;
                                        m.setPreviewVacancy(DbUtils.isPreLink(m.getUrl()));
                                        // простой поиск
                                        if (swipeDownloadListener == null) {
                                            vacancyModels.add(m);
                                        } else {
                                            // свайп сверху вниз - поиск обновляем
                                            vacancyModels.add(0, m);
                                        }
                                    }
                                }

                            // загрузку продолжаем если это не свайп сверху
                            if (swipeDownloadListener == null) {
                                isDownloadingMore = isAddedVacancyToList;
                                if (!isDownloadingMore)
                                    if (isActiveTab)
                                        ToastHelper.show(((Activity) context).getWindow().getDecorView(), App.getContext().getString(R.string.no_more_vacancy_msg) + " " + ParserFabric.getNameUa(indexJobUa));
                                    else
                                        toastTxtTemp = (App.getContext().getString(R.string.no_more_vacancy_msg) + " " + ParserFabric.getNameUa(indexJobUa));
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            //Log.e(TAG, "onError");
                            isLoadingVacancy = false;
                            if (!(e instanceof Exception)) {// нет больше вакансий
                                isDownloadingMore = false;
                                if (isActiveTab)
                                    ToastHelper.show(((Activity) context).findViewById(R.id.nav_host_fragment), App.getContext().getString(R.string.no_more_vacancy_msg) + " " + ParserFabric.getNameUa(indexJobUa));
                                else
                                    toastTxtTemp = (App.getContext().getString(R.string.no_more_vacancy_msg) + " " + ParserFabric.getNameUa(indexJobUa));
                            }
                            fabricDisposable = null;
                        } catch (Exception ex) {
                            //Log.e(TAG, "onError2");
                        } catch (OutOfMemoryError ex) {
                            e.printStackTrace();
                        } finally {
                            if (animateListener != null)
                                animateListener.onCompleteDownload();
                            if (swipeDownloadListener != null)
                                swipeDownloadListener.onCompleteSwipe(false);
                        }
                    }

                    @Override
                    public void onComplete() {
                        //Log.e(TAG, "onComplete");
                        isLoadingVacancy = false;
                        fabricDisposable = null;
                        int itemCount = vacancyModels.size() - currentSize;

                        // удалили все вакансии из списка и нет добавляемых
                        if (isRemoved && itemCount == 0) {
                            if (swipeDownloadListener != null) {
                                page--;
                                swipeDownloadListener.onCompleteSwipe(true);
                                if (animateListener != null)
                                    animateListener.onCompleteDownload();
                            } else
                                search(page);
//                            Utils.logException(new Exception("Vacancy " + page));
                            return;
                        }

                        if (swipeDownloadListener == null) {
                            notifyItemRangeInserted(currentSize, itemCount);
                        } else {
                            notifyItemRangeInserted(0, itemCount);
                            swipeDownloadListener.onCompleteSwipe(true);
                        }
                        if (animateListener != null)
                            animateListener.onCompleteDownload();
                    }
                });
    }

    public boolean isLoadingVacancy() {
        return isLoadingVacancy;
    }

    public void setActiveTab(boolean activeTab) {
        isActiveTab = activeTab;
    }

    public String getToast() {
        String tmp = toastTxtTemp;
        toastTxtTemp = "";
        return tmp;
    }

    public void setSwipeDownloadListener(SwipeDownloadListener swipeDownloadListener) {
        this.swipeDownloadListener = swipeDownloadListener;
    }
    //************************************************************************************

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ItemVacancyBinding binding = ItemVacancyBinding.inflate(inflater, viewGroup, false);
        return new VacancyHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        VacancyHolder holder = (VacancyHolder) viewHolder;
        holder.build((VacancyModel) vacancyModels.get(i), i);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        try {
            VacancyHolder mHolder = (VacancyHolder) holder;
            int pos = holder.getLayoutPosition();
            mHolder.clearImage((VacancyModel) vacancyModels.get(pos));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return vacancyModels.size();
    }

    public void onDestroy() {
        isLoadingVacancy = false;
        animateListener = null;
        if (fabricDisposable != null && !fabricDisposable.isDisposed())
            fabricDisposable.dispose();
    }

    public void onPause() {
        isLoadingVacancy = false;

        if (animateListener != null)
            animateListener.onDestroy();
        if (swipeDownloadListener != null)
            swipeDownloadListener.onCompleteSwipe(false);

        animateListener = null;
        if (fabricDisposable != null && !fabricDisposable.isDisposed())
            fabricDisposable.dispose();
    }

    @BindingAdapter("android:src")
    public static void setImageBitmap(ImageView view, Bitmap bitmap) {
        view.setImageBitmap(bitmap);
    }

    @BindingAdapter("android:src")
    public static void setImageFavorite(AppCompatImageView view, boolean isFavorite) {
        if (isFavorite)
            view.setImageResource(R.drawable.ic_favorites_status);
        else view.setImageBitmap(null);
    }

    class VacancyHolder extends RecyclerView.ViewHolder {
        private ItemVacancyBinding binding;

        VacancyHolder(ItemVacancyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.executePendingBindings();
        }

        void build(VacancyModel vacancyModel, int position) {
            binding.setVacancy(vacancyModel);

            binding.getRoot().setOnClickListener(view -> showFullVacancy(vacancyModel, position));
            try {
                if (position > lastAnimatedPosition && animate) {
                    lastAnimatedPosition = position;
                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.from_bottom);
                    animation.setInterpolator(new AccelerateDecelerateInterpolator());
                    binding.getRoot().setAnimation(animation);
                    animation.start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        void clearImage(VacancyModel vacancyModel) {
            vacancyModel.clearImage();
        }

        private void showFullVacancy(VacancyModel model, int position) {
            if (!model.isPreviewVacancy()) {
                model.setPreviewVacancy(true);
                DbUtils.addLink(model.getUrl());
            }
            try {
                Intent intent = new Intent(context, VacancyActivity.class);
                intent.putExtra("INDEX", indexParser);
                intent.putExtra("URL", model.getUrl());
                //***
                intent.putExtra("POSITION", position);
                intent.putExtra("FAVORITE", model.isFavorite());

                if (uaFragment != null) {
                    uaFragment.startActivityForResult(intent, 100);
                    if (uaFragment.getActivity() != null)
                        uaFragment.getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.no_animation);
                } else {
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.no_animation);
                }

            } catch (Exception e) {
                Utils.logException(e);
            }
        }
    }

    public void notifyFavorite(int pos, boolean isFavorite) {
        if (pos >= 0 && pos < vacancyModels.size()) {
            try {
                if (vacancyModels.get(pos) instanceof VacancyModel) {
                    VacancyModel model = (VacancyModel) vacancyModels.get(pos);
                    model.setFavorite(isFavorite);
                    DbUtils.saveFavorite(model, indexParser);
                    notifyDataSetChanged();
                }
            } catch (Exception e) {
                Utils.logException(e);
            }
        }
    }

    public void removeVacancy(int pos, @NonNull DownloadAnimateListener listener) {
        if (pos >= 0 && pos < vacancyModels.size()) {
            try {
                vacancyModels.remove(pos);
                notifyDataSetChanged();
                if (vacancyModels.size() == 0) // удалили все вакансии
                    listener.onCompleteDownload();
            } catch (Exception e) {
                Utils.logException(e);
            }
        }
    }
}
