package com.megatron.findjobmylife.classes;

import android.content.Context;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.models.RegionModel;
import com.megatron.findjobmylife.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class RegionManager {
    private ArrayList<RegionModel> citys = new ArrayList<>();
    private static RegionManager instance;

    private RegionManager(Context context) {
        init(context);
    }

    public synchronized static RegionManager getInstance(Context context) {
        if (instance == null) instance = new RegionManager(context);
        return instance;
    }

    public synchronized static void onDestroy() {
        if (instance != null)
            instance.citys.clear();
        instance = null;
    }

    private void init(Context context) {
        String json;
        try {
            InputStream is = context.getResources().openRawResource(R.raw.city);
            int size = is.available();
            byte[] buffer = new byte[size];
            int len = is.read(buffer);
            is.close();
            if (len == 0) return;

            json = new String(buffer, Utils.UTF);
            jsonToCity(json);
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jsonToCity(String json) {
        if (json == null) return;
        try {
            JSONArray arr = new JSONArray(json);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                String cityUa = obj.getString("title_ua");
                String cityRu = obj.getString("title_ru");

                String work_ua = obj.getString("work_ua");
                String rabota_ua = obj.getString("rabota_ua");
                String hh_ua = obj.getString("hh_ua");
                String olx_ua = obj.getString("olx_ua");
                String jobs_ua = obj.getString("jobs_ua");

                citys.add(new RegionModel(cityUa, cityRu, work_ua, rabota_ua, hh_ua, olx_ua, jobs_ua));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized ArrayList<RegionModel> getEmptySearch() {
        RegionModel model;
        if (citys.size() > 0)
            model = citys.get(0);
        else model = new RegionModel("", "", "", "", "5", "", "");
        ArrayList<RegionModel> list = new ArrayList<>();
        list.add(model);
        return list;
    }

    public synchronized RegionSearchResult get(String title) {
        if (title.trim().equals("")) {
            return new RegionSearchResult(getEmptySearch(), new ArrayList<>());
        }

        ArrayList<RegionModel> result = new ArrayList<>();
        ArrayList<String> regionCity = new ArrayList<>();

        String find = title.trim();
        for (RegionModel model : citys) {
            String ua = model.getUaCity().trim();
            String ru = model.getRuCity().trim();
            if (find.equals(ua) || ua.toLowerCase().startsWith(find)) {
                result.add(model);
                regionCity.add(ua);
            }
            if (find.equals(ru) || ru.toLowerCase().startsWith(find)) {
                result.add(model);
                regionCity.add(ru);
            }
        }
        return new RegionSearchResult(result, regionCity);
    }

    public static class RegionSearchResult {
        private ArrayList<RegionModel> regionModelsResult;
        private ArrayList<String> regionCityResult;

        RegionSearchResult(ArrayList<RegionModel> regionModelsResult, ArrayList<String> regionCityResult) {
            this.regionModelsResult = regionModelsResult;
            this.regionCityResult = regionCityResult;
        }

        public ArrayList<RegionModel> getRegionModelsResult() {
            return regionModelsResult;
        }

        public ArrayList<String> getRegionCityResult() {
            return regionCityResult;
        }
    }

}
