package com.megatron.findjobmylife.classes;

import com.megatron.findjobmylife.App;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class VacancyManager {
    private ArrayList<String> vacancyNameModels = new ArrayList<>();
    private static VacancyManager instance;

    private VacancyManager() {
        init();
    }

    public synchronized static VacancyManager getInstance() {
        if (instance == null) instance = new VacancyManager();
        return instance;
    }

    public synchronized static void onDestroy() {
        if (instance != null)
            instance.vacancyNameModels.clear();
        instance = null;
    }

    private void init() {
        String json;
        try {
            InputStream is = App.getContext().getResources().openRawResource(R.raw.vacancy);
            int size = is.available();
            byte[] buffer = new byte[size];
            int len = is.read(buffer);
            is.close();
            if (len == 0) return;

            json = new String(buffer, Utils.UTF);
            toVacancy(json);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void toVacancy(String json) {
        if (json == null) return;
        try {
            String[] vacancyList = json.split("\r\n");
            vacancyNameModels.addAll(Arrays.asList(vacancyList));

            Collections.sort(vacancyNameModels, String::compareToIgnoreCase);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized ArrayList<String> get(String search) {
        if (search == null) return new ArrayList<>();
        if (search.trim().equals("")) return new ArrayList<>();
        String txt = search.trim().toLowerCase();
        ArrayList<String> res = new ArrayList<>();
        for (String model : vacancyNameModels) {
            if (model.startsWith(txt))
                res.add(model);
        }
        return res;
    }
}
