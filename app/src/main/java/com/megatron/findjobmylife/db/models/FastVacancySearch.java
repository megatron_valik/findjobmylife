package com.megatron.findjobmylife.db.models;

import io.realm.RealmObject;

/**
 * для быстрого поиска
 */
public class FastVacancySearch extends RealmObject {

    private String vacancy;

    public FastVacancySearch() {
    }

    public FastVacancySearch(String vacancy) {
        this.vacancy = vacancy;
    }

    public String getVacancy() {
        return vacancy;
    }

    public void setVacancy(String vacancy) {
        this.vacancy = vacancy;
    }
}
