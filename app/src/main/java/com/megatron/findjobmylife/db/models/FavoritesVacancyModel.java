package com.megatron.findjobmylife.db.models;

import io.realm.RealmObject;

public class FavoritesVacancyModel extends RealmObject {
    private int index;
    private String title;
    private String subTitle;
    private String vipTitle;
    private String cityName;
    private String overflow;
    private String url;
    private String urlImage;

    public FavoritesVacancyModel() {
    }

    public FavoritesVacancyModel(int index, String title, String subTitle, String vipTitle,
                                 String cityName, String overflow, String url, String urlImage) {
        this.index = index;
        this.title = title;
        this.subTitle = subTitle;
        this.vipTitle = vipTitle;
        this.cityName = cityName;
        this.overflow = overflow;
        this.url = url;
        this.urlImage = urlImage;
    }

    public int getIndex() {
        return index;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public String getVipTitle() {
        return vipTitle;
    }

    public String getCityName() {
        return cityName;
    }

    public String getOverflow() {
        return overflow;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlImage() {
        return urlImage;
    }
}
