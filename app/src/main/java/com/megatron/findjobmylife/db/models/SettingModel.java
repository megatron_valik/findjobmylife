package com.megatron.findjobmylife.db.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SettingModel extends RealmObject {
    @PrimaryKey
    private int indexJobUa;
    private RealmList<Boolean> categorySelected; // категория
    private int categorySelectedIndex;

    private RealmList<Boolean> employmentSelected;// вид занятости
    private int employmentSelectIndex;// вид зайнятости

    private int salaryFromSelected; //зп от
    private int salaryToSelected; // зр до
    private String salarySum = ""; // зп сума
    private RealmList<Boolean> otherSelected = new RealmList<>();
    private int otherSelectedIndex;

    public SettingModel() {
    }

    public SettingModel(int indexJobUa) {
        this.indexJobUa = indexJobUa;
    }

    public int getIndexJobUa() {
        return indexJobUa;
    }

    public void setIndexJobUa(int indexJobUa) {
        this.indexJobUa = indexJobUa;
    }

    public RealmList<Boolean> getCategorySelected() {
        return categorySelected;
    }

    public void setCategorySelected(RealmList<Boolean> categorySelected) {
        this.categorySelected = categorySelected;
    }

    public int getCategorySelectedIndex() {
        return categorySelectedIndex;
    }

    public void setCategorySelectedIndex(int categorySelectedIndex) {
        this.categorySelectedIndex = categorySelectedIndex;
    }

    public RealmList<Boolean> getEmploymentSelected() {
        return employmentSelected;
    }

    public void setEmploymentSelected(RealmList<Boolean> employmentSelected) {
        this.employmentSelected = employmentSelected;
    }

    public int getEmploymentSelectIndex() {
        return employmentSelectIndex;
    }

    public void setEmploymentSelectIndex(int employmentSelectIndex) {
        this.employmentSelectIndex = employmentSelectIndex;
    }

    public int getSalaryFromSelected() {
        return salaryFromSelected;
    }

    public void setSalaryFromSelected(int salaryFromSelected) {
        this.salaryFromSelected = salaryFromSelected;
    }

    public int getSalaryToSelected() {
        return salaryToSelected;
    }

    public void setSalaryToSelected(int salaryToSelected) {
        this.salaryToSelected = salaryToSelected;
    }

    public String getSalarySum() {
        return salarySum;
    }

    public void setSalarySum(String salarySum) {
        this.salarySum = salarySum;
    }

    public RealmList<Boolean> getOtherSelected() {
        return otherSelected;
    }

    public void setOtherSelected(RealmList<Boolean> otherSelected) {
        this.otherSelected = otherSelected;
    }

    public int getOtherSelectedIndex() {
        return otherSelectedIndex;
    }

    public void setOtherSelectedIndex(int otherSelectedIndex) {
        this.otherSelectedIndex = otherSelectedIndex;
    }
}
