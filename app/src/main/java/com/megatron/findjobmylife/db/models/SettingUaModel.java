package com.megatron.findjobmylife.db.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SettingUaModel extends RealmObject {
    @PrimaryKey
    private int indexSettingUa = 1;
    private int all_ua = 1;
    private int work_ua;
    private int rabota_ua;
    private int hh_ua;
    private int olx_ua;
    private int jobs_ua;

    public SettingUaModel() {
    }

    public int getIndexSettingUa() {
        return indexSettingUa;
    }

    public void setIndexSettingUa(int indexSettingUa) {
        this.indexSettingUa = indexSettingUa;
    }

    public int isWork_ua() {
        return work_ua;
    }

    public void setWork_ua(int work_ua) {
        this.work_ua = work_ua;
    }

    public int isRabota_ua() {
        return rabota_ua;
    }

    public void setRabota_ua(int rabota_ua) {
        this.rabota_ua = rabota_ua;
    }

    public int isHh_ua() {
        return hh_ua;
    }

    public void setHh_ua(int hh_ua) {
        this.hh_ua = hh_ua;
    }

    public int isOlx_ua() {
        return olx_ua;
    }

    public void setOlx_ua(int olx_ua) {
        this.olx_ua = olx_ua;
    }

    public int isJobs_ua() {
        return jobs_ua;
    }

    public void setJobs_ua(int jobs_ua) {
        this.jobs_ua = jobs_ua;
    }

    public boolean[] selected() {
        return new boolean[]{all_ua == 1, work_ua == 1, rabota_ua == 1, hh_ua == 1, olx_ua == 1, jobs_ua == 1};
    }

    public void setSelected(boolean[] selected) {
        if (selected != null && selected.length == 6) {
            all_ua = selected[0] ? 1 : 0;
            work_ua = selected[1] ? 1 : 0;
            rabota_ua = selected[2] ? 1 : 0;
            hh_ua = selected[3] ? 1 : 0;
            olx_ua = selected[4] ? 1 : 0;
            jobs_ua = selected[5] ? 1 : 0;
        }
    }
}
