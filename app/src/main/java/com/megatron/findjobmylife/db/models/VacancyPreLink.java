package com.megatron.findjobmylife.db.models;

import io.realm.RealmObject;

public class VacancyPreLink extends RealmObject {
    private long indexVacancy; // индекс сайта (WORK, RABOTA, HH)
    private String urlVacancy; // ссылка вакансии

    public VacancyPreLink() {
    }

    public void setIndexVacancy(long indexVacancy) {
        this.indexVacancy = indexVacancy;
    }

    public void setUrlVacancy(String urlVacancy) {
        this.urlVacancy = urlVacancy;
    }

}
