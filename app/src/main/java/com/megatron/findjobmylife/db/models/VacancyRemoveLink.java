package com.megatron.findjobmylife.db.models;

import io.realm.RealmObject;

/**
 * клас для вакансий которые необходимо удалить из списка отображаемых
 * 23.06.2020
 */
public class VacancyRemoveLink extends RealmObject {
    private long indexVacancy; // индекс сайта (WORK, RABOTA, HH, JOBS, OLX)
    private String urlVacancy; // ссылка вакансии

    public VacancyRemoveLink() {
    }

    public void setIndexVacancy(long indexVacancy) {
        this.indexVacancy = indexVacancy;
    }

    public void setUrlVacancy(String urlVacancy) {
        this.urlVacancy = urlVacancy;
    }

}
