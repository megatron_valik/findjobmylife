package com.megatron.findjobmylife.db.utils;

import com.megatron.findjobmylife.db.models.FastVacancySearch;
import com.megatron.findjobmylife.db.models.FavoritesVacancyModel;
import com.megatron.findjobmylife.db.models.SettingModel;
import com.megatron.findjobmylife.db.models.SettingUaModel;
import com.megatron.findjobmylife.db.models.VacancyPreLink;
import com.megatron.findjobmylife.db.models.VacancyRemoveLink;
import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class DbUtils {
    private final static int DB_VERSION = 4; // 3

    public static void migrate() {
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("default.realm")
                .schemaVersion(DB_VERSION)
                .migration(new Migration())
                .build();
        Realm.setDefaultConfiguration(configuration);
    }

    //***************************************************************************
    //проверка на посещенную ссылку
    public static Boolean isPreLink(String url) {
        boolean res = false;
        try (Realm realm = Realm.getDefaultInstance()) {
            VacancyPreLink p = realm.where(VacancyPreLink.class)
                    .equalTo("urlVacancy", url)
                    .findFirst();
            res = p != null;
        } catch (Exception e) {
            Utils.logException(e);
        }
        return res;
    }

    // добавить посещенную ссылку
    public static void addLink(String url) {
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.beginTransaction();
            VacancyPreLink preLink = realm.createObject(VacancyPreLink.class);
            preLink.setIndexVacancy(0);
            preLink.setUrlVacancy(url);

            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    // очистить посещенные ссылки
    public static void clearCash() {
        try (Realm realm = Realm.getDefaultInstance()) {
            RealmResults<VacancyPreLink> preLink = realm.where(VacancyPreLink.class).findAll();

            realm.beginTransaction();
            preLink.deleteAllFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    //***************************************************************************

    //***************** Удаление из списка вакансий *****************************

    public static boolean isRemovedLink(int index, String url){
        boolean res = false;
        try (Realm realm = Realm.getDefaultInstance()) {
            VacancyRemoveLink p = realm.where(VacancyRemoveLink.class)
                    .equalTo("indexVacancy", index)
                    .equalTo("urlVacancy", url)
                    .findFirst();
            res = p != null;
        } catch (Exception e) {
            Utils.logException(e);
        }
        return res;
    }

    public static void addRemovedLink(int index, String url){
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.beginTransaction();
            VacancyRemoveLink preLink = realm.createObject(VacancyRemoveLink.class);
            preLink.setIndexVacancy(index);
            preLink.setUrlVacancy(url);

            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    public static void clearRemovedCash(){
        try (Realm realm = Realm.getDefaultInstance()) {
            RealmResults<VacancyRemoveLink> preLink = realm.where(VacancyRemoveLink.class).findAll();

            realm.beginTransaction();
            preLink.deleteAllFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    //***************************************************************************

    public static SettingModel getSetting(int indexJob) {
        SettingModel res = null;
        try (Realm realm = Realm.getDefaultInstance()) {
            SettingModel p = realm.where(SettingModel.class)
                    .equalTo("indexJobUa", indexJob)
                    .findFirst();
            if (p != null)
                res = realm.copyFromRealm(p);
        } catch (Exception e) {
            Utils.logException(e);
        }
        return res;
    }

    public static void saveSetting(SettingModel model) {
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.beginTransaction();
            realm.insertOrUpdate(model);
            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    public static void saveFavorite(VacancyModel vacancyModel, int index) {
        FavoritesVacancyModel favorites = new FavoritesVacancyModel(index,
                vacancyModel.getTitle(), vacancyModel.getSubTitle(),
                vacancyModel.getVipTitle(), vacancyModel.getCityName(),
                vacancyModel.getOverflow(), vacancyModel.getUrl(),
                vacancyModel.getUrlImage()
        );
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.beginTransaction();
            boolean addOrRemove = vacancyModel.isFavorite();

            if (addOrRemove)
                realm.insertOrUpdate(favorites);
            else {
                RealmResults<FavoritesVacancyModel> res = realm
                        .where(FavoritesVacancyModel.class)
                        .equalTo("title", vacancyModel.getTitle())
                        .equalTo("subTitle", vacancyModel.getSubTitle())
                        .equalTo("overflow", vacancyModel.getOverflow())
                        .equalTo("url", vacancyModel.getUrl())
                        .findAll();
                if (res != null)
                    res.deleteFirstFromRealm();
            }

            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    public static boolean isFavoritesVacancy(String uri) {
        try (Realm realm = Realm.getDefaultInstance()) {
            FavoritesVacancyModel res = realm
                    .where(FavoritesVacancyModel.class)
                    .equalTo("url", uri)
                    .findFirst();
            if (res != null) {
                FavoritesVacancyModel m = realm.copyFromRealm(res);
                return m.getUrl().equals(uri);
            }
            return false;//res != null;

        } catch (Exception e) {
//            Utils.logException(e);
        }
        return false;
    }

    public static ArrayList<VacancyModel> getFavoritesVacancy(int index) {
        ArrayList<VacancyModel> res = new ArrayList<>();
        try (Realm realm = Realm.getDefaultInstance()) {
            RealmResults<FavoritesVacancyModel> models = realm
                    .where(FavoritesVacancyModel.class)
                    .equalTo("index", index)
                    .findAll();
            if (models != null) {
                ArrayList<FavoritesVacancyModel> list = new ArrayList<>(realm.copyFromRealm(models));
                for (FavoritesVacancyModel model : list) {
                    res.add(new VacancyModel(model.getTitle(), model.getSubTitle(),
                            model.getCityName(), model.getUrlImage(), model.getVipTitle(),
                            model.getOverflow(), model.getUrl()));
                }
            }

        } catch (Exception e) {
            Utils.logException(e);
        }
        return res;
    }

    public static void clearFavorite() {
        try (Realm realm = Realm.getDefaultInstance()) {
            RealmResults<FavoritesVacancyModel> preLink = realm.where(FavoritesVacancyModel.class).findAll();

            realm.beginTransaction();
            preLink.deleteAllFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    public static void addFastSearch(FastVacancySearch vacancySearch) {
        if (vacancySearch == null || vacancySearch.getVacancy().equals("")) return;
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.beginTransaction();
            if (realm.where(FastVacancySearch.class)
                    .equalTo("vacancy", vacancySearch.getVacancy())
                    .count() == 0) {
                realm.insertOrUpdate(vacancySearch);
            }
            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    public static ArrayList<FastVacancySearch> getFastSearch() {
        ArrayList<FastVacancySearch> res = new ArrayList<>();
        try (Realm realm = Realm.getDefaultInstance()) {
            RealmResults<FastVacancySearch> models = realm
                    .where(FastVacancySearch.class)
                    .findAll();
            if (models != null) {
                ArrayList<FastVacancySearch> list = new ArrayList<>(realm.copyFromRealm(models));
                if (list.size() >= 2) {
                    Collections.reverse(list);
                }
                int count = 10;
                int index = 0;
                for (FastVacancySearch model : list) {
                    res.add(new FastVacancySearch(model.getVacancy()));
                    index++;
                    if (index >= count) break;
                }
            }
        } catch (Exception e) {
            Utils.logException(e);
        }
        return res;
    }

    public static void clearFastSearch() {
        try (Realm realm = Realm.getDefaultInstance()) {
            RealmResults<FastVacancySearch> fastSearch = realm.where(FastVacancySearch.class).findAll();

            realm.beginTransaction();
            fastSearch.deleteAllFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    public static void writeSelectedUa(SettingUaModel uaModel) {
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.beginTransaction();
            realm.insertOrUpdate(uaModel);
            realm.commitTransaction();
        } catch (Exception e) {
            Utils.logException(e);
        }
    }

    public static boolean[] readSelectUa() {
        SettingUaModel uaModel = new SettingUaModel();
        try (Realm realm = Realm.getDefaultInstance()) {
            SettingUaModel p = realm.where(SettingUaModel.class)
                    .equalTo("indexSettingUa", 1)
                    .findFirst();
            if (p != null)
                uaModel = realm.copyFromRealm(p);
        } catch (Exception e) {
            Utils.logException(e);
        }
        return uaModel.selected();
    }
}
