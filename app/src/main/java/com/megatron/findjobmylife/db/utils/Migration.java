package com.megatron.findjobmylife.db.utils;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class Migration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        try {
            if (oldVersion == 0) {
                schema.create("FavoritesVacancyModel")
                        .addField("index", int.class)
                        .addField("title", String.class)
                        .addField("subTitle", String.class)
                        .addField("vipTitle", String.class)
                        .addField("cityName", String.class)
                        .addField("overflow", String.class)
                        .addField("url", String.class)
                        .addField("urlImage", String.class);
                oldVersion++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (oldVersion == 1) {
            try {
                schema.create("FastVacancySearch")
                        .addField("vacancy", String.class);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                oldVersion++;
            }
        }
        if (oldVersion == 2) {
            try {
                schema.create("SettingUaModel")
                        .addField("indexSettingUa", int.class, FieldAttribute.PRIMARY_KEY)
                        .addField("all_ua", int.class)
                        .addField("work_ua", int.class)
                        .addField("rabota_ua", int.class)
                        .addField("hh_ua", int.class)
                        .addField("olx_ua", int.class)
                        .addField("jobs_ua", int.class);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                oldVersion++;
            }
        }
        if (oldVersion == 3) {
            try {
                RealmObjectSchema obj = schema.get("VacancyRemoveLink");
                if (obj != null) {
                    obj.addField("indexVacancy", long.class)
                            .addField("urlVacancy", String.class);
                } else {
                    schema.create("VacancyRemoveLink")
                            .addField("indexVacancy", long.class)
                            .addField("urlVacancy", String.class);
                }
            } catch (Throwable e) {
                e.printStackTrace();
            } finally {
                oldVersion++;
            }
        }

    }
}