package com.megatron.findjobmylife.dialogs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.ui.HtmlTextView;
import com.megatron.findjobmylife.utils.ToastHelper;
import com.megatron.findjobmylife.utils.Utils;

public class PhoneDialogHelper {
    private String phone;
    private Context context;
    private HtmlTextView.HtmlTextViewCallListener callListener;

    public PhoneDialogHelper(String phone, Context context, HtmlTextView.HtmlTextViewCallListener callListener) {
        this.phone = phone;
        this.context = context;
        this.callListener = callListener;
    }

    public void show() {
        if (phone == null || phone.trim().equals("")) return;
        String[] phones = phone.split(",");
        if (phones.length == 0) return;
        try {
            if (!Utils.hasPermission(context)) {
                if (callListener != null)
                    callListener.onCall(phone);
                return;
            }
        } catch (Exception e) {
            Utils.logException(e);
            return;
        }

        if (phones.length == 1) {
            onCall(phones[0]);
        } else {
            showDialog(phones);
        }
    }

    private void showDialog(String[] phones) {
        final int[] selectedItem = {-1};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Button[] finalBtnOk = {null};
        builder.setTitle(context.getString(R.string.chose_phone_title))
                .setCancelable(false)
                // добавляем одну кнопку для закрытия диалога
                .setNegativeButton(context.getString(R.string.cancel_btn), (dialog, id) -> dialog.cancel())
                .setPositiveButton(context.getString(R.string.ok_btn), ((dialog, which) -> {
                    if (selectedItem[0] < 0 || selectedItem[0] >= phones.length) return;
                    onCall(phones[selectedItem[0]]);
                }))

                // добавляем переключатели
                .setSingleChoiceItems(phones, -1, (dialog, item) -> {
                    selectedItem[0] = item;
                    if (finalBtnOk[0] != null) {
                        finalBtnOk[0].setEnabled(true);
                    }
                });

        AlertDialog dialogBuild = builder.create();
        dialogBuild.show();

        Button btnOk = dialogBuild.getButton(AlertDialog.BUTTON_POSITIVE);
        btnOk.setEnabled(false);
        finalBtnOk[0] = btnOk;
    }

    private void onCall(String phoneCall) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneCall));
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            ToastHelper.show(context.getString(R.string.error_call_phone));
        }
    }
}
