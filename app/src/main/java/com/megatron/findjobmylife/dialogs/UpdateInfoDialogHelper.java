package com.megatron.findjobmylife.dialogs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatTextView;

import com.megatron.findjobmylife.BuildConfig;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.utils.Utils;

import java.io.IOException;
import java.io.InputStream;

public class UpdateInfoDialogHelper {
    private Context context;
    private final static String PREF_VERSION = "app_version";

    public UpdateInfoDialogHelper(Context context) {
        this.context = context;
    }

    public void show() {
        int versionCode = BuildConfig.VERSION_CODE;
        int lastVersion = preferenceVersion();
        if (lastVersion == -1) {
            saveVersion(versionCode);
        } else if (lastVersion == 0 || versionCode > lastVersion) {
            saveVersion(versionCode);
            try {
                showDialog();
            } catch (Exception e) {
                Utils.logException(e);
            } catch (StackOverflowError e) {
                Utils.logException(e);
            }
        }
    }

    private int preferenceVersion() {
        String prefVersion = Utils.readPreference(PREF_VERSION);
        if (prefVersion.trim().equals("")) return 0;
        int version = -1;
        try {
            version = Integer.parseInt(prefVersion);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }

    private void saveVersion(int version) {
        Utils.savePreference(PREF_VERSION, version + "");
    }

    private void showDialog() {
        String mes = getText();
        if (mes.trim().equals("")) return;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.version_info_dialog, null);

        AppCompatTextView txt = view.findViewById(R.id.txtIndex);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txt.setText(Html.fromHtml(mes, Html.FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE, null, null));
        } else {
            txt.setText(Html.fromHtml(mes, null, null));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false)
                .setView(view)
                .setPositiveButton(context.getString(R.string.ok_btn),
                        ((dialog, which) -> dialog.dismiss()));
        builder.create().show();
    }

    private String getText() {
        String res = "";
        try {
            InputStream stream = context.getAssets().open("index.html");

            int size = stream.available();
            byte[] buffer = new byte[size];
            int len = stream.read(buffer);
            if (len == 0) return res;
            stream.close();
            res = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }
        return res;
    }
}
