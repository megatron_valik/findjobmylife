package com.megatron.findjobmylife.favorites;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.databinding.FragmentFavoriteListBinding;
import com.megatron.findjobmylife.models.DownloadModel;
import com.megatron.findjobmylife.utils.Utils;

import java.util.Objects;

import static android.app.Activity.RESULT_CANCELED;

public abstract class AbstractFavoriteFragment extends Fragment {
    private DownloadModel downloadModel = new DownloadModel();
    private FavoriteVacancyAdapter adapter;
    private MyAnimatorDownload animatorDownload;

    public abstract int getIndex();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentFavoriteListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite_list,
                container, false);
        View v = binding.getRoot();

        RecyclerView rv = v.findViewById(R.id.rv_ua);
        int orientation = Objects.requireNonNull(getActivity()).getResources().getConfiguration().orientation;

        StaggeredGridLayoutManager linearLayoutManager = new StaggeredGridLayoutManager(
                orientation == Configuration.ORIENTATION_PORTRAIT ? 1 : 2,
                StaggeredGridLayoutManager.VERTICAL);

        rv.setLayoutManager(linearLayoutManager);
        View downloadProgress = v.findViewById(R.id.downloadProgress);

        if (adapter == null) {
            adapter = new FavoriteVacancyAdapter(getActivity(), this, getIndex())
                    .setAnimateListener(animatorDownload = new MyAnimatorDownload(binding, downloadProgress))
                    .search();
        } else {
            adapter.setAnimateListener(animatorDownload = new MyAnimatorDownload(binding, downloadProgress));
            if (adapter.getItemCount() == 0 && !adapter.isLoadingVacancy()) {
                animateProgress(downloadProgress, false);
                adapter.search();
            } else if (adapter.isLoadingVacancy()) {
                animateProgress(downloadProgress, true);
            }
            binding.setDownload(downloadModel);
        }
        rv.setAdapter(adapter);

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) return;
        if (requestCode == 100 && data != null) {
            int pos = data.getIntExtra("POSITION", -1);
            boolean isFavorite = data.getBooleanExtra("FAVORITE", false);
            if (adapter != null && pos >= 0) {
                adapter.updateFavoriteItem(pos, isFavorite);
                if (animatorDownload != null)
                    animatorDownload.onCompleteDownload();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (adapter != null)
            adapter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null)
            adapter.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.setAnimateListener(animatorDownload);
        }
    }

    private class MyAnimatorDownload implements FavoriteVacancyAdapter.DownloadAnimateListener {
        private FragmentFavoriteListBinding binding;
        private View downloadProgress;

        MyAnimatorDownload(FragmentFavoriteListBinding binding, View downloadProgress) {
            this.binding = binding;
            this.downloadProgress = downloadProgress;
        }

        @Override
        public void onStartDownload() {
            downloadModel.setDownloading(true);
            downloadModel.setEmptyShow(false);
            binding.setDownload(downloadModel);
            animateProgress(downloadProgress, true);
        }

        @Override
        public void onCompleteDownload() {
            if (adapter.getItemCount() == 0)
                showEmptyVacancy();
            downloadModel.setDownloading(false);
            binding.setDownload(downloadModel);
            animateProgress(downloadProgress, false);
        }

        @Override
        public void onDestroy() {
            downloadModel.setDownloading(false);
            downloadModel.setEmptyShow(false);
        }
    }

    private void showEmptyVacancy() {
        downloadModel.setEmptyShow(true);
    }

    private void animateProgress(View view, boolean show) {
        Utils.animateDownloadProgress(getActivity(), view, show);
    }

}
