package com.megatron.findjobmylife.favorites;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class FavoritePageAdapter extends FragmentStatePagerAdapter {

    private AbstractFavoriteFragment[] items;

    FavoritePageAdapter(@NonNull FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    void setItems(AbstractFavoriteFragment[] items){
        this.items = items;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return items[position];
    }

    @Override
    public int getCount() {
        return 5;
    }

}
