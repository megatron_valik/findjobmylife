package com.megatron.findjobmylife.favorites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.favorites.tabFragments.HhUaFavorite;
import com.megatron.findjobmylife.favorites.tabFragments.JobsUaFavorite;
import com.megatron.findjobmylife.favorites.tabFragments.OlxUaFavorite;
import com.megatron.findjobmylife.favorites.tabFragments.RabotaUaFavorite;
import com.megatron.findjobmylife.favorites.tabFragments.WorkUaFavorite;

public class FavoriteTabFragment extends Fragment {
    private AbstractFavoriteFragment[] items = {
            new WorkUaFavorite(), new RabotaUaFavorite(), new HhUaFavorite(),
            new OlxUaFavorite(), new JobsUaFavorite()
    };

    public FavoriteTabFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tab, container, false);

        TabLayout tabLayout = v.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("WORK.UA"));
        tabLayout.addTab(tabLayout.newTab().setText("RABOTA.UA"));
        tabLayout.addTab(tabLayout.newTab().setText("HH.UA"));
        tabLayout.addTab(tabLayout.newTab().setText("OLX.UA"));
        tabLayout.addTab(tabLayout.newTab().setText("JOBS.UA"));

        FavoritePageAdapter adapter = new FavoritePageAdapter(getChildFragmentManager());
        adapter.setItems(items);

        final ViewPager viewPager = v.findViewById(R.id.pager);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return v;
    }
}
