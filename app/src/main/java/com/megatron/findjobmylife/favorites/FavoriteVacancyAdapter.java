package com.megatron.findjobmylife.favorites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.activity.VacancyActivity;
import com.megatron.findjobmylife.databinding.ItemVacancyBinding;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.models.VacancyModel;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FavoriteVacancyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public interface DownloadAnimateListener {
        void onStartDownload();

        void onCompleteDownload();

        void onDestroy();
    }

    private ArrayList<VacancyModel> vacancyModels = new ArrayList<>();
    private Context context;
    private int indexParser;
    private Disposable fabricDisposable = null;
    private AbstractFavoriteFragment uaFragment;
    private DownloadAnimateListener animateListener;
    private boolean isLoadingVacancy = false;
    private boolean isDownloadingMore = false;
    //после поворота екрана - догружаем данные и потом удаляем
    private int removeItemIndex = -1;

    FavoriteVacancyAdapter(Context context, AbstractFavoriteFragment favoriteFragment, int index) {
        this.context = context;
        uaFragment = favoriteFragment;
        indexParser = index;
    }

    FavoriteVacancyAdapter setAnimateListener(DownloadAnimateListener listener) {
        animateListener = listener;
        return this;
    }

    FavoriteVacancyAdapter search() {
        searchFavorite();
        return this;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ItemVacancyBinding binding = ItemVacancyBinding.inflate(inflater, viewGroup, false);
        return new VacancyHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        VacancyHolder holder = (VacancyHolder) viewHolder;
        holder.build(vacancyModels.get(position), position);
    }

    @Override
    public int getItemCount() {
        return vacancyModels.size();
    }

    void onDestroy() {
        isLoadingVacancy = false;
        animateListener = null;
        if (fabricDisposable != null && !fabricDisposable.isDisposed())
            fabricDisposable.dispose();
    }

    void onPause() {
        isLoadingVacancy = false;

        if (animateListener != null)
            animateListener.onDestroy();

        animateListener = null;
        if (fabricDisposable != null && !fabricDisposable.isDisposed())
            fabricDisposable.dispose();
    }

    void updateFavoriteItem(int pos, boolean isFavorite) {
        if (pos >= vacancyModels.size()) {
            removeItemIndex = pos;
            return;
        }
        VacancyModel model = vacancyModels.get(pos);
        model.setFavorite(isFavorite);
        DbUtils.saveFavorite(model, indexParser);
        if (!model.isFavorite()) {
            vacancyModels.remove(pos);
            notifyDataSetChanged();
            if (animateListener != null)
                animateListener.onCompleteDownload();
        }
    }

    class VacancyHolder extends RecyclerView.ViewHolder {
        ItemVacancyBinding binding;

        VacancyHolder(ItemVacancyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.executePendingBindings();
        }

        void build(VacancyModel vacancyModel, int position) {
            binding.setVacancy(vacancyModel);

            binding.getRoot().setOnClickListener(view -> showFullVacancy(vacancyModel, position));
        }

        private void showFullVacancy(VacancyModel model, int position) {
            Intent intent = new Intent(context, VacancyActivity.class);
            intent.putExtra("INDEX", indexParser);
            intent.putExtra("URL", model.getUrl());
            //***
            intent.putExtra("POSITION", position);
            intent.putExtra("FAVORITE", model.isFavorite());

//            intent.putExtra("TITLE", model.getTitle());
//            intent.putExtra("SUBTITLE", model.getSubTitle());
//            intent.putExtra("VIPTITLE", model.getVipTitle());
//            intent.putExtra("CITYNAME", model.getCityName());
//            intent.putExtra("OVERFLOW", model.getOverflow());
//            intent.putExtra("URLIMAGE", model.getUrlImage());
            if (uaFragment != null) {
                uaFragment.startActivityForResult(intent, 100);
                if (uaFragment.getActivity() != null)
                    uaFragment.getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.no_animation);
            } else {
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.no_animation);
            }

        }
    }

    private void searchFavorite() {
        if (isDownloadingMore) {
            if (animateListener != null)
                animateListener.onCompleteDownload();
            return;
        }
        onSearch(indexParser).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<VacancyModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        fabricDisposable = d;
                        isLoadingVacancy = true;
                        if (animateListener != null)
                            animateListener.onStartDownload();
                    }

                    @Override
                    public void onNext(ArrayList<VacancyModel> models) {
                        vacancyModels = models;
                        isDownloadingMore = vacancyModels.size() == 0;
                    }

                    @Override
                    public void onError(Throwable e) {
                        fabricDisposable = null;
                        if (animateListener != null)
                            animateListener.onCompleteDownload();
                    }

                    @Override
                    public void onComplete() {
                        isLoadingVacancy = false;
                        fabricDisposable = null;
                        isDownloadingMore = true;

                        if (animateListener != null)
                            animateListener.onCompleteDownload();
                        if (removeItemIndex >= 0) {
                            updateFavoriteItem(removeItemIndex, true);
                            removeItemIndex = -1;
                        }
                        notifyDataSetChanged();
                    }
                });
    }

    boolean isLoadingVacancy() {
        return isLoadingVacancy;
    }

    private Observable<ArrayList<VacancyModel>> onSearch(int index) {
        return Observable.create(emitter -> {
            try {
                ArrayList<VacancyModel> list = DbUtils.getFavoritesVacancy(index);
                emitter.onNext(list);
                emitter.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }

        });
    }

}
