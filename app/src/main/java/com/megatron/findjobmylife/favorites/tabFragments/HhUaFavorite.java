package com.megatron.findjobmylife.favorites.tabFragments;

import com.megatron.findjobmylife.favorites.AbstractFavoriteFragment;
import com.megatron.findjobmylife.parser.ParserFabric;

public class HhUaFavorite extends AbstractFavoriteFragment {
    @Override
    public int getIndex() {
        return ParserFabric.HH_UA;
    }
}
