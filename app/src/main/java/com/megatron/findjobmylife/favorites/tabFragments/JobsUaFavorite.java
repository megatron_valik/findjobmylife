package com.megatron.findjobmylife.favorites.tabFragments;

import com.megatron.findjobmylife.favorites.AbstractFavoriteFragment;
import com.megatron.findjobmylife.parser.ParserFabric;

public class JobsUaFavorite extends AbstractFavoriteFragment {
    @Override
    public int getIndex() {
        return ParserFabric.JOBS_UA;
    }
}
