package com.megatron.findjobmylife.fragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.adapters.EndlessRecyclerViewScrollListener;
import com.megatron.findjobmylife.adapters.VacancyAdapter;
import com.megatron.findjobmylife.databinding.FragmentContentListBinding;
import com.megatron.findjobmylife.models.DownloadModel;
import com.megatron.findjobmylife.utils.ToastHelper;
import com.megatron.findjobmylife.utils.Utils;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public abstract class AbstractUaFragment extends Fragment {
    private boolean selectTabIndex = false;
    private SwipeRefreshLayout swipeLayout;
    private VacancyAdapter adapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    private DownloadModel downloadModel = new DownloadModel();
    private MyAnimatorDownload animatorDownload;
    private boolean isEmptyListAfterRemove; // пустой список после удаления всех вакансий

    void networkState(boolean isConnect) {
        if (scrollListener != null)
            scrollListener.setNetworkConnect(isConnect);
    }

    public abstract int getFragmentIndex();

    public abstract String getIdCity();

    public abstract String getParams();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentContentListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_content_list, container, false);
        View v = binding.getRoot();

        swipeLayout = v.findViewById(R.id.swipeLayout);
        swipeLayout.setColorSchemeResources(
                android.R.color.holo_blue_dark,
                android.R.color.holo_red_light
        );

        RecyclerView rv = v.findViewById(R.id.rv_ua);
        int orientation = getResources().getConfiguration().orientation;

        StaggeredGridLayoutManager linearLayoutManager = new StaggeredGridLayoutManager(
                orientation == Configuration.ORIENTATION_PORTRAIT ? 1 : 2,
                StaggeredGridLayoutManager.VERTICAL);

        View downloadProgress = v.findViewById(R.id.downloadProgress);

        if (adapter == null) {
            adapter = new VacancyAdapter(getActivity(), animatorDownload = new MyAnimatorDownload(binding, downloadProgress));
            if (selectTabIndex) {
                adapter.setActiveTab(true);
                selectTabIndex = false;
            }

            String vacancy = Utils.readPreference(Utils.PREFERENCES_VACANCY_NAME);
            String city = Utils.readPreference(Utils.PREFERENCES_CITY_NAME);
            String params = getParams();

            adapter.setUaFragment(this)
                    .setParams(params)
                    .set(vacancy.toLowerCase(), city, getIdCity(), getFragmentIndex())
                    .setIndexUa(getFragmentIndex())
                    .search(0);
            scrollListener = adapter.setScrollListener();
        } else {
            adapter.setAnimateListener(animatorDownload = new MyAnimatorDownload(binding, downloadProgress));
            scrollListener = adapter.setScrollListener();
            if (adapter.getItemCount() == 0 && !adapter.isLoadingVacancy()) {
                animateProgress(downloadProgress, false);
                adapter.search();
            } else if (adapter.isLoadingVacancy()) {
                animateProgress(downloadProgress, true);
            }
            binding.setDownload(downloadModel);
        }

        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(adapter);
        rv.addOnScrollListener(scrollListener);

        swipeLayout.setOnRefreshListener(() -> {
            if (adapter != null) {
                adapter.setSwipeDownloadListener(isSuccess -> {
                    if (swipeLayout != null)
                        swipeLayout.setRefreshing(false);
                    adapter.setSwipeDownloadListener(null);
                    if (isSuccess)
                        linearLayoutManager.scrollToPositionWithOffset(0, 0);
                });
                adapter.search(0);
            }
        });

        v.findViewById(R.id.btnSearchRepeat).setOnClickListener(view -> repeatSearch());

        return v;
    }

    private void repeatSearch() {
        if (adapter != null) {
            adapter.search();
        }
    }

    private void animateProgress(View view, boolean show) {
        Utils.animateDownloadProgress(getActivity(), view, show);
    }

    private void showEmptyVacancy() {
        downloadModel.setEmptyShow(true);
    }

    void setActiveTab(boolean activeTab) {
        if (adapter != null) { // задать активную вкладку
            adapter.setActiveTab(activeTab);
            if (activeTab) {
                String toast = adapter.getToast();
                if (toast != null && !toast.equals("")) {
                    ToastHelper.show(getView(), toast);
                }
            }
        } else selectTabIndex = activeTab; // при первом старте
    }

    @Override
    public void onDestroy() {
        if (adapter != null)
            adapter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null)
            adapter.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.setAnimateListener(animatorDownload);
        }
        if (isEmptyListAfterRemove) { // нет вакансий после удаления из списка
            if (animatorDownload != null)
                animatorDownload.onCompleteDownload();
            isEmptyListAfterRemove = false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) return;
        if (resultCode == RESULT_OK && requestCode == 100 && data != null) {
            int pos = data.getIntExtra("POSITION", -1);
            boolean isFavorite = data.getBooleanExtra("FAVORITE", false);
            boolean isRemoved = data.getBooleanExtra("REMOVE", false);
            if (adapter != null) {
                if (isRemoved)
                    adapter.removeVacancy(pos, new VacancyAdapter.DownloadAnimateListener() {

                                @Override
                                public void onStartDownload() {

                                }

                                @Override
                                public void onCompleteDownload() {
                                    isEmptyListAfterRemove = true;
                                }

                                @Override
                                public void onDestroy() {

                                }
                            }
                    );
                else
                    adapter.notifyFavorite(pos, isFavorite);
            }
        }
    }

    private class MyAnimatorDownload implements VacancyAdapter.DownloadAnimateListener {
        private FragmentContentListBinding binding;
        private View downloadProgress;

        MyAnimatorDownload(FragmentContentListBinding binding, View downloadProgress) {
            this.binding = binding;
            this.downloadProgress = downloadProgress;
        }

        @Override
        public void onStartDownload() {
            downloadModel.setDownloading(true);
            downloadModel.setEmptyShow(false);
            binding.setDownload(downloadModel);
            animateProgress(downloadProgress, true);
        }

        @Override
        public void onCompleteDownload() {
            if (adapter.getItemCount() == 0)
                showEmptyVacancy();
            downloadModel.setDownloading(false);
            binding.setDownload(downloadModel);
            animateProgress(downloadProgress, false);
        }

        @Override
        public void onDestroy() {
            downloadModel.setDownloading(false);
            downloadModel.setEmptyShow(false);
        }
    }
}
