package com.megatron.findjobmylife.fragments;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.megatron.findjobmylife.fragments.tabFragments.HhUaFragment;
import com.megatron.findjobmylife.fragments.tabFragments.JobsUaFragment;
import com.megatron.findjobmylife.fragments.tabFragments.OlxUaFragment;
import com.megatron.findjobmylife.fragments.tabFragments.RabotaUaFragment;
import com.megatron.findjobmylife.fragments.tabFragments.WorkUaFragment;

import java.util.ArrayList;
import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private List<AbstractUaFragment> items = new ArrayList<>();

    PagerAdapter(FragmentManager fm, boolean[] tabs) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        if (tabs[0] || tabs[1]) {
            items.add(new WorkUaFragment());
        }
        if (tabs[0] || tabs[2]) {
            items.add(new RabotaUaFragment());
        }
        if (tabs[0] || tabs[3]) {
            items.add(new HhUaFragment());
        }
        if (tabs[0] || tabs[4]) {
            items.add(new OlxUaFragment());
        }
        if (tabs[0] || tabs[5]) {
            items.add(new JobsUaFragment());
        }
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                return items.get(position);
            default:
                return items.get(0);
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    void setActiveTab(int tabIndex, boolean isActive) {
        items.get(tabIndex).setActiveTab(isActive);
    }

    void networkStatusChange(boolean isConnected) {
        for (AbstractUaFragment f : items) {
            f.networkState(isConnected);
        }
    }
}
