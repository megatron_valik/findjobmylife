package com.megatron.findjobmylife.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.adapters.AutoCompleteTextAdapter;
import com.megatron.findjobmylife.adapters.AutoCompleteTextVacancyAdapter;
import com.megatron.findjobmylife.databinding.FragmentSearchBinding;
import com.megatron.findjobmylife.db.models.FastVacancySearch;
import com.megatron.findjobmylife.db.models.SettingModel;
import com.megatron.findjobmylife.db.models.SettingUaModel;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.fragments.setting.AbstractSettingFragment;
import com.megatron.findjobmylife.models.ModelInternet;
import com.megatron.findjobmylife.models.RegionModel;
import com.megatron.findjobmylife.ui.MultiSelectSpinner;
import com.megatron.findjobmylife.ui.MyAutoCompleteTextView;
import com.megatron.findjobmylife.utils.ToastHelper;
import com.megatron.findjobmylife.utils.Utils;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.realm.RealmList;

public class SearchFragment extends AbstractSettingFragment {
    //    private OnFragmentInteractionListener mListener;
    private AutoCompleteTextAdapter autoCompleteTextAdapter;
    private AutoCompleteTextVacancyAdapter autoCompleteTextVacancyAdapter;
    private ModelInternet modelInternet = new ModelInternet();
    private Disposable searchDestroyed, searchVacancyDestroyed;
    private SettingModel[] settingModels = new SettingModel[5];

    private AppCompatSpinner setting_search_work_ua;
    private AppCompatSpinner setting_search_rabota_ua;
    private AppCompatSpinner setting_search_hh_ua;
    private AppCompatSpinner setting_search_olx_ua;
    private AppCompatSpinner setting_search_jobs_ua;

    private MyAutoCompleteTextView cityIn;
    private MyAutoCompleteTextView vacancyIn;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("CheckResult")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentSearchBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);
        View v = binding.getRoot();
        binding.setInternet(modelInternet);

        vacancyIn = v.findViewById(R.id.vacancyNameTextView);
        cityIn = v.findViewById(R.id.cityNameTextView);
        autoCompleteTextAdapter = new AutoCompleteTextAdapter(Objects.requireNonNull(getActivity()),
                R.layout.support_simple_spinner_dropdown_item);
        autoCompleteTextVacancyAdapter = new AutoCompleteTextVacancyAdapter(getActivity(),
                R.layout.support_simple_spinner_dropdown_item);

        if (vacancyIn != null) {
            vacancyIn.setText(Utils.readPreference(Utils.PREFERENCES_VACANCY_NAME));
            vacancyIn.setAdapter(autoCompleteTextVacancyAdapter);
            vacancyIn.setOnFocusChangeListener((v12, hasFocus) -> {
                if (hasFocus && autoCompleteTextVacancyAdapter != null) {
                    try {
                        if (autoCompleteTextVacancyAdapter.showFastVacancy()) {
                            vacancyIn.showDropDown();
                        }
                    } catch (Exception e) {
                        Utils.logException(e);
                    }
                }
            });
            vacancyIn.setOnEditorActionListener((v13, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                    Activity activity = getActivity();
                    if (activity != null)
                        Utils.hideKeyboard(activity);
                }
                return false;
            });
        }

        if (cityIn != null) {
            cityIn.setAdapter(autoCompleteTextAdapter);
            cityIn.setText(Utils.readPreference(Utils.PREFERENCES_CITY_NAME));
            cityIn.setOnEditorActionListener((v1, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search(binding, vacancyIn, cityIn);
                    return true;
                }
                return false;
            });
            cityIn.setOnItemClickListener((parent, view, position, id) ->
                    autoCompleteTextAdapter.setSelectedItem(position));
            cityIn.setOnEditorActionListener((v13, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search(binding, vacancyIn, cityIn);
                }
                return false;
            });
        }

        v.findViewById(R.id.btnSearch).setOnClickListener(view -> search(binding, vacancyIn, cityIn));

        //********************
        v.findViewById(R.id.more_setting).setOnClickListener(view -> onShowSetting(binding, view));
        v.findViewById(R.id.hide_setting).setOnClickListener(view -> onShowSetting(binding, view));
        v.findViewById(R.id.btnSaveSettingSearch).setOnClickListener(view -> onSaveSetting());

        //**************************
        String all = getResources().getString(R.string.all_ua);
        String[] ua = new String[]{all, "WORK.UA", "RABOTA.UA", "HH.UA", "OLX.UA", "JOBS.UA"};
        MultiSelectSpinner sp = v.findViewById(R.id.sp_setting_ua);
        sp.setItems(ua);
        sp.setFirstActiveIfEmpty(true);
        sp.setSelection(DbUtils.readSelectUa());
        sp.setChangeListener(selectedItems -> {
            SettingUaModel uaModel = new SettingUaModel();
            uaModel.setSelected(selectedItems);
            DbUtils.writeSelectedUa(uaModel);
        });
        //**************************

        searchSettingSpinner(v);
        return v;
    }

    private Observable<String> observableOn(@NonNull MyAutoCompleteTextView inputLayout) {
        return Observable.create((ObservableOnSubscribe<String>) subscriber ->
                inputLayout.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        subscriber.onNext(s.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                }))
                .map(text -> text.toLowerCase().trim())
                .debounce(250, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void searchSettingSpinner(View view) {
        setting_search_work_ua = view.findViewById(R.id.setting_search_work_ua);
        setting_search_rabota_ua = view.findViewById(R.id.setting_search_rabota_ua);
        setting_search_hh_ua = view.findViewById(R.id.setting_search_hh_ua);
        setting_search_olx_ua = view.findViewById(R.id.setting_search_olx_ua);
        setting_search_jobs_ua = view.findViewById(R.id.setting_search_jobs_ua);

        //***
        if (setting_search_work_ua != null && setting_search_rabota_ua != null &&
                setting_search_hh_ua != null && setting_search_olx_ua != null &&
                setting_search_jobs_ua != null && settingModels != null) {
            view.findViewById(R.id.btnResetSettingSearch).setOnClickListener(v -> {
                setting_search_work_ua.setSelection(0);
                setting_search_rabota_ua.setSelection(0);
                setting_search_hh_ua.setSelection(0);
                setting_search_olx_ua.setSelection(0);
                setting_search_jobs_ua.setSelection(0);

                for (int i = 0; i < settingModels.length; i++) {
                    settingModels[i] = new SettingModel(i);
                    settingModels[i].setCategorySelected(new RealmList<>());
                    settingModels[i].setEmploymentSelected(new RealmList<>());
                    settingModels[i].setOtherSelected(new RealmList<>());

                    settingModels[i].setCategorySelectedIndex(-1);
                }
                onSaveSetting();
            });
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void readSetting() {
        for (int i = 0; i < settingModels.length; i++) {
            settingModels[i] = DbUtils.getSetting(i);
            if (settingModels[i] == null) {
                settingModels[i] = new SettingModel(i);
                settingModels[i].setCategorySelected(new RealmList<>());
                settingModels[i].setEmploymentSelected(new RealmList<>());
                settingModels[i].setOtherSelected(new RealmList<>());
            }
            int size = settingModels[i].getCategorySelected().size();
            if (size > 0) {
                int select = -1;
                RealmList<Boolean> booleans = settingModels[i].getCategorySelected();
                for (int index = 0; index < size; index++) {
                    try {
                        if (booleans.get(index)) {
                            select = index;
                            break;
                        }
                    } catch (NullPointerException e) {
                        //
                    }
                }
                select = (select >= 0) ? select + 1 : 0;
                settingModels[i].setCategorySelectedIndex(select);
            }
        }

        String[] categoryWorkUa = addDefaultCategory(getActivity().getResources().getStringArray(R.array.Category));
        String[] categoryRabotaUa = (getActivity().getResources().getStringArray(R.array.Category2));
        String[] categoryHhUa = addDefaultCategory(getActivity().getResources().getStringArray(R.array.Category3));
        String[] categoryOlxUa = (getActivity().getResources().getStringArray(R.array.Category4));
        String[] categoryJobsUa = (getActivity().getResources().getStringArray(R.array.Category5));

        onDetachSpinnerListener();

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categoryWorkUa);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1); // The drop down view
        setting_search_work_ua.setAdapter(spinnerArrayAdapter);

        ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categoryRabotaUa);
        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_list_item_1); // The drop down view
        setting_search_rabota_ua.setAdapter(spinnerArrayAdapter2);

        ArrayAdapter<String> spinnerArrayAdapter3 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categoryHhUa);
        spinnerArrayAdapter3.setDropDownViewResource(android.R.layout.simple_list_item_1); // The drop down view
        setting_search_hh_ua.setAdapter(spinnerArrayAdapter3);

        ArrayAdapter<String> spinnerArrayAdapter4 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categoryOlxUa);
        spinnerArrayAdapter4.setDropDownViewResource(android.R.layout.simple_list_item_1); // The drop down view
        setting_search_olx_ua.setAdapter(spinnerArrayAdapter4);

        ArrayAdapter<String> spinnerArrayAdapter5 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categoryJobsUa);
        spinnerArrayAdapter5.setDropDownViewResource(android.R.layout.simple_list_item_1); // The drop down view
        setting_search_jobs_ua.setAdapter(spinnerArrayAdapter5);

        //***
        setting_search_work_ua.setSelection(settingModels[0].getCategorySelectedIndex(), false);
        setting_search_rabota_ua.setSelection(settingModels[1].getCategorySelectedIndex(), false);
        setting_search_hh_ua.setSelection(settingModels[2].getCategorySelectedIndex(), false);
        setting_search_olx_ua.setSelection(settingModels[3].getCategorySelectedIndex(), false);
        setting_search_jobs_ua.setSelection(settingModels[4].getCategorySelectedIndex(), false);
        //***
        onItemSelectedListenerInit(setting_search_work_ua, 0);
        onItemSelectedListenerInit(setting_search_rabota_ua, 1);
        onItemSelectedListenerInit(setting_search_hh_ua, 2);
        onItemSelectedListenerInit(setting_search_olx_ua, 3);
        onItemSelectedListenerInit(setting_search_jobs_ua, 4);
    }

    private void onItemSelectedListenerInit(AppCompatSpinner spinner, int settingIndex) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                settingModels[settingIndex].setCategorySelectedIndex(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void saveWorkUa() {
        categoryIdList = getResources().getStringArray(R.array.CategoryId);
        otherIdList = getResources().getStringArray(R.array.OtherId);
        employmentIdList = getResources().getStringArray(R.array.EmploymentId);
        String[] salaryFromIdList = getResources().getStringArray(R.array.SalaryFromId);
        String[] salaryToIdList = getResources().getStringArray(R.array.SalaryToId);

        RealmList<Boolean> selectedCategory = new RealmList<>();
        int indexSelectCategory = settingModels[0].getCategorySelectedIndex() - 1;
        for (int i = 0; i < categoryIdList.length; i++) {
            selectedCategory.add(i == indexSelectCategory);
        }

        settingModels[0].setCategorySelected(selectedCategory);

        String a = "/?advs=1";
        String b = "";
        b += getParamsSearch("&category=", "+", categoryIdList, toBoolean(selectedCategory));
        b += getParamsSearch("&employment=", "+", employmentIdList, toBoolean(settingModels[0].getEmploymentSelected()));
        b += getParamsSearch("&", "&", otherIdList, toBoolean(settingModels[0].getOtherSelected()));

        if (settingModels[0].getSalaryFromSelected() > 0)
            b += "&" + salaryFromIdList[settingModels[0].getSalaryFromSelected()];
        if (settingModels[0].getSalaryToSelected() > 0)
            b += "&" + salaryToIdList[settingModels[0].getSalaryToSelected()];

        if (!b.equals(""))
            b = a + b;

        settingModel = settingModels[0];

        Utils.savePreference(Utils.SEARCH_WORK_UA, b);
        saveSetting(0);
    }

    private void saveRabotaUa() {
        String b = "";
        settingModel = settingModels[1];
        categoryIdList = getResources().getStringArray(R.array.Category2Id);
        employmentIdList = getResources().getStringArray(R.array.Employment2Id);

        if (settingModel.getSalarySum() != null && !settingModel.getSalarySum().equals(""))
            b += "salary=" + settingModel.getSalarySum();
//            b += "&salary=" + settingModel.getSalarySum();

        if (settingModel.getEmploymentSelectIndex() > 0) {
            if (b.equals(""))
                b += employmentIdList[settingModel.getEmploymentSelectIndex()];
            else
                b += "&" + employmentIdList[settingModel.getEmploymentSelectIndex()];
        }

        if (settingModel.getCategorySelectedIndex() > 0) {
            if (b.equals(""))
                b += "parentId=" + categoryIdList[settingModel.getCategorySelectedIndex()];
            else
                b += "&parentId=" + categoryIdList[settingModel.getCategorySelectedIndex()];
        }
        Utils.savePreference(Utils.SEARCH_RABOTA_UA, b);
        saveSetting(1);
    }

    private void saveHhUa() {
        categoryIdList = getResources().getStringArray(R.array.Category3Id);
        employmentIdList = getResources().getStringArray(R.array.Employment3Id);
        otherIdList = getResources().getStringArray(R.array.Other3Id);

        RealmList<Boolean> selectedCategory = new RealmList<>();
        int indexSelectCategory = settingModels[2].getCategorySelectedIndex() - 1;
        for (int i = 0; i < categoryIdList.length; i++) {
            selectedCategory.add(i == indexSelectCategory);
        }
        settingModels[2].setCategorySelected(selectedCategory);

        settingModel = settingModels[2];

        String b = "";
        b += getParamsSearch("&specialization=", "&specialization=", categoryIdList, toBoolean(settingModel.getCategorySelected()));
        b += getParamsSearch("&", "&", employmentIdList, toBoolean(settingModel.getEmploymentSelected()));
        b += "&" + otherIdList[settingModel.getOtherSelectedIndex()];

        if (settingModel.getSalarySum() != null && !settingModel.getSalarySum().equals(""))
            b += "&salary=" + settingModel.getSalarySum();

        Utils.savePreference(Utils.SEARCH_HH_UA, b);

        saveSetting(2);
    }

    private void saveOlxUa() {
        settingModel = settingModels[3];
        categoryIdList = getResources().getStringArray(R.array.Category4Id);
        employmentIdList = getResources().getStringArray(R.array.Employment4Id);
        otherIdList = getResources().getStringArray(R.array.Other4Id);

        String b = "";
        // категория
        if (settingModel.getCategorySelectedIndex() > 0)
            b += categoryIdList[settingModel.getCategorySelectedIndex()];

        // тип роботы
        boolean isEmployment = false;
        boolean[] employments = toBoolean(settingModel.getEmploymentSelected());
        if (employments.length >= 3 && (employments[1] || employments[2])) {
            isEmployment = true;
            String s = "/?search[filter_enum_job_type]";
            if (employments[1])
                s += "[0]=" + employmentIdList[1];
            if (employments[1] && employments[2]) {
                s += "&search[filter_enum_job_type][1]=" + employmentIdList[2];
            } else if (employments[2])
                s += "[0]=" + true;

            b += s;
        }

        // Зарплата
        if (settingModel.getSalaryFromSelected() > 0) {
            String s = isEmployment ? "&" : "/?";
            s += "search[filter_float_salary%3Afrom]=" + settingModel.getSalaryFromSelected();
            b += s;
            isEmployment = true;
        }
        if (settingModel.getSalaryToSelected() > 0) {
            String s = isEmployment ? "&" : "/?";
            s += "search[filter_float_salary%3Ato]=" + settingModel.getSalaryToSelected();
            b += s;
            isEmployment = true;
        } //*************

        // тип зайнятости
        boolean[] others = toBoolean(settingModel.getOtherSelected());
        if (others.length >= 3 && (others[1] || others[2])) {
            String s = (isEmployment ? "&" : "/?") + "search[filter_enum_job_timing]";

            if (others[1])
                s += "[0]=" + otherIdList[1];
            if (others[1] && others[2]) {
                s += "&search[filter_enum_job_timing][1]=" + otherIdList[2];
            } else if (others[2]) {
                s += "[0]=" + otherIdList[2];
            }

            b += s;
        }

        Utils.savePreference(Utils.SEARCH_OLX_UA, b);
        saveSetting(3);
    }

    private void saveJobsUa() {
        categoryIdList = getResources().getStringArray(R.array.Category5Id);
        String[] experienceId = getResources().getStringArray(R.array.Experience5Id);
        String[] workGraphId = getResources().getStringArray(R.array.WorkGraph5Id);

        settingModel = settingModels[4];

        String b = "REPLACE?";
        if (settingModel.getCategorySelectedIndex() > 0) {
            b = categoryIdList[settingModel.getCategorySelectedIndex()] + "REPLACE?";
        }
        boolean addEnd = false;
        if (settingModel.getEmploymentSelectIndex() > 0) {
            b += "experience=" + experienceId[settingModel.getEmploymentSelectIndex()];
            addEnd = true;
        }
        if (settingModel.getOtherSelectedIndex() > 0) {
            b += (addEnd ? "&" : "") + "work_graph=" + workGraphId[settingModel.getOtherSelectedIndex()];
            addEnd = true;
        }
        if (settingModel.getSalaryFromSelected() > 0 || settingModel.getSalaryToSelected() > 0) {
            b += (addEnd ? "&" : "") + "salary=" + settingModel.getSalaryFromSelected() +
                    "%2C" + (settingModel.getSalaryToSelected() > 0 ? settingModel.getSalaryToSelected() : 100000);
        }


        Utils.savePreference(Utils.SEARCH_JOBS_UA, b);
        saveSetting(4);
    }

    private String[] addDefaultCategory(String[] category) {
        String[] res = new String[category.length + 1];
        res[0] = getString(R.string.default_category_item);

        System.arraycopy(category, 0, res, 1, category.length);
        return res;
    }

    private void search(FragmentSearchBinding binding, MyAutoCompleteTextView vacancyIn, MyAutoCompleteTextView cityIn) {
        modelInternet.setWiFi(Utils.isWiFiConnect(Objects.requireNonNull(getActivity())));
        binding.setInternet(modelInternet);

        if (!modelInternet.isWiFi()) return;

        String vacancy = vacancyIn.getText().toString();
        String city = cityIn.getText().toString();

        onButtonPressed(vacancy, city, vacancyIn);
    }

    private void onButtonPressed(String vacancy, String city, View view) {
//        if (mListener != null) {
        RegionModel model = autoCompleteTextAdapter.getRegionFromPosition(city);
        if (model == null || !(model.getRuCity().equals(city) || model.getUaCity().equals(city))) {
            ToastHelper.show(getString(R.string.err_city_name));
            return;
        }

        Utils.savePreference(Utils.PREFERENCES_VACANCY_NAME, vacancy.trim());
        Utils.savePreference(Utils.PREFERENCES_CITY_NAME, city.trim());
        DbUtils.addFastSearch(new FastVacancySearch(vacancy.trim()));

        Utils.savePreference(Utils.PREFERENCES_CITY_WORK_ID, model.getWork_ua_city_id());
        Utils.savePreference(Utils.PREFERENCES_CITY_RABOTA_ID, String.valueOf(model.getRabota_ua_city_id()));
        Utils.savePreference(Utils.PREFERENCES_CITY_HH_ID, String.valueOf(model.getHh_ua_city_id()));
        Utils.savePreference(Utils.PREFERENCES_CITY_OLX_ID, String.valueOf(model.getOlx_ua_city_id()));
        Utils.savePreference(Utils.PREFERENCES_CITY_JOBS_ID, model.getJobs_ua_city_id());
//            mListener.onSearchVacancy();
        NavController controller = Navigation.findNavController(view);
        if (controller != null)
            controller.navigate(R.id.tabFragment);
//        }
    }

    private void onShowSetting(FragmentSearchBinding binding, View view) {
        switch (view.getId()) {
            case R.id.more_setting:
                modelInternet.setSettingSearch(true);
                break;
            case R.id.hide_setting:
                modelInternet.setSettingSearch(false);
                break;
        }
        binding.setInternet(modelInternet);
    }

    private void onSaveSetting() {
        saveWorkUa();
        saveRabotaUa();
        saveHhUa();
        saveOlxUa();
        saveJobsUa();
    }

    @Override
    public void saveSetting() {

    }

    @Override
    public int getIdButtonSave() {
        return R.id.btnSaveSettingSearch;
    }

//    @Override
//    public void onAttach(@NonNull Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onPause() {
        super.onPause();
        hidePopupMenu(vacancyIn);
        hidePopupMenu(cityIn);

        if (searchDestroyed != null)
            if (!searchDestroyed.isDisposed())
                searchDestroyed.dispose();

        if (searchVacancyDestroyed != null)
            if (!searchVacancyDestroyed.isDisposed())
                searchVacancyDestroyed.dispose();

        onDetachSpinnerListener();
        Utils.hideKeyboard(this.getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (cityIn != null && autoCompleteTextAdapter != null) {
            Observable<String> searchSubscriber = observableOn(cityIn);
            searchDestroyed = searchSubscriber.subscribe(text -> autoCompleteTextAdapter.search(text),
                    Throwable::printStackTrace
            );
        }
        //************************
        if (vacancyIn != null && autoCompleteTextVacancyAdapter != null) {
            Observable<String> searchSubscriberVacancy = observableOn(vacancyIn);
            searchVacancyDestroyed = searchSubscriberVacancy.subscribe(text -> autoCompleteTextVacancyAdapter.search(text),
                    Throwable::printStackTrace
            );
        }
        readSetting();
    }

    private void onDetachSpinnerListener() {
        clearAdapter(setting_search_work_ua);
        clearAdapter(setting_search_rabota_ua);
        clearAdapter(setting_search_hh_ua);
        clearAdapter(setting_search_olx_ua);
        clearAdapter(setting_search_jobs_ua);
    }

    private void clearAdapter(AppCompatSpinner spinner) {
        spinner.setOnItemSelectedListener(null);
        Context context = getActivity();
        if (context == null) {
            context = getContext();
            if (context == null) {
                spinner.setAdapter(null);
                return;
            }

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, new String[0]);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1); // The drop down view
        spinner.setAdapter(adapter);
    }

    private void hidePopupMenu(MyAutoCompleteTextView textView) {
        if (textView != null) {
            textView.clearFocus();
            textView.dismissDropDown();
        }
    }

    public interface OnFragmentInteractionListener {
        void onSearchVacancy();
    }

}