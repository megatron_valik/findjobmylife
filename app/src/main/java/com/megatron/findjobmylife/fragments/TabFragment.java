package com.megatron.findjobmylife.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.db.models.SettingUaModel;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.utils.ToastHelper;
import com.megatron.findjobmylife.utils.Utils;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment extends Fragment {
    private PagerAdapter adapter;

    public TabFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tab, container, false);

        boolean[] uaModel = DbUtils.readSelectUa();

        TabLayout tabLayout = v.findViewById(R.id.tab_layout);
        if (uaModel[0] || uaModel[1])
            tabLayout.addTab(tabLayout.newTab().setText("WORK.UA"));
        if (uaModel[0] || uaModel[2])
            tabLayout.addTab(tabLayout.newTab().setText("RABOTA.UA"));
        if (uaModel[0] || uaModel[3])
            tabLayout.addTab(tabLayout.newTab().setText("HH.UA"));
        if (uaModel[0] || uaModel[4])
            tabLayout.addTab(tabLayout.newTab().setText("OLX.UA"));
        if (uaModel[0] || uaModel[5])
            tabLayout.addTab(tabLayout.newTab().setText("JOBS.UA"));

        final ViewPager viewPager = v.findViewById(R.id.pager);
        adapter = new PagerAdapter(getChildFragmentManager(), uaModel);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                adapter.setActiveTab(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                adapter.setActiveTab(tab.getPosition(), false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // при первом старте задать индекс, иначе не отображался тост уведомлений
        adapter.setActiveTab(0, true);

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        startReadNetworkBroadcast();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopReadNetworkBroadcast();
    }

    private void startReadNetworkBroadcast() {
        requireActivity().registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void stopReadNetworkBroadcast() {
        try {
            requireActivity().unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mNetworkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Boolean isConnected = Utils.isWiFiConnect(context);
            if (adapter != null) {
                adapter.networkStatusChange(isConnected);
            }
            if (!isConnected) {
                ToastHelper.show(getString(R.string.no_internet_msg));
            }
        }
    };

}
