package com.megatron.findjobmylife.fragments.setting;

import androidx.fragment.app.Fragment;

import android.view.View;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.db.models.SettingModel;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.utils.ToastHelper;

import io.realm.RealmList;

public abstract class AbstractSettingFragment extends Fragment {
    protected SettingModel settingModel;
    protected String[] categoryIdList;
    protected String[] employmentIdList;
    protected String[] otherIdList;

    public abstract void saveSetting();

    public abstract int getIdButtonSave();

    void defaultSetting(int index) {
        settingModel = new SettingModel(index);
        settingModel.setCategorySelected(new RealmList<>());
        settingModel.setEmploymentSelected(new RealmList<>());
        settingModel.setOtherSelected(new RealmList<>());
    }

    SettingModel readSetting(int indexJob) {
        return DbUtils.getSetting(indexJob);
    }

    protected void saveSetting(int index) {
        if (settingModel == null)
            settingModel = new SettingModel(index);
        DbUtils.saveSetting(settingModel);
        ToastHelper.show(getView(), getString(R.string.save_setting_toast));
    }

    RealmList<Boolean> toRealmBool(boolean[] booleans) {
        RealmList<Boolean> res = new RealmList<>();
        for (boolean b : booleans)
            res.add(b);
        return res;
    }

    @SuppressWarnings("ConstantConditions")
    protected boolean[] toBoolean(RealmList<Boolean> realmList) {
        boolean[] res = new boolean[realmList.size()];
        for (int i = 0; i < realmList.size(); i++) {
            try {
                res[i] = realmList.get(i);
            } catch (NullPointerException e) {
                //
            }
        }
        return res;
    }

    protected String getParamsSearch(String first, String indexId, String[] itemsId, boolean[] _selection) {
        boolean foundOne = false;
        StringBuilder sb = new StringBuilder();
        sb.append(first);

        for (int i = 0; i < _selection.length; ++i) {
            if (_selection[i]) {
                if (foundOne) {
                    sb.append(indexId);
                }
                foundOne = true;
                sb.append(itemsId[i]);
            }
        }
        if (!foundOne) return "";

        return sb.toString();
    }

    void initSaveBtn(View v) {
        v.findViewById(getIdButtonSave()).setOnClickListener(v1 -> saveSetting());
    }
}
