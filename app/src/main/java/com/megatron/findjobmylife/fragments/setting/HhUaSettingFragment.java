package com.megatron.findjobmylife.fragments.setting;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.ui.MultiSelectSpinner;
import com.megatron.findjobmylife.utils.Utils;

/**
 * доп. настройки HH.ЮА
 */
public class HhUaSettingFragment extends AbstractSettingFragment {

    public HhUaSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hh_ua_setting, container, false);
        settingModel = readSetting(2);
        boolean isSave = settingModel != null;
        if (settingModel == null) {
            defaultSetting(2);
        }

        categoryIdList = getResources().getStringArray(R.array.Category3Id);
        employmentIdList = getResources().getStringArray(R.array.Employment3Id);
        otherIdList = getResources().getStringArray(R.array.Other3Id);

        try {
            MultiSelectSpinner category_hh_ua = v.findViewById(R.id.category_hh_ua);
            category_hh_ua.setItems(getResources().getStringArray(R.array.Category3));
            if (isSave)
                category_hh_ua.setSelection(toBoolean(settingModel.getCategorySelected()));
            category_hh_ua.setChangeListener(selectedItems -> settingModel.setCategorySelected(toRealmBool(selectedItems)));
        } catch (Exception e) {
            Utils.logException(e);
        }
        try {
            MultiSelectSpinner type_job_hh_ua = v.findViewById(R.id.employment_hh_ua);
            type_job_hh_ua.setItems(getResources().getStringArray(R.array.Employment3));
            if (isSave)
                type_job_hh_ua.setSelection(toBoolean(settingModel.getEmploymentSelected()));
            type_job_hh_ua.setChangeListener(selectedItems -> settingModel.setEmploymentSelected(toRealmBool(selectedItems)));
        } catch (Exception e) {
            Utils.logException(e);
        }
        try {
            AppCompatSpinner other_hh_ua = v.findViewById(R.id.other_hh_ua);
            if (isSave)
                other_hh_ua.setSelection(settingModel.getOtherSelectedIndex());
            other_hh_ua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    settingModel.setOtherSelectedIndex(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }
        try {
            AppCompatEditText money_hh_ua = v.findViewById(R.id.money_hh_ua);
            money_hh_ua.setText(settingModel.getSalarySum());
            money_hh_ua.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    settingModel.setSalarySum(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }
        initSaveBtn(v);
        return v;
    }

    @Override
    public void saveSetting() {
        String b = "";
        b += getParamsSearch("&specialization=", "&specialization=", categoryIdList, toBoolean(settingModel.getCategorySelected()));
        b += getParamsSearch("&", "&", employmentIdList, toBoolean(settingModel.getEmploymentSelected()));
        b += "&" + otherIdList[settingModel.getOtherSelectedIndex()];

        if (settingModel.getSalarySum() != null && !settingModel.getSalarySum().equals(""))
            b += "&salary=" + settingModel.getSalarySum();

        Utils.savePreference(Utils.SEARCH_HH_UA, b);

        saveSetting(2);
    }

    @Override
    public int getIdButtonSave() {
        return R.id.btnSaveSettingHhUa;
    }
}
