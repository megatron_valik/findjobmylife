package com.megatron.findjobmylife.fragments.setting;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.utils.Utils;

public class JobsUaSettingFragment extends AbstractSettingFragment {
    private String[] experienceId;
    private String[] workGraphId;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_jobs_ua_setting, container, false);
        settingModel = readSetting(4);
        if (settingModel == null) {
            defaultSetting(4);
        }
        categoryIdList = getResources().getStringArray(R.array.Category5Id);
        experienceId = getResources().getStringArray(R.array.Experience5Id); // досвід роботи
        workGraphId = getResources().getStringArray(R.array.WorkGraph5Id); // графік роботи

        AppCompatSpinner category_jobs_ua = v.findViewById(R.id.category_jobs_ua);
        AppCompatSpinner work_graph_jobs_ua = v.findViewById(R.id.work_graph_jobs_ua);
        AppCompatSpinner experience_jobs_ua = v.findViewById(R.id.experience_jobs_ua);

        try {
            category_jobs_ua.setSelection(settingModel.getCategorySelectedIndex());
            category_jobs_ua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    settingModel.setCategorySelectedIndex(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            // Графік роботи
            work_graph_jobs_ua.setSelection(settingModel.getOtherSelectedIndex());
            work_graph_jobs_ua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    settingModel.setOtherSelectedIndex(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            //досвыд роботи = other
            experience_jobs_ua.setSelection(settingModel.getEmploymentSelectIndex());
            experience_jobs_ua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    settingModel.setEmploymentSelectIndex(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            // зарплата від
            AppCompatEditText money_from_jobs_ua = v.findViewById(R.id.money_from_jobs_ua);
            int sumFrom = settingModel.getSalaryFromSelected();
            money_from_jobs_ua.setText(sumFrom > 0 ? sumFrom + "" : "");
            money_from_jobs_ua.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int value = Utils.strToInt(charSequence.toString());
                    settingModel.setSalaryFromSelected(value);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            // зарплата до
            AppCompatEditText money_to_jobs_ua = v.findViewById(R.id.money_to_jobs_ua);
            int sumTo = settingModel.getSalaryToSelected();
            money_to_jobs_ua.setText(sumTo > 0 ? sumTo + "" : "");
            money_to_jobs_ua.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int value = Utils.strToInt(charSequence.toString());
                    settingModel.setSalaryToSelected(value);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        initSaveBtn(v);
        return v;
    }

    @Override
    public void saveSetting() {
        String b = "REPLACE?";
        if (settingModel.getCategorySelectedIndex() > 0) {
            b = categoryIdList[settingModel.getCategorySelectedIndex()] + "REPLACE?";
        }
        boolean addEnd = false;
        if (settingModel.getEmploymentSelectIndex() > 0) {
            b += "experience=" + experienceId[settingModel.getEmploymentSelectIndex()];
            addEnd = true;
        }
        if (settingModel.getOtherSelectedIndex() > 0) {
            b += (addEnd ? "&" : "") + "work_graph=" + workGraphId[settingModel.getOtherSelectedIndex()];
            addEnd = true;
        }
        if (settingModel.getSalaryFromSelected() > 0 || settingModel.getSalaryToSelected() > 0) {
            b += (addEnd ? "&" : "") + "salary=" + settingModel.getSalaryFromSelected() +
                    "%2C" + (settingModel.getSalaryToSelected() > 0 ? settingModel.getSalaryToSelected() : 100000);
        }

        Utils.savePreference(Utils.SEARCH_JOBS_UA, b);
        saveSetting(4);
    }

    @Override
    public int getIdButtonSave() {
        return R.id.btnSaveSettingJobsUa;
    }
}
