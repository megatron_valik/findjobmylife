package com.megatron.findjobmylife.fragments.setting;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.db.models.SettingUaModel;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.ui.MultiSelectSpinner;
import com.megatron.findjobmylife.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainSettingFragment extends Fragment {

    public MainSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main_setting, container, false);
        v.findViewById(R.id.btnSettingVacancy).setOnClickListener(this::replaceFragment);

        v.findViewById(R.id.btnSettingClearCash).setOnClickListener(view -> Utils.clearCash());

        v.findViewById(R.id.btnSettingClearFavorite).setOnClickListener(view -> Utils.clearFavorite());

        v.findViewById(R.id.btnSettingClearFastSearch).setOnClickListener(view -> Utils.clearFastSearch());

        v.findViewById(R.id.btnSettingClearRemoved).setOnClickListener(view -> Utils.clearRemovedVacancy());

        String all = getResources().getString(R.string.all_ua);
        String[] ua = new String[]{all, "WORK.UA", "RABOTA.UA", "HH.UA", "OLX.UA", "JOBS.UA"};
        MultiSelectSpinner sp = v.findViewById(R.id.sp_setting_ua);
        sp.setItems(ua);
        sp.setFirstActiveIfEmpty(true);
        sp.setSelection(DbUtils.readSelectUa());
        sp.setChangeListener(selectedItems -> {
            SettingUaModel uaModel = new SettingUaModel();
            uaModel.setSelected(selectedItems);
            DbUtils.writeSelectedUa(uaModel);
        });

        SwitchCompat switchAnimate = v.findViewById(R.id.switchAnimate);
        boolean checkAnimate = Utils.stringToBool(Utils.readPreference(Utils.ANIMATE_LIST), true);
        switchAnimate.setChecked(checkAnimate);
        switchAnimate.setOnCheckedChangeListener((compoundButton, b) -> Utils.savePreference(Utils.ANIMATE_LIST, String.valueOf(b)));

        return v;
    }

    private void replaceFragment(View view) {
        Navigation.findNavController(view).navigate(R.id.settingUa);
    }

}
