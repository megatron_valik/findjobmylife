package com.megatron.findjobmylife.fragments.setting;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.ui.MultiSelectSpinner;
import com.megatron.findjobmylife.utils.Utils;

/**
 * Доп. настройки OLX
 */
public class OlxUaSettingFragment extends AbstractSettingFragment {

    public OlxUaSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_olx_ua_setting, container, false);
        settingModel = readSetting(3);
        categoryIdList = getResources().getStringArray(R.array.Category4Id);
        employmentIdList = getResources().getStringArray(R.array.Employment4Id); // тип занятости
        otherIdList = getResources().getStringArray(R.array.Other4Id); // тип работы

        if (settingModel == null) {
            defaultSetting(3);
        }

        try {
            if (settingModel.getEmploymentSelected().size() == 0)
                settingModel.setEmploymentSelected(toRealmBool(new boolean[]{true, false, false}));

            if (settingModel.getOtherSelected().size() == 0)
                settingModel.setOtherSelected(toRealmBool(new boolean[]{true, false, false}));
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            //категория
            AppCompatSpinner category_olx_ua = v.findViewById(R.id.category_olx_ua);
            category_olx_ua.setSelection(settingModel.getCategorySelectedIndex());
            category_olx_ua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    settingModel.setCategorySelectedIndex(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            // вид зайнятості
            MultiSelectSpinner type_job_olx_ua = v.findViewById(R.id.employment);
            type_job_olx_ua.setFirstActiveIfEmpty(true);
            type_job_olx_ua.setItems(getResources().getStringArray(R.array.Employment4));
            type_job_olx_ua.setSelection(toBoolean(settingModel.getEmploymentSelected()));
            type_job_olx_ua.setChangeListener(selectedItems -> settingModel.setEmploymentSelected(toRealmBool(selectedItems)));
        } catch (Resources.NotFoundException e) {
            Utils.logException(e);
        }

        try {
            // інше
            MultiSelectSpinner other_olx_ua = v.findViewById(R.id.other_olx_ua);
            other_olx_ua.setFirstActiveIfEmpty(true);
            other_olx_ua.setItems(getResources().getStringArray(R.array.Other4));
            other_olx_ua.setSelection(toBoolean(settingModel.getOtherSelected()));
            other_olx_ua.setChangeListener(selectedItems -> settingModel.setOtherSelected(toRealmBool(selectedItems)));
        } catch (Resources.NotFoundException e) {
            Utils.logException(e);
        }

        try {
            // зарплата від
            AppCompatEditText money_from_olx_ua = v.findViewById(R.id.money_from_olx_ua);
            int sumFrom = settingModel.getSalaryFromSelected();
            money_from_olx_ua.setText(sumFrom > 0 ? sumFrom + "" : "");
            money_from_olx_ua.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int value = Utils.strToInt(charSequence.toString());
                    settingModel.setSalaryFromSelected(value);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            // зарплата до
            AppCompatEditText money_to_olx_ua = v.findViewById(R.id.money_to_olx_ua);
            int sumTo = settingModel.getSalaryToSelected();
            money_to_olx_ua.setText(sumTo > 0 ? sumTo + "" : "");
            money_to_olx_ua.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int value = Utils.strToInt(charSequence.toString());
                    settingModel.setSalaryToSelected(value);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        initSaveBtn(v);

        return v;
    }

    @Override
    public void saveSetting() {
        String b = "";
        // категория
        if (settingModel.getCategorySelectedIndex() > 0)
            b += categoryIdList[settingModel.getCategorySelectedIndex()];

        // тип роботы
        boolean isEmployment = false;
        boolean[] employments = toBoolean(settingModel.getEmploymentSelected());
        if (employments[1] || employments[2]) {
            isEmployment = true;
            String s = "/?search[filter_enum_job_type]";
            if (employments[1])
                s += "[0]=" + employmentIdList[1];
            if (employments[1] && employments[2]) {
                s += "&search[filter_enum_job_type][1]=" + employmentIdList[2];
            } else if (employments[2])
                s += "[0]=" + true;

            b += s;
        }

        // Зарплата
        if (settingModel.getSalaryFromSelected() > 0) {
            String s = isEmployment ? "&" : "/?";
            s += "search[filter_float_salary%3Afrom]=" + settingModel.getSalaryFromSelected();
            b += s;
            isEmployment = true;
        }
        if (settingModel.getSalaryToSelected() > 0) {
            String s = isEmployment ? "&" : "/?";
            s += "search[filter_float_salary%3Ato]=" + settingModel.getSalaryToSelected();
            b += s;
            isEmployment = true;
        } //*************

        // тип зайнятости
        boolean[] others = toBoolean(settingModel.getOtherSelected());
        if (others[1] || others[2]) {
            String s = (isEmployment ? "&" : "/?") + "search[filter_enum_job_timing]";

            if (others[1])
                s += "[0]=" + otherIdList[1];
            if (others[1] && others[2]) {
                s += "&search[filter_enum_job_timing][1]=" + otherIdList[2];
            } else if (others[2]) {
                s += "[0]=" + otherIdList[2];
            }

            b += s;
        }

        Utils.savePreference(Utils.SEARCH_OLX_UA, b);
        saveSetting(3);
    }

    @Override
    public int getIdButtonSave() {
        return R.id.btnSaveSettingOlxUa;
    }
}
