package com.megatron.findjobmylife.fragments.setting;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.utils.Utils;

/**
 * доп. настройки РАБОТА.ЮА
 */
public class RabotaUaSettingFragment extends AbstractSettingFragment {

    public RabotaUaSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_rabota_ua_setting, container, false);
        settingModel = readSetting(1);
        if (settingModel == null) {
            defaultSetting(1);
        }

        categoryIdList = getResources().getStringArray(R.array.Category2Id);
        employmentIdList = getResources().getStringArray(R.array.Employment2Id);

        try {
            AppCompatSpinner category_rabota_ua = v.findViewById(R.id.category_rabota_ua);
            category_rabota_ua.setSelection(settingModel.getCategorySelectedIndex());
            category_rabota_ua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    settingModel.setCategorySelectedIndex(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            AppCompatSpinner employment_rabota_ua = v.findViewById(R.id.employment_rabota_ua);
            employment_rabota_ua.setSelection(settingModel.getEmploymentSelectIndex());
            employment_rabota_ua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    settingModel.setEmploymentSelectIndex(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        try {
            AppCompatEditText money_rabota_ua = v.findViewById(R.id.money_rabota_ua);
            money_rabota_ua.setText(settingModel.getSalarySum());
            money_rabota_ua.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    settingModel.setSalarySum(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            Utils.logException(e);
        }

        initSaveBtn(v);
        return v;
    }

    @Override
    public void saveSetting() {
        String b = "";
        String[] params = new String[]{"", "", ""};

        if (settingModel.getSalarySum() != null && !settingModel.getSalarySum().equals(""))
//            b += "&salary=" + settingModel.getSalarySum();
            params[0] = "salary=" + settingModel.getSalarySum();

        if (settingModel.getEmploymentSelectIndex() > 0) {
//            b += "&" + employmentIdList[settingModel.getEmploymentSelectIndex()];
            params[1] = employmentIdList[settingModel.getEmploymentSelectIndex()];
        }

        if (settingModel.getCategorySelectedIndex() > 0) {
//            b += "&parentId=" + categoryIdList[settingModel.getCategorySelectedIndex()];
            params[2] = "parentId=" + categoryIdList[settingModel.getCategorySelectedIndex()];
        }
        b += Utils.getParameters(params).replace("?", "");

        Utils.savePreference(Utils.SEARCH_RABOTA_UA, b);
        saveSetting(1);
    }

    @Override
    public int getIdButtonSave() {
        return R.id.btnSaveSettingRabotaUa;
    }


}
