package com.megatron.findjobmylife.fragments.setting;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.ui.MultiSelectSpinner;
import com.megatron.findjobmylife.utils.Utils;

import java.util.Objects;

/**
 * доп. настройки ВОРК.ЮА
 */
public class WorkUaSettingFragment extends AbstractSettingFragment {
    private String[] salaryFromIdList;
    private String[] salaryToIdList;

    public WorkUaSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_work_ua_setting, container, false);
        settingModel = readSetting(0);
        boolean isSave = settingModel != null;
        if (settingModel == null) {
            defaultSetting(0);
        }

        categoryIdList = getResources().getStringArray(R.array.CategoryId);
        employmentIdList = getResources().getStringArray(R.array.EmploymentId);
        otherIdList = getResources().getStringArray(R.array.OtherId);
        salaryFromIdList = getResources().getStringArray(R.array.SalaryFromId);
        salaryToIdList = getResources().getStringArray(R.array.SalaryToId);

        try {
            MultiSelectSpinner category_work_ua = v.findViewById(R.id.category_work_ua);
            category_work_ua.setItems(getResources().getStringArray(R.array.Category));
            if (isSave)
                category_work_ua.setSelection(toBoolean(settingModel.getCategorySelected()));
            category_work_ua.setChangeListener(selectedItems -> settingModel.setCategorySelected(toRealmBool(selectedItems)));
        } catch (Resources.NotFoundException e) {
            Utils.logException(e);
        }

        try {
            MultiSelectSpinner type_job_work_ua = v.findViewById(R.id.employment);
            type_job_work_ua.setItems(getResources().getStringArray(R.array.Employment));
            if (isSave)
                type_job_work_ua.setSelection(toBoolean(settingModel.getEmploymentSelected()));
            type_job_work_ua.setChangeListener(selectedItems -> settingModel.setEmploymentSelected(toRealmBool(selectedItems)));
        } catch (Resources.NotFoundException e) {
            Utils.logException(e);
        }

        try {
            MultiSelectSpinner other_work_ua = v.findViewById(R.id.other_work_ua);
            other_work_ua.setItems(getResources().getStringArray(R.array.Other));
            if (isSave)
                other_work_ua.setSelection(toBoolean(settingModel.getOtherSelected()));
            other_work_ua.setChangeListener(selectedItems -> settingModel.setOtherSelected(toRealmBool(selectedItems)));
        } catch (Resources.NotFoundException e) {
            Utils.logException(e);
        }

        initSaveBtn(v);
        salaryChangeInit(v);
        return v;
    }

    private void salaryChangeInit(View v) {
        String[] salaryFrom = getResources().getStringArray(R.array.SalaryFrom);
        String[] salaryTo = getResources().getStringArray(R.array.SalaryFrom);

        AppCompatSpinner spinnerFrom = v.findViewById(R.id.money_from_work_ua);
        AppCompatSpinner spinnerTo = v.findViewById(R.id.money_to_work_ua);

        int posFrom = settingModel.getSalaryFromSelected();
        int posTo = settingModel.getSalaryToSelected();

        ArrayAdapter<String> fromAdapter = getAdapter(salaryFrom);
        spinnerFrom.setAdapter(fromAdapter);

        ArrayAdapter<String> toAdapter = getAdapter(salaryTo);
        spinnerTo.setAdapter(toAdapter);

        spinnerFrom.setSelection(posFrom);
        spinnerTo.setSelection(posTo);

        spinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= spinnerTo.getSelectedItemPosition() && position > 0 &&
                        spinnerTo.getSelectedItemPosition() > 0)
                    spinnerTo.setSelection(position + 1);
                settingModel.setSalaryFromSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position <= spinnerFrom.getSelectedItemPosition() && position > 0 &&
                        spinnerFrom.getSelectedItemPosition() > 0)
                    spinnerFrom.setSelection(0);
                settingModel.setSalaryToSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private ArrayAdapter<String> getAdapter(String[] mass) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), android.R.layout.simple_spinner_item, mass);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1); // The drop down view
        return adapter;
    }

    @Override
    public void saveSetting() {
        String a = "/?advs=1";
        String b = "";
        b += getParamsSearch("&category=", "+", categoryIdList, toBoolean(settingModel.getCategorySelected()));
        b += getParamsSearch("&employment=", "+", employmentIdList, toBoolean(settingModel.getEmploymentSelected()));
        b += getParamsSearch("&", "&", otherIdList, toBoolean(settingModel.getOtherSelected()));

        if (settingModel.getSalaryFromSelected() > 0)
            b += "&" + salaryFromIdList[settingModel.getSalaryFromSelected()];
        if (settingModel.getSalaryToSelected() > 0)
            b += "&" + salaryToIdList[settingModel.getSalaryToSelected()];

        if (!b.equals(""))
            b = a + b;
        Utils.savePreference(Utils.SEARCH_WORK_UA, b);
        saveSetting(0);
    }

    @Override
    public int getIdButtonSave() {
        return R.id.btnSaveSettingWorkUa;
    }
}
