package com.megatron.findjobmylife.fragments.tabFragments;

import com.megatron.findjobmylife.fragments.AbstractUaFragment;
import com.megatron.findjobmylife.parser.ParserFabric;
import com.megatron.findjobmylife.utils.Utils;

public class HhUaFragment extends AbstractUaFragment {

    public HhUaFragment() {
        // Required empty public constructor
    }

    @Override
    public int getFragmentIndex() {
        return ParserFabric.HH_UA;
    }

    @Override
    public String getIdCity() {
        return Utils.readPreference(Utils.PREFERENCES_CITY_HH_ID);
    }

    @Override
    public String getParams() {
        return Utils.readPreference(Utils.SEARCH_HH_UA);
    }


}
