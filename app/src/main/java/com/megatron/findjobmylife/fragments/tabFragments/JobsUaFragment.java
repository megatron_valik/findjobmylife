package com.megatron.findjobmylife.fragments.tabFragments;

import com.megatron.findjobmylife.fragments.AbstractUaFragment;
import com.megatron.findjobmylife.parser.ParserFabric;
import com.megatron.findjobmylife.utils.Utils;

public class JobsUaFragment extends AbstractUaFragment {

    public JobsUaFragment() {
    }

    @Override
    public int getFragmentIndex() {
        return ParserFabric.JOBS_UA;
    }

    @Override
    public String getIdCity() {
        return Utils.readPreference(Utils.PREFERENCES_CITY_JOBS_ID);
    }

    @Override
    public String getParams() {
        return Utils.readPreference(Utils.SEARCH_JOBS_UA);
    }
}
