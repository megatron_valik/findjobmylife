package com.megatron.findjobmylife.fragments.tabFragments;

import com.megatron.findjobmylife.fragments.AbstractUaFragment;
import com.megatron.findjobmylife.parser.ParserFabric;
import com.megatron.findjobmylife.utils.Utils;

public class OlxUaFragment extends AbstractUaFragment {
    @Override
    public int getFragmentIndex() {
        return ParserFabric.OLX_UA;
    }

    @Override
    public String getIdCity() {
        return Utils.readPreference(Utils.PREFERENCES_CITY_OLX_ID);
    }

    @Override
    public String getParams() {
        return Utils.readPreference(Utils.SEARCH_OLX_UA);
    }
}
