package com.megatron.findjobmylife.fragments.tabFragments;

import androidx.fragment.app.Fragment;

import com.megatron.findjobmylife.fragments.AbstractUaFragment;
import com.megatron.findjobmylife.parser.ParserFabric;
import com.megatron.findjobmylife.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class RabotaUaFragment extends AbstractUaFragment {
    public RabotaUaFragment() {
        // Required empty public constructor
    }

    @Override
    public int getFragmentIndex() {
        return ParserFabric.RABOTA_UA;
    }

    @Override
    public String getIdCity() {
        return Utils.readPreference(Utils.PREFERENCES_CITY_RABOTA_ID);
    }

    @Override
    public String getParams() {
        return Utils.readPreference(Utils.SEARCH_RABOTA_UA);
    }

}
