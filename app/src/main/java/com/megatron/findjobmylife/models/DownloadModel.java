package com.megatron.findjobmylife.models;

import androidx.databinding.BaseObservable;

public class DownloadModel extends BaseObservable {
    private boolean downloading;
    private boolean emptyShow;

    public boolean isDownloading() {
        return downloading;
    }

    public void setDownloading(boolean downloading) {
        this.downloading = downloading;
    }

    public boolean isEmptyShow() {
        return emptyShow;
    }

    public void setEmptyShow(boolean emptyShow) {
        this.emptyShow = emptyShow;
    }
}
