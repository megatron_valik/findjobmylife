package com.megatron.findjobmylife.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.megatron.findjobmylife.models.hh.Item;

public class HhVacancyModel {
    @SerializedName("items")
    public List<Item> items = null;

    @SerializedName("found")
    public Integer found;

    @SerializedName("pages")
    public Integer pages;

    @SerializedName("per_page")
    public Integer perPage;

    @SerializedName("page")
    public Integer page;

    @SerializedName("clusters")
    public Object clusters;

    @SerializedName("arguments")
    public Object arguments;

    @SerializedName("alternate_url")
    public String alternateUrl;
/*
    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Integer getFound() {
        return found;
    }

    public void setFound(Integer found) {
        this.found = found;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Object getClusters() {
        return clusters;
    }

    public void setClusters(Object clusters) {
        this.clusters = clusters;
    }

    public Object getArguments() {
        return arguments;
    }

    public void setArguments(Object arguments) {
        this.arguments = arguments;
    }

    public String getAlternateUrl() {
        return alternateUrl;
    }

    public void setAlternateUrl(String alternateUrl) {
        this.alternateUrl = alternateUrl;
    }
*/
}
