package com.megatron.findjobmylife.models;

import androidx.databinding.BaseObservable;
import android.graphics.Bitmap;

public class InfoModel extends BaseObservable {
    private Bitmap image;
    private String text;

    public InfoModel(Bitmap image, String text) {
        this.image = image;
        this.text = text;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getText() {
        return text;
    }
}
