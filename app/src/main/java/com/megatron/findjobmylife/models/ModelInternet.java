package com.megatron.findjobmylife.models;

import androidx.databinding.BaseObservable;

public class ModelInternet extends BaseObservable {
    private boolean isWiFi = true;
    private boolean settingSearch = false;

    public boolean isWiFi() {
        return isWiFi;
    }

    public void setWiFi(boolean wiFi) {
        isWiFi = wiFi;
    }

    public boolean isSettingSearch() {
        return settingSearch;
    }

    public void setSettingSearch(boolean settingSearch) {
        this.settingSearch = settingSearch;
    }
}
