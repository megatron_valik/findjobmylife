package com.megatron.findjobmylife.models;

public class RegionModel {
    private String uaCity;
    private String ruCity;
    private String work_ua_city_id;
    private String rabota_ua_city_id;
    private String hh_ua_city_id;
    private String olx_ua_city_id;
    private String jobs_ua_city_id;

    public RegionModel(String uaCity, String ruCity, String work_ua_city_id, String rabota_ua_city_id,
                       String hh_ua_city_id, String olx_ua_city_id, String jobs_ua_city_id) {
        this.uaCity = uaCity;
        this.ruCity = ruCity;
        this.work_ua_city_id = work_ua_city_id;
        this.rabota_ua_city_id = rabota_ua_city_id;
        this.hh_ua_city_id = hh_ua_city_id;
        this.olx_ua_city_id = olx_ua_city_id;
        this.jobs_ua_city_id = jobs_ua_city_id;
    }

    public String getUaCity() {
        return uaCity;
    }

    public String getRuCity() {
        return ruCity;
    }

    public String getWork_ua_city_id() {
        return work_ua_city_id;
    }

    public String getRabota_ua_city_id() {
        return rabota_ua_city_id;
    }

    public String getHh_ua_city_id() {
        return hh_ua_city_id;
    }

    public String getOlx_ua_city_id() {
        return olx_ua_city_id;
    }

    public String getJobs_ua_city_id() {
        return jobs_ua_city_id;
    }
}
