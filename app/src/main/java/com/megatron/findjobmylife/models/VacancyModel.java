package com.megatron.findjobmylife.models;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.megatron.findjobmylife.App;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.utils.Utils;

public class VacancyModel extends BaseObservable implements InterfaceVacancyModel {
    private String title;
    private String subTitle;
    private Bitmap image;
    private String vipTitle;
    private String cityName;
    private String overflow;
    private String url;
    private String urlImage;

    private boolean vip;
    private boolean hasImage;
    private boolean emptyVacancy = false;

    private boolean previewVacancy = false; // была просмотрена вакансия
    private boolean favorite; // добавлено в избранное

    public VacancyModel(String title, String subTitle, String cityName, String image, String vip, String overflow, String url) {
        this.title = title == null ? "" : title;
        this.subTitle = subTitle == null ? "" : subTitle;
        this.cityName = cityName == null ? "" : cityName;
        this.vipTitle = vip == null ? "" : vip;
        this.vip = vip != null && vip.equals("VIP");
        this.overflow = overflow == null ? "" : overflow;
        this.url = url == null ? "" : url;
        this.urlImage = image == null ? "" : image;

        hasImage = image != null && !image.equals("");
        if (hasImage)
            downloadBitmap(image);
        changeFavorite();
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public String getCityName() {
        return cityName;
    }

    private void setImage(Bitmap image) {
        this.image = image;
        if (image != null)
            notifyPropertyChanged(BR.image);
    }

    public String getVipTitle() {
        return vipTitle;
    }

    public boolean getVip() {
        return vip;
    }

    @Bindable
    public Bitmap getImage() {
        if (hasImage && image == null) {
            downloadBitmap(urlImage);
        }
        return image;
    }

    public String getOverflow() {
        return overflow;
    }

    public String getUrl() {
        return url;
    }

    public boolean getHasImage() {
        return hasImage;
    }

    public boolean getEmptyVacancy() {
        return emptyVacancy;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setEmptyVacancy(Boolean emptyVacancy) {
        this.emptyVacancy = emptyVacancy;
    }

    @Bindable
    public boolean isPreviewVacancy() {
        return previewVacancy;
    }

    public void setPreviewVacancy(boolean previewVacancy) {
        this.previewVacancy = previewVacancy;
        notifyPropertyChanged(BR.previewVacancy);
    }

    @Bindable
    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
        notifyPropertyChanged(BR.favorite);
    }

    private void changeFavorite() {
        setFavorite(DbUtils.isFavoritesVacancy(url));
    }

    public void clearImage() {
        image = null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof VacancyModel)) return false;
        VacancyModel model = (VacancyModel) obj;
        return model.getUrl().equals(this.url);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append(title).append(" ").append(subTitle).append(" ").append(cityName);
        if (urlImage != null)
            b.append(" ").append(urlImage);

        return b.toString();
    }

    private void downloadBitmap(String url) {
        String urlDisplay = url;
        if (!urlDisplay.startsWith("http"))
            urlDisplay = "https:" + urlDisplay;

        Handler uiHandler = new Handler(Looper.getMainLooper());
        String finalUrlDisplay = urlDisplay;
        uiHandler.post(() ->
                        Glide.with(App.getContext())
                                .asBitmap()
                                .load(finalUrlDisplay)
                                .listener(new RequestListener<Bitmap>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
//                                        try {
//                                            String url = (String) model;
//                                            loadImage(url);
//                                        } catch (Exception ex) {
//                                            ex.printStackTrace();
//                                        } catch (OutOfMemoryError ex) {
//                                            ex.printStackTrace();
//                                        }
//                                        return false;
                                        return true;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                        return false;
                                    }
                                })
                                .into(new SimpleTarget<Bitmap>(350, 350) {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                        setImage(Utils.makeTransparent(resource));
                                    }
                                })
//                Glide.with(App.getContext())
//                        .asBitmap()
//                        .load(finalUrlDisplay)
//                        .into(new SimpleTarget<Bitmap>() {
//                            @Override
//                            public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> transition) {
//                                setImage(Utils.makeTransparent(resource));
//                            }
//
//                            @Override
//                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
//                                super.onLoadFailed(errorDrawable);
//                            }
//                        })
        );
    }

//    private void loadImage(String src) {
//        Handler uiHandler = new Handler(Looper.getMainLooper());
//        uiHandler.post(() -> {
//            Bitmap bmp = getBitmapFromURL(src);
//            if (bmp != null)
//                setImage(Utils.makeTransparent(bmp));
//        });
//    }

//    private Bitmap getBitmapFromURL(String src) {
//        try {
//            URL url = new URL(src);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            return BitmapFactory.decodeStream(input);
//        } catch (IOException e) {
//            return null;
//        } catch (Exception e) {
//            return null;
//        } catch (OutOfMemoryError e) {
//            return null;
//        }
//    }

}