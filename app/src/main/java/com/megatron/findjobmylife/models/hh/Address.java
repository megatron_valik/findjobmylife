package com.megatron.findjobmylife.models.hh;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("city")
    public String city;

    @SerializedName("street")
    public String street;

    @SerializedName("building")
    public String building;

    @SerializedName("description")
    public Object description;

    @SerializedName("lat")
    public Double lat;

    @SerializedName("lng")
    public Double lng;

    @SerializedName("raw")
    public Object raw;

    @SerializedName("metro")
    public Object metro;

    @SerializedName("metro_stations")
    public List<Object> metroStations = null;

    @SerializedName("id")
    public String id;
/*
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Object getRaw() {
        return raw;
    }

    public void setRaw(Object raw) {
        this.raw = raw;
    }

    public Object getMetro() {
        return metro;
    }

    public void setMetro(Object metro) {
        this.metro = metro;
    }

    public List<Object> getMetroStations() {
        return metroStations;
    }

    public void setMetroStations(List<Object> metroStations) {
        this.metroStations = metroStations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
*/
}
