package com.megatron.findjobmylife.models.hh;

import com.google.gson.annotations.SerializedName;

public class Area {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("url")
    public String url;
/*
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
*/
}
