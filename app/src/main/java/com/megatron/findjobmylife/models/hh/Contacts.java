package com.megatron.findjobmylife.models.hh;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Contacts {

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("phones")
    public List<Phone> phones = null;
/*
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }
*/
}
