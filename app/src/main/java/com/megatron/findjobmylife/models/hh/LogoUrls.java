package com.megatron.findjobmylife.models.hh;

import com.google.gson.annotations.SerializedName;

public class LogoUrls {

    @SerializedName("240")
    public String _240;

    @SerializedName("90")
    public String _90;

    @SerializedName("original")
    public String original;
/*
    public String get240() {
        return _240;
    }

    public void set240(String _240) {
        this._240 = _240;
    }

    public String get90() {
        return _90;
    }

    public void set90(String _90) {
        this._90 = _90;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
*/
}
