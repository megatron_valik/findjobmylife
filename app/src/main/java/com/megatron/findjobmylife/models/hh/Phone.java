package com.megatron.findjobmylife.models.hh;

import com.google.gson.annotations.SerializedName;

public class Phone {

    @SerializedName("comment")
    public Object comment;

    @SerializedName("city")
    public String city;

    @SerializedName("number")
    public String number;

    @SerializedName("country")
    public String country;
/*
    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
*/
}
