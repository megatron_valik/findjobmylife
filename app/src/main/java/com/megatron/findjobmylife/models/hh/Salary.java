package com.megatron.findjobmylife.models.hh;

import com.google.gson.annotations.SerializedName;

public class Salary {

    @SerializedName("from")
    public Integer from;

    @SerializedName("to")
    public Integer to;

    @SerializedName("currency")
    public String currency;

    @SerializedName("gross")
    public Boolean gross;
/*
    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getGross() {
        return gross;
    }

    public void setGross(Boolean gross) {
        this.gross = gross;
    }
*/
}
