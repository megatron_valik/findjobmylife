package com.megatron.findjobmylife.models.hh;

import com.google.gson.annotations.SerializedName;

public class Snippet {

    @SerializedName("requirement")
    public String requirement;

    @SerializedName("responsibility")
    public String responsibility;
/*
    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }
*/
}
