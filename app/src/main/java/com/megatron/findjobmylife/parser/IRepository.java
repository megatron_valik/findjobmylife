package com.megatron.findjobmylife.parser;

import com.megatron.findjobmylife.models.VacancyModel;

import java.util.ArrayList;

import io.reactivex.Observable;

public interface IRepository {
    Observable<ArrayList<VacancyModel>> getVacancys();
    Observable<VacancyModel> getVacancyUa();
}
