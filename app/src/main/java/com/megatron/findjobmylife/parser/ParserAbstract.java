package com.megatron.findjobmylife.parser;

import com.megatron.findjobmylife.models.VacancyModel;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.exceptions.UndeliverableException;

public abstract class ParserAbstract implements IRepository {
    //private final String TAG = "Parser";
    protected String city;
    protected String vacancy;
    protected String idCity;
    protected String uri;
    protected int pageIndex;
    protected String params;

    ParserAbstract paramsCityVacancy(String city, String vacancy) {
        this.city = city;
        this.vacancy = vacancy;
        return this;
    }

    ParserAbstract setCityId(String idCity) {
        this.idCity = idCity;
        return this;
    }

    public ParserAbstract uri(String uri) {
        this.uri = uri;
        return this;
    }

    ParserAbstract setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
        return this;
    }

    ParserAbstract setParams(String params) {
        this.params = params;
        return this;
    }

    public abstract VacancyModel getVacancy() throws Exception;

    @Override
    public Observable<VacancyModel> getVacancyUa() {
        return Observable.create(observableEmitter -> {
            VacancyModel model = null;
            try {
                model = getVacancy();
            } catch (Exception e) {
                //Log.e(TAG, e.getMessage());
                if (observableEmitter == null || observableEmitter.isDisposed())
                    return;
                observableEmitter.onError(e);
            } catch (OutOfMemoryError e) {
                if (observableEmitter == null || observableEmitter.isDisposed())
                    return;
                observableEmitter.onError(e);
            }
            try {
                if (observableEmitter == null || observableEmitter.isDisposed())
                    return;

                if (model != null) {
                    observableEmitter.onNext(model);
                    observableEmitter.onComplete();
                } else
                    observableEmitter.onError(new Throwable());
            } catch (UndeliverableException e) {
                observableEmitter.onError(e);
                //Log.e(TAG, e.getMessage());
            } catch (Exception e) {
                observableEmitter.onError(e);
                //Log.e(TAG, e.getMessage());
            } catch (OutOfMemoryError e) {
                observableEmitter.onError(e);
            }
        });
    }

    //*******************************
    public abstract ArrayList<VacancyModel> getVacancyList() throws Exception;

    @Override
    public Observable<ArrayList<VacancyModel>> getVacancys() {
        return Observable.create(observableEmitter -> {
                    ArrayList<VacancyModel> vacancyModels = null;
                    try {
                        vacancyModels = getVacancyList();
                    } catch (OutOfMemoryError e) {
                        if (observableEmitter == null || observableEmitter.isDisposed())
                            return;
                        observableEmitter.onError(e);
                    } catch (Exception e) {
                        if (observableEmitter == null || observableEmitter.isDisposed())
                            return;
                        observableEmitter.onError(e);
                    }
                    try {
                        if (observableEmitter == null || observableEmitter.isDisposed())
                            return;

                        if (vacancyModels != null && vacancyModels.size() > 0) {
                            observableEmitter.onNext(vacancyModels);
                            observableEmitter.onComplete();
                        } else
                            observableEmitter.onError(new Throwable("Throwable"));
                    } catch (UndeliverableException e) {
                        observableEmitter.onError(e);
                    } catch (Exception e) {
                        observableEmitter.onError(e);
                    } catch (OutOfMemoryError e) {
                        observableEmitter.onError(e);
                    }
                }
        );
    }
}
