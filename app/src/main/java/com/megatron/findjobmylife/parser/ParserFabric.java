package com.megatron.findjobmylife.parser;

import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.parsers.HhUaParser;
import com.megatron.findjobmylife.parser.parsers.JobsUaParser;
import com.megatron.findjobmylife.parser.parsers.OlxUaParser;
import com.megatron.findjobmylife.parser.parsers.RabotaUaParser;
import com.megatron.findjobmylife.parser.parsers.WorkUaParser;
import com.megatron.findjobmylife.parser.vacancy.VacancyHhUa;
import com.megatron.findjobmylife.parser.vacancy.VacancyJobsUa;
import com.megatron.findjobmylife.parser.vacancy.VacancyOlxUa;
import com.megatron.findjobmylife.parser.vacancy.VacancyRabotaUa;
import com.megatron.findjobmylife.parser.vacancy.VacancyWorkUa;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.exceptions.UndeliverableException;

public class ParserFabric {
    public static final int WORK_UA = 0;
    public static final int RABOTA_UA = 1;
    public static final int HH_UA = 2;
    public static final int OLX_UA = 3;
    public static final int JOBS_UA = 4;

    private String city;
    private String vacancy;
    private String idCity;
    private String parameter;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public void set(String vacancy, String city, String idCity) {
        this.vacancy = vacancy;
        this.city = city;
        this.idCity = idCity;
    }

    public Observable<VacancyModel> readVacancy(int siteIndex, String uri) {
        ParserAbstract parser = getParserVacancy(siteIndex);
        if (parser == null) return null;

        try {
            return parser.uri(uri)
                    .setCityId(idCity)
                    .getVacancyUa();
        } catch (UndeliverableException ex) {
            return null;
        } catch (OutOfMemoryError e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public Observable<ArrayList<VacancyModel>> readVacancy(int siteIndex, int pageIndex) {
        ParserAbstract parser = getParser(siteIndex);
        if (parser == null) return null;
        try {
            return parser.paramsCityVacancy(city, vacancy)
                    .setCityId(idCity)
                    .setPageIndex(pageIndex)
                    .setParams(parameter)
                    .getVacancys();
        } catch (UndeliverableException ex) {
            return null;
        } catch (OutOfMemoryError e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private ParserAbstract getParser(int index) {
        switch (index) {
            case WORK_UA:
                return new WorkUaParser();
            case RABOTA_UA:
                return new RabotaUaParser();
            case HH_UA:
                return new HhUaParser();
            case OLX_UA:
                return new OlxUaParser();
            case JOBS_UA:
                return new JobsUaParser();
        }
        return null;
    }

    private ParserAbstract getParserVacancy(int index) {
        switch (index) {
            case WORK_UA:
                return new VacancyWorkUa();
            case RABOTA_UA:
                return new VacancyRabotaUa();
            case HH_UA:
                return new VacancyHhUa();
            case OLX_UA:
                return new VacancyOlxUa();
            case JOBS_UA:
                return new VacancyJobsUa();
        }
        return null;
    }

    public static String getNameUa(int index) {
        switch (index) {
            case WORK_UA:
                return "(WORK.UA)";
            case RABOTA_UA:
                return "(RABOTA.UA)";
            case HH_UA:
                return "(HH.UA)";
            case OLX_UA:
                return "(OLX.UA)";
            case JOBS_UA:
                return "(JOBS.UA)";
            default:
                return "";
        }
    }
    //*********************************************************************************************

}
