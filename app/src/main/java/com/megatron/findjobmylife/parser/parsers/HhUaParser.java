package com.megatron.findjobmylife.parser.parsers;

import com.google.gson.Gson;
import com.megatron.findjobmylife.App;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.db.models.SettingModel;
import com.megatron.findjobmylife.db.utils.DbUtils;
import com.megatron.findjobmylife.models.HhVacancyModel;
import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.models.hh.Item;
import com.megatron.findjobmylife.parser.ParserAbstract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.HttpUrl;

public class HhUaParser extends ParserAbstract {
    //    private static final String TAG = "HHua";
    private static final String URL = "api.hh.ru";

    @Override
    public VacancyModel getVacancy() {
        return null;
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() throws Exception {
        String id = idCity;
//        Log.e(TAG, "1....");

        try {
            if (id.equals("") || id.equals("0"))
                id = "5";

            HttpUrl.Builder builderHttp = new HttpUrl.Builder()
                    .scheme("https")
                    .host(URL)
                    .addPathSegment("vacancies")
                    .addQueryParameter("area", id);//+
//            Log.e(TAG, "2.....");
            if (vacancy != null && !vacancy.equals("")) {
                builderHttp.addQueryParameter("text", vacancy);
            }
            SettingModel param = DbUtils.getSetting(2);
            if (param != null) {
//                Log.e(TAG, "3.....");
                String[] idCategory = App.getContext().getResources().getStringArray(R.array.Category3Id);
                String[] idEmploy = App.getContext().getResources().getStringArray(R.array.Employment3Id);
                String[] idOther = App.getContext().getResources().getStringArray(R.array.Other3Id);

                for (int i = 0; i < param.getCategorySelected().size(); i++) {
                    Boolean b = param.getCategorySelected().get(i);
                    if (b != null && b)
                        builderHttp.addQueryParameter("specialization", idCategory[i]);
                }
                for (int i = 0; i < param.getEmploymentSelected().size(); i++) {
                    Boolean b = param.getEmploymentSelected().get(i);
                    if (b != null && b)
                        builderHttp.addQueryParameter("employment", idEmploy[i]);
                }

                if (param.getOtherSelectedIndex() > 0)
                    builderHttp.addQueryParameter("experience", idOther[param.getOtherSelectedIndex()]);
                if (param.getSalarySum() != null && !param.getSalarySum().equals(""))
                    builderHttp.addQueryParameter("salary", param.getSalarySum());
            }
//            Log.e(TAG, "4......");
            if (pageIndex > 0) {
                builderHttp.addQueryParameter("page", pageIndex + "");
            }
            try {
                String u = builderHttp.toString();
//                Log.e(TAG, "url = " + u);
                BufferedReader bufferedReader = null;
                try {
                    java.net.URL url = new URL(u);
                    HttpURLConnection huc = (HttpURLConnection) url.openConnection();
                    HttpURLConnection.setFollowRedirects(false);
                    huc.setConnectTimeout(15000);
                    huc.connect();

                    bufferedReader = new BufferedReader(new InputStreamReader(huc.getInputStream()));

                    StringBuilder stringBuffer = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuffer.append(line);
                    }
//                    Log.e(TAG, "44...");

                    return jsonToVacancyList(stringBuffer.toString());
                } catch (Exception ex) {
//                    Log.e(TAG, "oyoy " + ex.getMessage());
                    throw ex;
                } finally {
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
//                Log.e(TAG, "yoyo " + e.getMessage());
                throw e;
            }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
//            Log.e(TAG, "vvv " + e.getMessage());
            throw e;
        }
    }

    private ArrayList<VacancyModel> jsonToVacancyList(String json) {
        ArrayList<VacancyModel> res = new ArrayList<>();
//        Log.e(TAG, "json = " + json);

        if (json == null || json.equals("")) return res;

        try {
            HhVacancyModel hhVacancyModel = new Gson().fromJson(json, HhVacancyModel.class);
            if (hhVacancyModel != null) {
                try {
                    if (hhVacancyModel.items == null) {
//                        Log.e(TAG, "8....");
                        return res;
                    }
//                    Log.e(TAG, "88...");

                    int size = hhVacancyModel.items.size();
//                    Log.e(TAG, "888..." + size);

                    for (int i = 0; i < size; i++) {
//                        Log.e(TAG, "ii = " + i);
                        try {
                            Item items = hhVacancyModel.items.get(i);
                            if (items == null) continue;

                            String title = "" + items.name;

                            String subTitle = "";
                            if (items.employer != null)
                                subTitle += items.employer.name;
//                            Log.e(TAG, "t =" + title);

//                            Log.e(TAG, "st =" + subTitle);
                            String cityName = "";
                            if (items.area != null)
                                cityName += items.area.name;
//                            Log.e(TAG, "city =" + cityName);

                            String image = "";
                            if (items.employer != null && items.employer.logoUrls != null)
                                if (items.employer.logoUrls._90 != null)
                                    image = items.employer.logoUrls._90;
                                else if (items.employer.logoUrls._240 != null)
                                    image = items.employer.logoUrls._240;
                                else if (items.employer.logoUrls.original != null)
                                    image = items.employer.logoUrls.original;
//                            Log.e(TAG, "im =" + image);

                            String overflow = "";
                            if (items.snippet != null && items.snippet.requirement != null)
                                overflow += items.snippet.requirement + "\r\n";
                            if (items.snippet != null && items.snippet.responsibility != null)
                                overflow += items.snippet.responsibility;
                            overflow = replace(overflow);
//                            Log.e(TAG, "ov =" + overflow);

                            String url = "" + items.alternateUrl;
//                            Log.e(TAG, "u =" + url);

                            VacancyModel vacancyModel = new VacancyModel(title, subTitle, cityName, image, "", overflow, url);
//                            Log.e(TAG, "model =" + vacancyModel.toString());

                            res.add(vacancyModel);
                        } catch (Exception e) {
//                            Log.e(TAG, "ex = " + e.getMessage());
                        }
                    }
                } catch (Exception e) {
//                    Log.e(TAG, "json err1 " + e.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
//            Log.e(TAG, "json err2 " + e.getMessage());
        }

        return res;
    }

    private String replace(String str) {
        if (str == null) return "";
        return str
                .replace("&quot;", "")
                .replace("&amp;", "")
                .replace("null", "")
                .replace("<highlighttext>", "")
                .replace("</highlighttext>", "")
                .replace("&#39;", "'")
                ;
    }

}
