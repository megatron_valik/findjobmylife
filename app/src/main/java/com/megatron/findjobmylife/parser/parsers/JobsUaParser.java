package com.megatron.findjobmylife.parser.parsers;

import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserAbstract;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class JobsUaParser extends ParserAbstract {
    private static final String URL = "jobs.ua";

    @Override
    public VacancyModel getVacancy() {
        return null;
    }
    //*******************************

    private String getUrl(int pageIndex) {
        String mUrl = URL + "/vacancy/";
        String cityName = idCity.trim();

        boolean isParam = false;
        if (params != null && !params.equals("") && !params.equals("REPLACE?")) {
            isParam = true;
            mUrl += params;
        }

        String formatVacancyAndCity = "";

        if (!cityName.trim().equals("")) {
            formatVacancyAndCity += "/" + cityName.trim();
        }

        if (!vacancy.trim().equals("")) {
            String mVacancy = vacancy.trim().replaceAll("\\s+", " ");
            mVacancy = mVacancy
                    .replace("+", "%2B")
                    .replace("#", "%23")
                    .replaceAll(" ", "-");
            formatVacancyAndCity += /*(isParam ? "/" : formatVacancyAndCity.endsWith("/") ? "" : "/") +*/ "/rabota-" + mVacancy;
        }

        if (isParam) {
            if (pageIndex > 0) {
                formatVacancyAndCity += "/page-" + pageIndex;
            }

            mUrl = mUrl.replace("REPLACE", formatVacancyAndCity);
        } else
            mUrl += formatVacancyAndCity;

        if (pageIndex > 0 && !isParam) {
            mUrl += "/page-" + pageIndex;
        }

        return mUrl.replaceAll("//", "/");
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() throws Exception {
        String u = "https://" + getUrl(pageIndex);// + "/";
        ArrayList<VacancyModel> res = new ArrayList<>();

        try {
            Document doc = Jsoup.connect(u).timeout(45000).userAgent("Mozilla").get();
            // переглянутые вакансии
            Elements listSelected = doc.getElementsByClass("b-vacancy__item js-item_list is_choose_me");

            // все остальные
            Elements list = doc.getElementsByClass("b-vacancy__item js-item_list");
            if (listSelected != null && listSelected.size() > 0) {
                list.addAll(0, listSelected);
            }

            for (Element element : list) {
                Attributes atr = element.attributes();
                if (atr.size() <= 1) {
                    continue;
                }

                String title;
                String subTitle = "";
                StringBuilder cityName = new StringBuilder();
                String vip = "";
                String overflow = "";
                String urlVacancy;
                String imUri = "";

                // TITLE
                Elements top = element.getElementsByClass("b-vacancy__top");
                if (top.size() == 0)
                    continue;

                Elements childs = top.get(0).children();

                Element select = childs.get(0);
                title = select.text();

                // URL VACANCY
                Elements url = select.children();
                urlVacancy = url.attr("href");
                if (urlVacancy.equals(""))
                    urlVacancy = getValue(url, "href", false);

                // ЛОГО  b-vacancy__logo
                Elements logo = element.getElementsByClass("b-vacancy__logo");
                if (logo != null && logo.size() > 0) {
                    Attributes aaa = logo.get(0).attributes();
                    imUri = "//" + URL + "/" + aaa.get("src");
                }

                // COMPANY
                Elements subTitleMain = element.getElementsByClass("b-vacancy__tech__item");
                if (subTitleMain != null && subTitleMain.size() > 0) {
                    Elements city = subTitleMain.get(0).getElementsByClass("link__hidden");
                    if (city != null && city.size() > 0) {
                        subTitle = city.text();
                    } else {
                        subTitle = subTitleMain.get(0).text();
                    }
                    int s = subTitleMain.size();
                    for (int i = 1; i < s; i++) {
                        cityName.append(subTitleMain.get(i).text()).append(i < s - 1 ? " · " : "");
                    }

                    Elements vipMain = subTitleMain.get(0).getElementsByClass("b-label_empty b-label_empty__vip");
                    if (vipMain != null && vipMain.size() > 0) {
                        vip = vipMain.text();
                    }

                }

                // OVERFLOW
                Elements overflowMain = element.getElementsByClass("grey-light");
                if (overflowMain != null) {
                    overflow = overflowMain.text();
                }

                // ***********************************************
                VacancyModel vacancy = new VacancyModel(title, subTitle, cityName.toString(), imUri, vip, overflow, urlVacancy);
                res.add(vacancy);
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return res;
    }

    private String getValue(Elements elements, String findParam, boolean isTextResult) {
        for (Element element : elements) {
            Attributes a = element.attributes();
            if (a.hasKey(findParam)) {
                if (isTextResult)
                    return element.text();
                return a.get(findParam);
            } else {
                String res = getValue(element.children(), findParam, isTextResult);
                if (!res.equals(""))
                    return res;
            }
        }
        return "";
    }
}
