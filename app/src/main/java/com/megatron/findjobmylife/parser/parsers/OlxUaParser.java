package com.megatron.findjobmylife.parser.parsers;

import com.megatron.findjobmylife.classes.TLSSocketFactory;
import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserAbstract;
import com.megatron.findjobmylife.utils.Utils;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class OlxUaParser extends ParserAbstract {
    private static final String URL = "www.olx.ua";
    private static int countPages = 0; // к-во страниц (вычитать при первом запросе - 0-ая страница)

    @Override
    public VacancyModel getVacancy() {
        return null;
    }

    private String getUrl(int pageIndex) {
        String mUrl = URL + "/uk/rabota/";
        String cityName = idCity.equals("") ? "" : idCity + "/";
        if (params != null && !params.equals("")) {
            mUrl += params + "/";
        }
        mUrl += cityName;
        if (!vacancy.trim().equals("")) {
            mUrl += "q-" + vacancy.trim().replaceAll("\\s+", " ").replace(" ", "-") + "/";
        }
        if (pageIndex > 0) {
            mUrl += "?page=" + pageIndex;
        }

        return mUrl;
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() throws Exception {
        String u = "https://" + getUrl(pageIndex);
        ArrayList<VacancyModel> res = new ArrayList<>();

        try {
            if (pageIndex > 0 && pageIndex > countPages) {
                return null;
            }

            Connection.Response execute = Jsoup.connect(u).sslSocketFactory(new TLSSocketFactory())
                    .ignoreHttpErrors(true).timeout(15000).execute();
            Document doc = Jsoup.parse(execute.body());
            Elements main = doc.select("tr.wrap");
            if (main == null) {
                return null;
            }

            if (main.size() > 0) {
                if (pageIndex == 0) {
                    countPages = getCountPages(doc.getElementsByClass("pager rel clr"));
                }

                for (Element e : main) {
                    Elements childs = e.getElementsByClass("offer promoted offer-job");
                    boolean isVip = childs.size() > 0;

                    if (childs.size() == 0) {
                        childs = e.getElementsByClass("offer  offer-job");
                        if (childs.size() == 0)
                            continue;
                    }

                    String title = getTitle(childs.get(0).getElementsByClass("marginright5 link linkWithHash detailsLink"));
                    if (title.equals(""))
                        continue;

                    String price = getTitle(childs.get(0).getElementsByClass("list-item__price"));

                    String[] param = getParam(e.getElementsByClass("space rel"));
                    String urlVacancy = getUrl(e.getElementsByClass("space rel"));
                    String imUri = getImage(childs);

                    VacancyModel vacancy = new VacancyModel(title, price, param[0], imUri, isVip ? "VIP" : "", "", urlVacancy);
//                    Log.v("OLXParser", vacancy.toString());
                    res.add(vacancy);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return res;
    }

    private String[] getParam(Elements rel) {
        StringBuilder txt = new StringBuilder();
        Elements el = rel.get(1).getElementsByClass("breadcrumb x-normal");
        for (int i = 0; i < el.size(); i++) {
            txt.append(getTitle(el.get(i).children())).append(" · ");
        }

        txt.append(getTitle(rel.get(1).getElementsByClass("breadcrumb breadcrumb--job-type x-normal"))).append(" · ");
        txt.append(getTitle(rel.get(1).getElementsByClass("breadcrumb breadcrumb--with-divider x-normal")));

        return new String[]{txt.toString()};
    }

    private String getImage(Elements e) {
        Elements el = e.get(0).getElementsByClass("offer-logo");
        if (el.size() > 0) {
            Elements im = el.select("img");
            if (im.size() > 0) {
                Attributes a = im.get(0).attributes();
                return a.get("src");
            }
        }

        return null;
    }

    private String getTitle(Elements e) {
        if (e.size() > 0 && e.hasText())
            return e.text();
        return "";
    }

    private String getUrl(Elements e) {
        Elements el = e.get(0).select("a");
        if (el.size() > 0) {
            Attributes a = el.get(0).attributes();
            return a.get("href");
        }
        return "";
    }

    private int getCountPages(Elements e) {
        int res = 0;
        if (e.size() > 0) {
            Elements page = e.get(0).getElementsByClass("item fleft");
            if (page.size() > 0) {
                String p = page.get(page.size() - 1).text();
                if (p != null && !p.equals("")) {
                    res = Utils.strToInt(p);
                }
            }
        }
        return res;
    }

}
