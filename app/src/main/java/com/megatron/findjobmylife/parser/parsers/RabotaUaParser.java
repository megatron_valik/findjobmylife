package com.megatron.findjobmylife.parser.parsers;

import android.util.Log;

import com.megatron.findjobmylife.App;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserAbstract;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Scanner;

import static com.megatron.findjobmylife.utils.Utils.enableSSLSocket;
import static com.megatron.findjobmylife.utils.Utils.getParameters;

public class RabotaUaParser extends ParserAbstract {
    private static final String URL_RABOTA = "www.rabota.ua";

    @Override
    public VacancyModel getVacancy() {
        return null;
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() throws Exception {
        String url = getUrl();
        ArrayList<VacancyModel> res = new ArrayList<>();

        try {
            enableSSLSocket();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
/*
            String content = get(url);
            Document doc = Jsoup.parse(content);
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            Document doc = Jsoup.connect(url).userAgent("Mozilla").ignoreContentType(true).execute().parse();
*/
            Connection.Response execute = Jsoup.connect(url).execute();
            Document doc = Jsoup.parse(execute.body());
            Elements main = doc.select("table.f-vacancylist-tablewrap");
            if (main == null) {
                return res;
            }

            if (!isFindVacancy(doc)) {
                VacancyModel model = new VacancyModel(App.getContext().getString(R.string.vacancy_not_found_title),
                        App.getContext().getString(R.string.you_quection) + vacancy +
                                App.getContext().getString(R.string.no_vacancy_now)
                        , "", "", "", "", "");
                model.setEmptyVacancy(true);
                res.add(model);

                VacancyModel vacancyList = new VacancyModel(App.getContext().getString(R.string.other_vacancy_list),
                        "", "", "", "", "", "");
                vacancyList.setEmptyVacancy(true);
                res.add(vacancyList);
            }

            if (main.size() > 0) {
                Elements childs = main.get(0).children().select("article.f-vacancylist-vacancyblock");
                if (childs.size() == 0) {
                    res = parseNew(main);
                    return res;
                }
                if (childs.hasText()) {
                    for (Element e : childs) {
                        String title = getValue("title", e.select("div.fd-f1"), false);
                        String sum = getSum(e);
                        String subTitle = getValue("href", e.getElementsByClass("f-vacancylist-companyname fd-merchant f-text-dark-bluegray"), true);
                        String cityName = getCity(e.getElementsByClass("f-vacancylist-characs-block fd-f-left-middle"));
                        String imUri = getValue("src", e.getElementsByClass("f-vacancylist-companylogo fd-f-right-top"), false);

                        String[] param = getParamsVacancy(e.getElementsByClass("fd-f-left-middle fd-f1 f-vacancylist-bottomblock"));
                        String overflow = getOverflow(e);
                        String urlVacancy = "https://" + URL_RABOTA + getValue("href", e.select("div.fd-f1"), false);

                        if (!param[1].equals(""))
                            cityName += " · " + param[1];

                        VacancyModel vacancy = new VacancyModel(title + sum, subTitle, cityName, imUri, param[0], overflow, urlVacancy);
                        if (!res.contains(vacancy))
                            res.add(vacancy);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return res;
    }

    private String get(String xml) throws IOException {
        StringBuilder content = new StringBuilder();
        try {
            URLConnection connection = new URL(xml).openConnection();
            Scanner scanner = new Scanner(connection.getInputStream());
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                content.append(line);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return content.toString();
    }

    private String getUrl() {
        String mUrl = "https://rabota.ua/jobsearch/vacancy_list";

        String mVacancy = vacancy.toLowerCase().trim().replaceAll("\\s+", " ");
        mVacancy = mVacancy.replaceAll(" ", "-");

        //*****************************
        if (mVacancy.equals("c#") || mVacancy.equals("c #")) {
            mUrl = "https://rabota.ua/zapros/c-sharp";

            if (!city.trim().equals("")) {
                try {
                    mUrl += "/" + URLEncoder.encode(idCity.trim().toLowerCase(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            if (pageIndex > 0) {
                mUrl += (mUrl.endsWith("/") ? "" : "/") + "pg" + pageIndex;
            }

            if (params != null && !params.equals("")) {
                mUrl += getParameters(params);
            }
        } else
            //****************************************
            if (mVacancy.equals("c++") || mVacancy.equals("c ++")) {
                mUrl = "https://rabota.ua/zapros/cpp";

                if (!city.trim().equals("")) {
                    try {
                        mUrl += "/" + URLEncoder.encode(idCity.trim().toLowerCase(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                if (pageIndex > 0) {
                    mUrl += (mUrl.endsWith("/") ? "" : "/") + "pg" + pageIndex;
                }

                if (params != null && !params.equals("")) {
                    mUrl += getParameters(params);
                }
            } else
            //*************************************
            {
                if (idCity.trim().equals("") && mVacancy.trim().equals("")) {
                    if (params != null && !params.equals("")) {
                        mUrl += getParameters(params, pageIndex > 0 ? "pg=" + pageIndex : "");
                    } else if (pageIndex > 0)
                        mUrl += getParameters("pg=" + pageIndex);
                    return mUrl;
                } else
                //************************************
                {
                    mUrl = "";
                    if (idCity.trim().equals("")) { // только вакансия
                        try {
                            mUrl += "/zapros/" + URLEncoder.encode(mVacancy, "UTF-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (pageIndex > 0)
                            mUrl += ("/pg" + pageIndex);
                    } else {
                        boolean b = mVacancy.equals(""); // только город или город+вакансия
                        try {
                            if (!mVacancy.equals(""))
                                mUrl += "/zapros/" + URLEncoder.encode(mVacancy, "UTF-8");
                            if (!idCity.equals(""))
                                mUrl += (mUrl.endsWith("/") ? "" : "/") + URLEncoder.encode(idCity.trim().toLowerCase(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        if (pageIndex > 0)
                            mUrl += (b ? "?pg=" : "/pg") + pageIndex;
                    }

                    mUrl = "https://rabota.ua" + mUrl;

                    if (params != null && !params.equals("")) {
                        mUrl += getParameters(params);
                    }
                }
            }
        mUrl = mUrl.toLowerCase();
        Log.v("rabotaParser", mUrl);
        return mUrl;
    }

    private String getUrl1() {
        String mUrl = "https://" + URL_RABOTA + "/ua/jobsearch/vacancy_list?keyWords=";

        String mVacancy = vacancy.toLowerCase().trim().replaceAll("\\s+", " ");
        mVacancy = mVacancy.replaceAll(" ", "-");

        if (mVacancy.equals("c#") || mVacancy.equals("c #")) {
            mUrl = "https://rabota.ua/zapros/c-sharp/";

            if (!city.trim().equals("")) {
                mUrl += idCity + "/";
            }

            if (pageIndex > 0) {
                mUrl += "pg" + pageIndex;
            }
        } else if (mVacancy.equals("c++") || mVacancy.equals("c ++")) {
            mUrl = "https://rabota.ua/zapros/cpp/";

            if (!city.trim().equals("")) {
                mUrl += idCity + "/";
            }

            if (pageIndex > 0) {
                mUrl += "pg" + pageIndex;
            }
        } else {
            if (city.trim().equals("") && vacancy.trim().equals(""))
                mUrl = mUrl + "&regionId=0";
            else if (city.trim().equals(""))
                mUrl = mUrl + mVacancy;
            else
                mUrl = mUrl + vacancy + "&regionId=" + idCity;

            if (params != null && !params.equals("")) {
                mUrl += params;
            }
        }
        if (pageIndex > 0) {
            mUrl += "&pg=" + pageIndex;
        }
        return mUrl;
    }

    private String getOverflow(Element e) {
        Elements elements = e.getElementsByClass("f-vacancylist-shortdescr f-text-gray fd-craftsmen");
        if (elements.hasText())
            return elements.text();
        return "";
    }

    private String getSum(Element e) {
        Elements elements = e.getElementsByClass("fd-beefy-soldier -price");
        if (elements.hasText())
            return ", " + elements.text();
        return "";
    }

    private String getValue(String findParam, Elements elements, boolean isTextResult) {
        for (Element element : elements) {
            Attributes a = element.attributes();
            if (a.hasKey(findParam)) {
                if (isTextResult)
                    return element.text();
                return a.get(findParam);
            } else {
                String res = getValue(findParam, element.children(), isTextResult);
                if (!res.equals(""))
                    return res;
            }
        }
        return "";
    }

    private String getCity(Elements elements) {
        if (elements.size() == 0) return "";
        Elements childs = elements.get(0).children();
        for (Element child : childs) {
            if (child.hasText()) {
                Elements location = child.getElementsByClass("fd-merchant");
                if (location.size() > 0) {
                    return location.text();
                }
            }
        }
        return "";
    }

    private String[] getParamsVacancy(Elements e) {
        String[] res = new String[2];
        res[0] = "";
        Elements el = e.get(0).getElementsByClass("fd-f1");
        Elements el2 = el.get(0).getElementsByClass("f-vacancylist-agotime f-text-light-gray fd-craftsmen");
        res[1] = el2.text();
        return res;
    }

    private boolean isFindVacancy(Document doc) {
        Elements block = doc.select("div.f-vacancylist-foundblock");
        if (block.hasText()) {
            Elements e = block.select("div.fd-f-left-middle");
            String r = e.text();
            String mask1 = "Знайдено ";
            if (!r.equals("") && r.startsWith(mask1)) {
                String[] split = r.split(" ");
                try {
                    String count = split[1];
                    int countRes = Integer.parseInt(count);
                    return countRes > 0;
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        }
        return true;
    }

//    private static void enableSSLSocket() throws KeyManagementException, NoSuchAlgorithmException {
//        HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
//
//        SSLContext context = SSLContext.getInstance("TLS");
//        context.init(null, new X509TrustManager[]{new X509TrustManager() {
//            @SuppressLint("TrustAllX509TrustManager")
//            public void checkClientTrusted(X509Certificate[] chain, String authType) {
//                //
//            }
//
//            @SuppressLint("TrustAllX509TrustManager")
//            public void checkServerTrusted(X509Certificate[] chain, String authType) {
//                //
//            }
//
//            public X509Certificate[] getAcceptedIssuers() {
//                return new X509Certificate[0];
//            }
//        }}, new SecureRandom());
//        HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
//    }

    //*****************************
    private ArrayList<VacancyModel> parseNew(Elements main) {
        ArrayList<VacancyModel> res = new ArrayList<>();
        Elements childs = main.get(0).children();
        if (childs.size() > 0) {
            Elements vacancyList = childs.get(0).children();
            if (vacancyList.size() > 0) {
                for (Element v : vacancyList) {
                    String title = getValue(v, "card-title");
                    if (title.equals(""))
                        continue;
                    String subTitle = getValue(v, "company-profile-name");
                    String overflow = getValue(v, "card-description");
                    String sum = getValue(v, "salary");
                    String city = getValue(v, "location");
                    String time = getValue(v, "publication-time");
                    String vip = getValue("data-is-hot", v.getElementsByClass("card"), false);
                    if (vip.equals("True")) vip = "VIP";
                    else vip = "";
                    String imUri = "";

                    Elements logos = v.getElementsByClass("card-logo");
                    if (logos != null && logos.size() > 0) {
                        Elements el = logos.get(0).select("img");
                        if (el != null && el.size() > 0) {
                            imUri = getValue("src", el, false);
                            if (imUri.equals(""))
                                imUri = getValue("data-cfsrc", el, false);
                        }
                    }

                    String urlVacancy = "https://" + URL_RABOTA + getValue("href", v.getElementsByClass("card-title"), false);

                    if (!sum.equals(""))
                        title += ", " + sum;
                    if (!time.equals(""))
                        city += " · " + time;

                    VacancyModel vacancy = new VacancyModel(title, subTitle, city, imUri, vip, overflow, urlVacancy);
                    if (!res.contains(vacancy))
                        res.add(vacancy);
                }
            }
        }

        return res;
    }

    private String getValue(Element element, String className) {
        String res = "";
        Elements elements = element.getElementsByClass(className);
        if (elements.size() > 0)
            return elements.text();
        return res;
    }

}
