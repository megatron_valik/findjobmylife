package com.megatron.findjobmylife.parser.parsers;

import com.megatron.findjobmylife.parser.ParserAbstract;
import com.megatron.findjobmylife.models.VacancyModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class WorkUaParser extends ParserAbstract {
    private static final String URL = "www.work.ua";

    @Override
    public VacancyModel getVacancy() {
        return null;
    }

    private String getUrl(int pageIndex) {
        String mUrl = URL;
        String cityName = idCity.equals("") ? city : idCity;

        if (cityName.trim().equals("") && vacancy.trim().equals(""))
            mUrl += "/jobs/" + ((pageIndex == 0) ? "?ss=1" : "");
        else if (cityName.trim().equals(""))
            mUrl += "/jobs-";
        else
            mUrl += "/jobs-" + cityName + "-";
        String mVacancy = vacancy.trim()
                .replaceAll("\\s+", " ");
        mVacancy = mVacancy
                .replace("+", "%2B")
                .replace("#", "%23")
                .replaceAll(" ", "+");
        mUrl += mVacancy;// + "/";

        boolean isParam = false;
        if (params != null && !params.equals("")) {
            isParam = true;
            mUrl += params;
        }

        if (pageIndex > 0) {
            mUrl += (isParam ? "&page=" : "/?page=") + pageIndex;
        }

        return mUrl;
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() throws Exception {
        String u = "https://" + getUrl(pageIndex) + "/";
        ArrayList<VacancyModel> res = new ArrayList<>();

        try {
            Document doc = Jsoup.connect(u).timeout(15000).get();
            Element main = doc.getElementById("pjax-job-list");
            if (main == null) {
                return null;
            }

            // по новому обработка
            Elements childses = main.children().select("div.card-hover");
            if (childses.size() > 0) {
                for (Element e : childses) {
                    String title = getTitleNew(e).replace(" ,", ",");
                    if (title.equals(""))
                        continue;
                    String cityName;
                    String overflow = getOverflowNew(e);
                    String urlVacancy = getUrlNew(e);
                    String imUri = "";
                    String subTitle;

                    Attributes a = e.attributes();
                    String aa = a.get("class");
                    if (aa.equals("card card-hover card-visited wordwrap job-link")
                            || aa.startsWith("card card-hover card-visited wordwrap")) {
                        Elements el = e.select("img");
                        if (el.size() == 1) { // есть картика-логотип
                            Attributes aaa = el.get(0).attributes();
                            imUri = aaa.get("src");
                            subTitle = aaa.get("alt");
                        } else {
                            Elements sTitle = e.children();
                            Elements st = sTitle.select("div.add-top-xs b");
                            subTitle = st.text();
                        }

                        String[] param = getParamsVacancy(e);
                        cityName = param[0];

                        VacancyModel vacancy = new VacancyModel(title, subTitle, cityName, imUri, param[1], overflow, urlVacancy);
                        //Log.v("WorkUaParser", urlVacancy);
                        res.add(vacancy);
                    }
                }
                return res;
            }

            // по старому обработка
            Elements childs = main.children().select("div");
            if (childs.hasText()) {
                for (Element e : childs) {
                    String title = getTitle(e).replace(" ,", ",");
                    if (title.equals(""))
                        continue;

                    String cityName;
                    String overflow = getOverflow(e);
                    String urlVacancy = getUrl(e);
                    String imUri = "";
                    String subTitle = "";

                    Attributes a = e.attributes();
                    String aa = a.get("class");
                    if (aa.equals("card card-hover card-visited wordwrap job-link")
                            || aa.startsWith("card card-hover card-visited wordwrap")) {
                        Elements el = e.select("img");
                        if (el.size() == 1) { // есть картика-логотип
                            Attributes aaa = el.get(0).attributes();
                            imUri = aaa.get("src");
                            subTitle = aaa.get("alt");
                        } else {
                            Elements sTitle = e.children();
                            if (sTitle.size() >= 2)
                                subTitle = sTitle.get(1).text();
                        }

                        String[] param = getParamsVacancy(e, subTitle);
                        cityName = param[0];

                        VacancyModel vacancy = new VacancyModel(title, subTitle, cityName, imUri, param[1], overflow, urlVacancy);
                        //Log.v("WorkUaParser", urlVacancy);
                        res.add(vacancy);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return res;
    }

    private String getTitle(Element elements) {
        Elements select = elements.select("h2.add-bottom-sm");

        if (select.size() == 1) {
            Elements ch = select.get(0).children();
            if (ch.size() >= 1) {
                Elements ch2 = select.get(0).children();
                if (ch2.size() >= 1) {
                    return ch2.text();
                }
            }
        }
        return "";
    }

    private String getTitleNew(Element elements) {
        String sumText = "";
        Elements select = elements.select("h2");
        Elements sum = elements.select("div b");
        if (sum.size() >= 2)
            sumText = ", " + sum.get(0).text();
        else {
            sum = elements.select("div span.text-muted");
            if (sum != null && sum.size() >= 2) {
                String txt = sum.get(0).text();
                if (!txt.trim().equals("·"))
                    sumText = ", " + txt;
            }
        }

        if (select.size() >= 1) {
            Elements ch = select.get(0).children();
            if (ch.size() >= 1) {
                Elements ch2 = select.get(0).children();
                if (ch2.size() >= 1) {
                    return ch2.text() + sumText;
                }
            }
        }
        return "";
    }

    private String getUrl(Element elements) {
        Elements select = elements.select("h2.add-bottom-sm");
        if (select.size() == 1) {
            Elements ch = select.get(0).children();
            if (ch.size() >= 1) {
                Attributes aaa = ch.get(0).attributes();
                return URL + aaa.get("href");
            }
        }
        return "";
    }

    private String getUrlNew(Element elements) {
        Elements select = elements.select("p.overflow");
        if (select.size() == 1) {
            Elements ch = select.get(0).children();
            if (ch.size() >= 1) {
                String aaa = ch.attr("href");
                return URL + aaa;
//                Attributes aaa = ch.get(0).attributes();
//                return URL + aaa.get("href");
            }
        }
        return "";
    }

    private String getOverflow(Element elements) {
        Elements select = elements.select("p.overflow");
        if (select.size() == 1) {
            return select.text();
        }
        return "";
    }

    private String getOverflowNew(Element elements) {
        Elements select = elements.select("p.overflow");
        if (select.size() == 1) {
            return select.text();
        }
        return "";
    }

    private String[] getParamsVacancy(Element elements, String title) {
        Elements select = elements.select("span");
        String[] res = new String[]{"", ""};
        StringBuilder str = new StringBuilder();
        int size = select.size();

        boolean isCityFound = false;

        for (int i = 0; i < size; i++) {
            Attributes vipAttr = select.get(i).attributes();
            for (Attribute attrVip : vipAttr) {
                if (attrVip.getValue().startsWith("label label-vip"))
                    res[1] = "VIP";
            }

            if (select.get(i).text().trim().equals("·") && !isCityFound) {
                str = new StringBuilder(select.get(i + 1).text());
                String s = select.get(i + 2).text();
                str.append(" ").append(s);

                isCityFound = true;

                for (int j = i + 2; j < size; j++) {
                    Attributes attr = select.get(j).attributes();
                    for (Attribute a : attr) {
                        if (a.getValue().startsWith("label label-hot")) {
                            String d = select.get(j).text();
                            if (!d.equals(s) && !str.toString().contains(d)) {
                                if (str.toString().trim().endsWith("·"))
                                    str.append(" ").append(d);
                                else
                                    str.append(" · ").append(d);
                            }
                        } else if (a.getValue().startsWith("distance-block")) {
                            String d = select.get(j).text();
                            if (!d.equals(s) && !str.toString().contains(d))
                                str.append(" ").append(d);
                        }
                    }
                    String yesterday = select.get(j).text();
                    if (yesterday.trim().startsWith("·") && !str.toString().contains(yesterday)) {
                        str.append(" ").append(yesterday);
                    }
                }
            }
        }

        if (str.toString().contains("VIP")) {
            res[1] = "VIP";
            str = new StringBuilder(str.toString().replace("VIP", ""));
        }
        str = new StringBuilder(str.toString().replace(title, "").replaceAll("\\s+", " ").trim());
        res[0] = str.toString();

        return res;
    }

    private String[] getParamsVacancy(Element element) {
        String[] res = {"", ""};
        Elements elements = element.select("span ~ span ~ span");
        res[0] = elements.text();

        //VIP
        Elements vip = element.select("span.label-vip");
        res[1] = vip.text();

        try {
            // last time *(4год тому)
            Elements times = element.select("div.pull-right");
            if (times != null && times.size() > 0) {
                String mTime;
                Elements childs = times.get(0).children();
                if (childs.size() <= 1)
                    mTime = childs.text();
                else
                    mTime = childs.get(childs.size() - 1).text();
                res[0] += mTime.equals("") ? "" : " · " + mTime;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
