package com.megatron.findjobmylife.parser.vacancy;

import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserAbstract;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class VacancyHhUa extends ParserAbstract {

    @Override
    public VacancyModel getVacancy() throws Exception {
        return getVacancy(uri);
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() {
        return null;
    }

    private VacancyModel getVacancy(String uri) throws Exception {
        try {
            Document doc = Jsoup.connect(uri).userAgent("Mozilla").get();

            Elements card = doc.getElementsByClass("vacancy-title ");
            Elements card2 = doc.getElementsByClass("vacancy-company");
            Elements cardOverflow = doc.getElementsByClass("vacancy-description");//++
            Elements cardTime = doc.getElementsByClass("vacancy-creation-time");//++

            try {
                onRemoveStyle(cardOverflow);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String image = null;
            String imageHttp = "";
            try {
                image = imageUrl(card2);
                if (image != null) {
                    imageHttp = "<img src=\"" + image + "\">";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                remove(cardOverflow, "wrap_hh_slider_wrapper");
                doc.select("a,script,.hidden,style,form").remove();
//              doc.select("a,script,.hidden,style,form,span").remove();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String id = uri.replace("https://hh.ru/vacancy/", "");
            String idVacancy = "https://hh.ru/applicant/vacancy_response?vacancyId=" + id;

            String pp = imageHttp + card.html() + card2.html() + cardOverflow.html() + cardTime.html();
            return new VacancyModel("", "", "", image, "", pp, idVacancy);
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        } catch (OutOfMemoryError e) {
            throw new Exception(e.getMessage());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void onRemoveStyle(Elements childs) {
        if (childs == null || childs.size() == 0) return;
        Elements sel = childs.get(0).getElementsByTag("style");
        if (sel != null && sel.size() > 0) {
            sel.first().remove();
        }
    }

    private void remove(Elements childs, String tag) {
        if (childs == null || childs.size() == 0) return;
        Elements sel = childs.get(0).getElementsByClass(tag);
        if (sel != null && sel.size() > 0) {
            sel.first().remove();
        }
    }

    private String imageUrl(Elements childs) {
        if (childs == null || childs.size() == 0) return null;
        Elements im = childs.get(0).getElementsByTag("meta");
        if (im != null && im.size() > 0) {
            int count = im.size();
            for (int i = 0; i < count; i++) {
                boolean findLogo = false;
                Attributes attr = im.get(i).attributes();
                for (Attribute a : attr) {
                    if (a.getKey().equals("itemProp") && a.getValue().equals("logo")) {
                        findLogo = true;
                        break;
                    }
                }
                if (findLogo)
                    return attr.get("content");

            }
            return null;
        }
        return null;
    }
}
