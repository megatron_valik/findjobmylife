package com.megatron.findjobmylife.parser.vacancy;

import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserAbstract;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class VacancyJobsUa extends ParserAbstract {
    @Override
    public VacancyModel getVacancy() throws Exception {
        return getVacancy(uri);
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() {
        return null;
    }

    private VacancyModel getVacancy(String uri) throws Exception {
        try {
            Document doc = Jsoup.connect(uri).timeout(50000).userAgent("Mozilla").get();

            Elements purchase = doc.getElementsByClass("b-vacancy-full__purchase");
            if (purchase != null && purchase.first() != null)
                purchase.first().remove();

            Elements footer = doc.getElementsByClass("b-resume-full__footer");
            if (footer != null && footer.first() != null)
                footer.first().remove();

            Elements footer2 = doc.getElementsByClass("b-vacancy-full__footer b-vacancy-full__footer-links");
            if (footer2 != null && footer2.first() != null)
                footer2.first().remove();

            // заменить ссылку рисунка
            Elements logo = doc.getElementsByClass("b-vacancy-full__logo");
            if (logo != null && logo.size() > 0) {
                Attributes a = logo.get(0).attributes();
                String img = a.get("src");
                String newImg = "https://jobs.ua" + img;
                logo.attr("src", newImg);
            }

            Elements card = doc.getElementsByClass("b-vacancy-full js-item_full");

            String pp = card.html();
            return new VacancyModel("", "", "", null, "", pp, uri);
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        } catch (OutOfMemoryError e) {
            throw new Exception(e.getMessage());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}
