package com.megatron.findjobmylife.parser.vacancy;

import com.megatron.findjobmylife.classes.TLSSocketFactory;
import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserAbstract;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VacancyOlxUa extends ParserAbstract {

    @Override
    public VacancyModel getVacancy() throws Exception {
        return getVacancy(uri);
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() {
        return null;
    }

    private VacancyModel getVacancy(String uri) throws Exception {
        try {
//            Document doc = Jsoup.connect(uri).sslSocketFactory(new TLSSocketFactory())
//                    .ignoreHttpErrors(true).get();

            Connection.Response res = Jsoup.connect(uri).sslSocketFactory(new TLSSocketFactory())
                    .ignoreHttpErrors(true).execute();
            Document doc = res.parse();
            removeClass(doc.getElementsByClass("offer-titlebox__buttons"));
            removeClass(doc.getElementsByClass("pding15"));
            removeClass(doc.getElementsByClass("observed-txt"));

            Elements card = doc.getElementsByClass("offercontentinner offer__innerbox");
            showPhone(doc);
            String phone = showNewPhone(doc, res.cookies());
            if (phone != null) {
                Element e = card.get(0);
                e = e.child(e.children().size() - 1);
                e.after((phone));
            }

            String pp = card.html();
            return new VacancyModel("", "", "", null, "", pp, uri);
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        } catch (OutOfMemoryError e) {
            throw new Exception(e.getMessage());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void showPhone(Document doc) {
        try {
            Elements select = doc.getElementsByClass("spoilerHidden");
            int size = select.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    Attributes attr = select.get(i).attributes();
                    for (Attribute a : attr) {
                        if (a.getKey().equals("data-phone")) {
                            select.get(i).append(a.getValue());
                        }
                    }
                    List<TextNode> childs = select.get(i).textNodes();
                    if (childs.size() >= 2) {
                        childs.get(0).remove();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeClass(Elements elements) {
        if (elements != null && elements.size() > 0) {
            try {
                Element element = elements.first();
                if (element != null)
                    element.remove();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String showNewPhone(Document doc, Map<String, String> cookies) {
        Element element = doc.getElementById("body-container");
        if (element != null) {
            Elements childs = element.children();
            String token = null;
            if (childs != null && childs.size() > 0) {
                for (Element el : childs) {
                    for (DataNode node : el.dataNodes()) {
                        String data = node.getWholeData();
                        if (data != null && data.contains("phoneToken")) {
                            int from = data.indexOf("phoneToken");
                            token = data.substring(from + 12).trim();
                            if (token.endsWith(";"))
                                token = token.substring(1, token.length() - 2);
                            break;
                        }
                    }
                }
            }

            if (token != null) {
                String id = getPhoneId(doc);
                if (id != null) {
                    return downloadPhone(id, token, cookies);
                }
            }
        }
        return null;
    }

    private String getPhoneId(Document doc) {
        Element element = doc.getElementById("contact_methods");
        if (element != null) {
            Elements childs = element.getElementsByClass("contact-button");
            if (childs != null) {
                for (Element el : childs) {
                    Attributes attributes = el.attributes();
                    if (attributes != null && attributes.size() > 0) {
                        for (Attribute a : attributes) {
                            if (a.getKey().equals("class")) {
                                String val = a.getValue();
                                if (val.contains("link-phone")) {
                                    int from = val.indexOf("{");
                                    int to = val.indexOf("}");
                                    if (from > 0 && to > 0) {
                                        String json = val.substring(from, to + 1);
                                        if (!json.equals("")) {
                                            String id = parseJson(json, "id");
                                            if (id != null)
                                                return id;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    private String parseJson(String json, String key) {
        try {
            JSONObject pars = new JSONObject(json);
            return pars.getString(key);
        } catch (JSONException e) {
            return null;
        }
    }

    private String downloadPhone(String id, String token, Map<String, String> cookies) {
        String urlPhone = "https://www.olx.ua/ajax/misc/contact/phone/" + id + "/?pt=" + token;
        try {
            Document doc = Jsoup.connect(urlPhone)
                    .ignoreContentType(true)
                    .cookies(cookies)
                    .header("Accept", "*/*")
                    .header("Accept-Encoding", "gzip, deflate, br")
                    .header("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
                    .header("Connection", "keep-alive")
                    .header("Host", "www.olx.ua")
                    .header("Referer", getReferer())
                    .sslSocketFactory(new TLSSocketFactory()).ignoreHttpErrors(true)
                    .get();
            if (doc != null) {
                String header = doc.html();
                if (!header.equals("")) {
                    return getBody(doc.getAllElements());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getReferer() {
        int index = uri.indexOf(".html");
        return uri.substring(0, index + 5);
    }

    private String getBody(Elements elements) {
        if (elements.size() > 0) {
            for (int i = 0; i < elements.size(); i++) {
                String txt = elements.get(i).tag().getName();
                if (txt.startsWith("body")) {
                    String body = elements.get(i).text();
                    body = parseJson(body, "value");
                    StringBuilder builder = new StringBuilder();
                    if (body != null) {
                        String[] s = body.split("</span>");
                        for (String str : s) {
                            if (str.trim().equals("")) continue;
                            builder.append("<div><span>")
                                    .append("Телефон: ")
                                    .append(str.trim()).append("</span></div>");
                        }
                    }
                    return builder.toString();
                }
                String body = getBody(elements.get(i).children());
                if (body != null && !body.equals("")) return body;
            }
        }
        return null;
    }
}
