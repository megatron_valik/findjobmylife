package com.megatron.findjobmylife.parser.vacancy;

import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserAbstract;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class VacancyRabotaUa extends ParserAbstract {

    @Override
    public VacancyModel getVacancy() throws Exception {
        return getVacancy(uri);
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() {
        return null;
    }

    private VacancyModel getVacancy(String uri) throws Exception {
        try {
            Document doc = Jsoup.connect(uri).userAgent("Mozilla").get();

            Elements eMap = doc.getElementsByClass("f-text-gray");
            if (eMap != null && eMap.first() != null) eMap.first().remove();
            eMap = doc.getElementsByClass("show_vac_map_popup");
            if (eMap != null && eMap.first() != null) eMap.first().remove();

            Elements ePhone = doc.getElementsByClass("hiddencontact");
            if (ePhone != null && ePhone.first() != null)
                ePhone.first().remove();

            Elements card = doc.getElementsByClass("d_content");
            String idVacancy = uri + "?mode=apply#apply";

            if (card.size() == 0) {
                card = doc.getElementsByClass("f-vacancy-inner-wrapper");
            }

            String pp = card.html();
            if (pp == null || pp.equals("")) {
                pp = doc.getElementsByClass("vacancy-ssr-holder").html();
            }

            return new VacancyModel("", "", "", null, "", pp, idVacancy);
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        } catch (OutOfMemoryError e) {
            throw new Exception(e.getMessage());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}
