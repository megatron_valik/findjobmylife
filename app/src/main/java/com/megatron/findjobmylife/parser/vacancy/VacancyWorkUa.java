package com.megatron.findjobmylife.parser.vacancy;

import android.text.Html;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.megatron.findjobmylife.models.VacancyModel;
import com.megatron.findjobmylife.parser.ParserAbstract;
import com.megatron.findjobmylife.utils.Utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class VacancyWorkUa extends ParserAbstract {

    @Override
    public VacancyModel getVacancy() throws Exception {
        return getVacancy(uri);
    }

    @Override
    public ArrayList<VacancyModel> getVacancyList() {
        return null;
    }

    private VacancyModel getVacancy(String uri) throws Exception {
        String siteUri = "https://" + uri;
        try {
            Document doc = Jsoup.connect(siteUri).get();
            doc.getElementsByClass("form-group hidden-print").first().remove();

            Elements map = doc.getElementsByClass("link-toggle");
            if (map != null && map.first() != null)
                map.first().remove();

            Elements card = doc.getElementsByClass("card wordwrap");
            String idVacancy = "https://www.work.ua/jobseeker/my/resumes/send/?id=";
            Elements id = doc.getElementsByClass("form-group hidden-print");
            if (id.size() == 0) {
                id = doc.getElementsByClass("list-inline");
                if (id.size() == 0)
                    id = doc.getElementsByClass("saved-jobs pull-left");
            }
            if (id.size() >= 1) {
                idVacancy += getIdVacancy(id);
            }
            getPhone(doc);
            getPhoneBottom(doc);

            //Log.v("VacancyWUa", idVacancy);
            String pp = card.html();
            return new VacancyModel("", "", "", null, "", pp, idVacancy);
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        } catch (OutOfMemoryError e) {
            throw new Exception(e.getMessage());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private String getIdVacancy(Elements childs) {
        if (childs != null && childs.size() > 0) {
            for (int i = 0; i < childs.size(); i++) {
                Attributes attr = childs.get(i).attributes();
                for (Attribute a : attr) {
                    if (a.getKey().equals("data-id"))
                        return a.getValue();
                }
                String str = getIdVacancy(childs.get(i).children());
                if (!str.equals(""))
                    return str;
            }
        }
        return "";
    }

    //телефон в описании
    private void getPhone(Document elements) {
        try {
            Elements select = elements.getElementsByClass("text-indent add-top-sm");
            int size = select.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    Elements childs = select.get(i).children();
                    int childSize = childs.size();
                    for (int j = 0; j < childSize; j++) {
                        Attributes attr = childs.get(j).attributes();
                        for (Attribute a : attr) {
                            if (a.getKey().equals("id") && a.getValue().equals("contact-phone")) {
                                Element child = select.get(i).child(j);
                                Elements ph = child.children();
                                int phSize = ph.size();
                                if (phSize > 0) {
                                    Element el = ph.get(0);
                                    PhoneExample phoneExample = getPhone(elements.baseUri());
                                    if (phoneExample != null && phoneExample.getContactPhone() != null)
                                        el.append(phoneExample.getContactPhone());
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //телефон после описания
    private void getPhoneBottom(Document elements) {
        try {
            Elements select = elements.getElementsByClass("link-phone nowrap js-get-phone");
            int size = select.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    Elements childs = select.get(i).children();
                    int childSize = childs.size();
                    for (int j = 0; j < childSize; j++) {
                        Attributes attr = childs.get(j).attributes();
                        for (Attribute a : attr) {
                            if (a.getKey().equals("class") && a.getValue().equals("glyphicon glyphicon-phone")) {
                                PhoneExample phoneExample = getPhone(elements.baseUri());
                                if (phoneExample != null) {
                                    Element discription = elements.getElementById("job-description");
                                    if (discription != null) {
                                        discription.replaceWith( // замена содержимого...
                                                new Element(Tag.valueOf("div"), "")
                                                        .append(phoneExample.getDescription()));
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // загружаем данные из скрипта
    private PhoneExample getPhone(String uri) throws Exception {
        String postData = uri + "ajax/get-jobs-data/"; // скрипт
        URL url = new URL(postData);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Content-Length", String.valueOf(postData.length()));
        // Write data
        OutputStream os = connection.getOutputStream();
        os.write(postData.getBytes());
        // Read response
        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        StringBuilder json = new StringBuilder();
        String r;
        while ((r = br.readLine()) != null) { // получаем данные
            json.append(r);
        }
        connection.disconnect();
        if (!json.toString().equals("")) {
            return parsePhone(Utils.removeUTFCharacters(json.toString()).toString());
        }
        return null;
    }

    // парсим данные телефона и описании
    private PhoneExample parsePhone(String json) {
        if (json == null || json.trim().equals("")) return null;
        try {
            PhoneExample phoneMobile = null; // ищем телефон
            int phonePos = json.indexOf("contactPhone");
            if (phonePos > 0) {
                String json2 = Html.fromHtml(json).toString();
                phoneMobile = new Gson().fromJson(json2, PhoneExample.class);
            }
            // ищем описание - замена текущему без телефона
            PhoneExample phone = new PhoneExample();
            phone.setDescription(getDescription(json));
            if (phoneMobile != null && phoneMobile.getContactPhone() != null)
                phone.setContactPhone(phoneMobile.getContactPhone());

            return phone;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class PhoneExample {
        @SerializedName("contactPhone")
        @Expose
        private String contactPhone;
        @SerializedName("description")
        @Expose
        private String description;

        String getContactPhone() {
            return contactPhone;
        }

        void setContactPhone(String contactPhone) {
            this.contactPhone = contactPhone;
        }

        String getDescription() {
            return description;
        }

        void setDescription(String description) {
            this.description = description;
        }
    }

    private String getDescription(String json) {
        int descriptionPos = json.indexOf("description");
        if (descriptionPos > 0) {
            String res = json.substring(descriptionPos + 14, json.length() - 2);
            return res.replace("\\r\\n", "").replace("\\", "");
        }
        return json;
    }
}
