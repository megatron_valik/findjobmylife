package com.megatron.findjobmylife.rating;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.utils.Utils;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class RatingHelper {
    private static final String PREFS_NAME = "findjobs";
    private static final String KEY_INT_LAUNCH_COUNT = "launch_count";
    private static final String KEY_BOOL_ASKED = "asked";
    private static final String KEY_LONG_LAST_DATE = "last_date";

    public interface RatingListener {
        void onClose();
    }

    private Context context;
    private RatingListener listener;
    private final SharedPreferences mPrefs;

    public RatingHelper(Context context, @NonNull RatingListener listener) {
        this.context = context;
        this.listener = listener;
        mPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void show() {
        // не спрашивать больше
        boolean noAsked = mPrefs.getBoolean(KEY_BOOL_ASKED, false);
        if (noAsked) {
            listener.onClose();
            return;
        }

        SharedPreferences.Editor editor = mPrefs.edit();
        // последний день открытия программы
        long lastDay = mPrefs.getLong(KEY_LONG_LAST_DATE, -1);
        // к-во открытий программы
        int countOpened = mPrefs.getInt(KEY_INT_LAUNCH_COUNT, 0);
        boolean showDialog = false;
        // первый раз запустили...
        if (lastDay == -1) {
            // дата запуска
            editor.putLong(KEY_LONG_LAST_DATE, System.currentTimeMillis());
            // к-во запусков
            editor.putInt(KEY_INT_LAUNCH_COUNT, 1);
        } else {
            int day = countDayRelay(lastDay);

            // 2 дня не отображаем
            if (day >= 3 && countOpened >= 3) {
                // на третий день отображаем
                editor.putInt(KEY_INT_LAUNCH_COUNT, 0);
                editor.putLong(KEY_LONG_LAST_DATE, System.currentTimeMillis());
                showDialog = true;
            } else if (day >= 3)
                editor.putInt(KEY_INT_LAUNCH_COUNT, countOpened + 1);
        }

        editor.apply();
        if (showDialog) {
            try {
                showRatingDialog();
            } catch (Exception e) {
                Utils.logException(e);
            }
        } else listener.onClose();
    }

    private int countDayRelay(long lastDay) {
        long msDiff = Calendar.getInstance().getTimeInMillis() - lastDay;
        long daysDiff = TimeUnit.MILLISECONDS.toDays(msDiff);
        return (int) daysDiff;
    }

    private void showNever(boolean show) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(KEY_BOOL_ASKED, show);
        editor.apply();
    }

    @NonNull
    private static Intent getStoreIntent() {
        final Uri uri = Uri.parse("market://details?id=com.megatron.findjobmylife");
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public static void openPlayStore(Context context) {
        final Intent intent = getStoreIntent();
        if (!(context instanceof Activity)) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        try {
            context.startActivity(intent);
        } catch (Exception e) {
            //
        }
    }

    private void showRatingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        @SuppressLint("InflateParams")
        View v = LayoutInflater.from(context).inflate(R.layout.rating_dialog, null);
        CheckBox checkNeverShow = v.findViewById(R.id.cbNeverShow);

        builder.setView(v)
                .setIcon(R.drawable.star)
                .setTitle(context.getString(R.string.rating_app_title))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.rating_btn_title), (dialog, which) -> {
                    showNever(checkNeverShow.isChecked());
                    openPlayStore(context);
                    dialog.dismiss();
                    listener.onClose();
                })
                .setNegativeButton(context.getString(R.string.rating_app_cancel_title), (dialog, which) -> {
                    showNever(checkNeverShow.isChecked());
                    dialog.dismiss();
                    listener.onClose();
                });

        builder.create().show();
    }
}
