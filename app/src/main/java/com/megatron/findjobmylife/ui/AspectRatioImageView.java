package com.megatron.findjobmylife.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.megatron.findjobmylife.R;

public class AspectRatioImageView extends androidx.appcompat.widget.AppCompatImageView {
    //private static final String TAG ="AspectRatioImageView";
    private static final float DEFAULT_ASPECT_RATIO = 1.5f;
    private final float mAspectRatio;

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        mAspectRatio = a.getFloat(R.styleable.AspectRatioImageView_aspect_ratio, DEFAULT_ASPECT_RATIO);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int newWidth = getMeasuredWidth();
        int newHeight = (int) (newWidth / mAspectRatio);

        setMeasuredDimension(newWidth, newHeight);
    }
}
