package com.megatron.findjobmylife.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.megatron.findjobmylife.App;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.dialogs.PhoneDialogHelper;
import com.megatron.findjobmylife.utils.ToastHelper;
import com.megatron.findjobmylife.utils.Utils;

public class HtmlTextView extends AppCompatTextView implements Html.ImageGetter, TextViewClickMovement.OnTextViewClickMovementListener {
    private static final int W = 128;
    private static final int H = 96;

    public interface HtmlTextViewCallListener {
        void onCall(String phone);
    }

    private HtmlTextViewCallListener callListener;

    public HtmlTextView(Context context) {
        super(context);
    }

    public HtmlTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HtmlTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void onClickInit(Context context) {
        setMovementMethod(new TextViewClickMovement(this, context));
    }

    public void setCallListener(HtmlTextViewCallListener callListener) {
        this.callListener = callListener;
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        Spanned string;
        String txt = text.toString()
                .replace("<dt>", "<p>")
                .replace("<dd>", "")
                .replace("</dt>", "")
                .replace("</dd>", "</p>")
                .replace("</li>", "</li><br>")
                .replace("<i class=\"fi-ok\"></i>", "<i class=\"fi-ok\"></i>&#10003 ")
                .replace("<li><p>", "<li>")
                .replace("</p></li>", "</li>")
                .replace("<li> <p>", "<li>")
                .replace("</p> </li>", "</li>");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txt = txt.replace("<li>", "<li>  ");
            string = (Html.fromHtml(txt, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV, this, null));
        } else {
            txt = txt.replace("<li>", "<li>&#8226 ");
            string = (Html.fromHtml(txt, this, null));
        }
        super.setText(string, type);
        onClickInit(this.getContext());
    }

    @Override
    public Drawable getDrawable(String source) {
        if (source != null && source.endsWith(".svg"))
            return getResources().getDrawable(R.drawable.empty);

        String s = source;
        if (s != null && s.startsWith("//")) {
            s = "https:" + s;
        }
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.drawable.empty);
        d.addLevel(0, 0, empty);

        int[] screenSize = Utils.screenSize(getContext());
        int wMin = Math.min(screenSize[0], screenSize[1]);
        int hMin = wMin / 5 / 2;

        int width = Math.min(wMin / 5, W);
        int height = Math.min(hMin, H);
        d.setBounds(0, 0, width, height);

        try {
            loadImage(s, d);
        } catch (Throwable e) {
            Utils.logException(e);
        }
        return d;
    }

    private void loadImage(String url, LevelListDrawable mDrawable) {
        Glide.with(App.getContext())
                .asBitmap()
                .load(url)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap loadedImage, Transition<? super Bitmap> transition) {
                        BitmapDrawable d;
                        int width = loadedImage.getWidth();
                        int height = loadedImage.getHeight();

                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(loadedImage, width, height, false);
                        resizedBitmap = resize(resizedBitmap, W, H);

                        d = new BitmapDrawable(resizedBitmap);
                        mDrawable.addLevel(1, 1, d);
//                            mDrawable.setBounds(0, 0, W, H);
                        mDrawable.setBounds(0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight());
                        mDrawable.setLevel(1);
                        invalidate();
                    }
                });
    }

    @Override
    public void onLinkClicked(String linkText, TextViewClickMovement.LinkType linkType, Context context) {
        switch (linkType) {
            case PHONE:
                new PhoneDialogHelper(linkText, context, callListener).show();
                break;
            case WEB_URL:
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(linkText));
                    context.startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                    ToastHelper.show(context.getString(R.string.error_open_http));
                }
                break;
            case EMAIL_ADDRESS:
                try {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", linkText, null));
                    context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
                } catch (Exception e) {
                    e.printStackTrace();
                    ToastHelper.show(context.getString(R.string.error_open_email));
                }
                break;
        }
    }

    @Override
    public void onLongClick(String text) {

    }

    private Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }
}
