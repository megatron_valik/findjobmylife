package com.megatron.findjobmylife.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;

import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.utils.Utils;

import java.util.Arrays;

public class MultiSelectSpinner extends AppCompatSpinner {
    public interface SpinnerChange {
        void onChangeList(boolean[] selectedItems);
    }

    private String[] mItems = null;
    private boolean[] mSelection = null;
    private ArrayAdapter<String> proxyAdapter;

    private SpinnerChange changeListener;
    private boolean isFirstActiveIfEmpty = false; // флаг что нужно установить первый флаг всегда активным если выбран другой в списке
    private AlertDialog dialog;

    public MultiSelectSpinner(Context context) {
        super(context);

//        proxyAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
//        super.setAdapter(proxyAdapter);
        initAdapter(context);
    }

    public MultiSelectSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);

//        proxyAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
//        super.setAdapter(proxyAdapter);
        initAdapter(context);
    }

    private void initAdapter(Context context) {
        proxyAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
        super.setAdapter(proxyAdapter);
    }

    public void setChangeListener(SpinnerChange changeListener) {
        this.changeListener = changeListener;
    }

    public void setFirstActiveIfEmpty(boolean firstActiveIfEmpty) {
        isFirstActiveIfEmpty = firstActiveIfEmpty;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean performClick() {
        if (mItems == null || mSelection == null || proxyAdapter == null) return false;
        if (dialog != null) {
            dialog.dismiss();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(mItems, mSelection, (dialog, which, isChecked) -> {
            try {
                if (mSelection != null && which < mSelection.length) {
                    if (isFirstActiveIfEmpty) { // если нужно активировать первую запись
                        if (which == 0) { // выделили первый элемент
                            mSelection[0] = true;
                            ((AlertDialog) dialog).getListView().setItemChecked(0, true);
                            for (int i = 1; i < mSelection.length; i++) {
                                mSelection[i] = false; // убираем все остальные
                                ((AlertDialog) dialog).getListView().setItemChecked(i, false);
                            }
                        } else { // выбрали другой элемент
                            mSelection[0] = false; // снимаем с главного галочку
                            ((AlertDialog) dialog).getListView().setItemChecked(0, false);
                            for (int i = 1; i < mSelection.length; i++) {
                                if (i == which)
                                    mSelection[i] = isChecked;
                            }
                        }
                        // проверка что есть хоть один активный элемент
                        boolean isCheck = false;
                        for (boolean aMSelection : mSelection) {
                            if (aMSelection) {
                                isCheck = true; // есть
                                break;
                            }
                        }
                        if (!isCheck) { // если нет - все галочки сняты
                            mSelection[0] = true; // ставим активный первый
                            ((AlertDialog) dialog).getListView().setItemChecked(0, true);
                            for (int i = 1; i < mSelection.length; i++) {
                                mSelection[i] = false;
                                ((AlertDialog) dialog).getListView().setItemChecked(i, false);
                            }
                        }
                    } else
                        mSelection[which] = isChecked;

                    proxyAdapter.clear();
                    proxyAdapter.add(buildSelectedItemString());
                    if (changeListener != null)
                        changeListener.onChangeList(mSelection);
                } else {
                    throw new IllegalArgumentException("Argument 'which' is out of bounds.");
                }
            } catch (Exception e) {
                Utils.logException(e);
            }
        }).setPositiveButton(R.string.submit_checked, (d, which) -> d.dismiss());
        dialog = builder.show();
        return true;
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException("setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(String[] items) {
        mItems = items;
        if (mItems == null || proxyAdapter == null) return;

        mSelection = new boolean[mItems.length];

        Arrays.fill(mSelection, false); // true defaults to checked, false defaults to unchecked
        proxyAdapter.clear();
        proxyAdapter.add(buildSelectedItemString());
    }

    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        if (mItems != null)
            for (int i = 0; i < mItems.length; ++i) {
                if (mSelection[i]) {
                    if (foundOne) {
                        sb.append(", ");
                    }
                    foundOne = true;

                    sb.append(mItems[i]);
                }
            }

        return sb.toString();
    }

    public void setSelection(boolean[] selection) {
        if (selection == null || selection.length == 0) return;
        mSelection = selection;
        if (proxyAdapter != null) {
            proxyAdapter.clear();
            proxyAdapter.add(buildSelectedItemString());
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (dialog != null) {
            dialog.dismiss();
        }
        super.onDetachedFromWindow();
    }
}
