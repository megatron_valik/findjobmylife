package com.megatron.findjobmylife.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.megatron.findjobmylife.R;

public class MyAutoCompleteTextView extends AppCompatAutoCompleteTextView {
    public MyAutoCompleteTextView(Context context) {
        super(context);
        init(context);
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void init(Context context) {
        Drawable drawable = ContextCompat.getDrawable(context, R.mipmap.ic_menu_close_clear_cancel);
        if (drawable != null) {
            int delW = drawable.getIntrinsicWidth() / 3;
            int delH = drawable.getIntrinsicHeight() / 3;

            drawable.setBounds(0, 0, drawable.getIntrinsicWidth() - delW,
                    drawable.getIntrinsicHeight() - delH);
            setCompoundDrawables(null, null, drawable, null);

            setOnTouchListener((v, event) -> {
                int x = (int) event.getX();
                int y = (int) event.getY();
                int left = getWidth() - getPaddingRight() - drawable.getIntrinsicWidth();
                int right = getWidth();
                boolean tappedX = x >= left && x <= right && y >= 0 && y <= (getBottom() - getTop());
                if (tappedX) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        setText("");
                    }
                    return true;
                }
                return false;
            });
        }
    }

}
