package com.megatron.findjobmylife.ui;

import android.content.Context;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Patterns;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.TextView;

import com.megatron.findjobmylife.utils.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextViewClickMovement extends LinkMovementMethod {
    private final OnTextViewClickMovementListener mListener;
    private final GestureDetector mGestureDetector;
    private TextView mWidget;
    private Spannable mBuffer;
    private Context context;

    public enum LinkType {

        /**
         * Indicates that phone link was clicked
         */
        PHONE,

        /**
         * Identifies that URL was clicked
         */
        WEB_URL,

        /**
         * Identifies that Email Address was clicked
         */
        EMAIL_ADDRESS,

        /**
         * Indicates that none of above mentioned were clicked
         */
        NONE
    }

    /**
     * Interface used to handle Long clicks on the {@link TextView} and taps
     * on the phone, web, mail links inside of {@link TextView}.
     */
    public interface OnTextViewClickMovementListener {

        /**
         * This method will be invoked when user press and hold
         * finger on the {@link TextView}
         *
         * @param linkText Text which contains link on which user presses.
         * @param linkType Type of the link can be one of {@link LinkType} enumeration
         */
        void onLinkClicked(final String linkText, final LinkType linkType, Context context);

        /**
         * @param text Whole text of {@link TextView}
         */
        void onLongClick(final String text);
    }

    TextViewClickMovement(final OnTextViewClickMovementListener listener, final Context context) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new SimpleOnGestureListener());
        this.context = context;
    }

    @Override
    public boolean onTouchEvent(final TextView widget, final Spannable buffer, final MotionEvent event) {
        mWidget = widget;
        mBuffer = buffer;
        mGestureDetector.onTouchEvent(event);

        return false;
    }

    /**
     * Detects various gestures and events.
     * Notify users when a particular motion event has occurred.
     */
    class SimpleOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent event) {
            // Notified when a tap occurs.
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            // Notified when a long press occurs.
            final String text = mBuffer.toString();

            if (mListener != null) {
                mListener.onLongClick(text);
            }
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent event) {
            // Notified when tap occurs.
            String linkText = getLinkText(mWidget, mBuffer, event);

            LinkType linkType = LinkType.NONE;
            if (Patterns.WEB_URL.matcher(linkText).matches() && linkText.startsWith("http")) {
                linkType = LinkType.WEB_URL;
            } else if (Patterns.PHONE.matcher(linkText).matches()) {
                linkType = LinkType.PHONE;
            } else if (Patterns.EMAIL_ADDRESS.matcher(linkText).matches()) {
                linkType = LinkType.EMAIL_ADDRESS;
            } else {
                String phone = matchPhone(linkText);
                if (!phone.equals("") && phone.length() >= 10
                    //&& phone.length() <= 13
                    //&& Patterns.PHONE.matcher(phone).matches()
                ) {
                    linkText = phone;
                    linkType = LinkType.PHONE;
                }
            }

            if (mListener != null) {
                mListener.onLinkClicked(linkText, linkType, context);
            }

            return false;
        }

        private String getLinkText(final TextView widget, final Spannable buffer, final MotionEvent event) {
            int x = (int) event.getX();
            int y = (int) event.getY();

            x -= widget.getTotalPaddingLeft();
            y -= widget.getTotalPaddingTop();

            x += widget.getScrollX();
            y += widget.getScrollY();

            Layout layout = widget.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

//            ClickableSpan[] link = buffer.getSpans(off, off, ClickableSpan.class);
//            if (link.length != 0) {
//                link[0].onClick(widget);
//                ClickableSpan start = link[0];
//                return buffer.subSequence(buffer.getSpanStart(start),
//                        buffer.getSpanEnd(start)).toString();
//            }
            SpannableStringBuilder strBuilder = new SpannableStringBuilder(buffer);
            URLSpan[] link = strBuilder.getSpans(off, off, URLSpan.class);
            if (link.length != 0) {
                return link[0].getURL();
            }

            String[] lines = new String[widget.getLineCount()];
            String text = widget.getText().toString();
            int start = 0;
            int end;
            for (int i = 0; i < widget.getLineCount(); i++) {
                end = layout.getLineEnd(i);
                lines[i] = text.substring(start, end);
                start = end;
            }

            try {
                return lines[line].replace("\n", "").trim();
            } catch (Exception e) {
                return "";
            }
        }
    }

    private String matchPhone(String linkText) {
        StringBuilder sb = new StringBuilder();
        try {
            String[] m = linkText.replaceAll("\\s+", "").split(","); // work.ua
            String[] mm = linkText.replaceAll("\\s+", "").split(";");//rabota.ua

            if (mm.length > m.length) {
                m = mm;
            }

            Pattern pat = Pattern.compile("[-]?[0-9]+(.[0-9]+)?");
//            Pattern patPhone = Pattern.compile("^(\\s*)?(\\+)?([- _():=+]?\\d[- _():=+]?){10,14}(\\s*)?$");
//            Patterns.PHONE.matcher(phone).matches();
            for (String str : m) {
                String s = str.replaceAll("-", "")
                        .replaceAll("\\s+", "")
                        .replaceAll("\\D+", "")
                        .replace("(", "")
                        .replace(")", "");

                Matcher matcher = pat.matcher(s);
                if (matcher.find()) {
                    sb.append(matcher.group());
//                    String phone = sb.toString();
//                    patPhone.matcher(phone);
                    Matcher mPhone = Patterns.PHONE.matcher(s);
                    if (mPhone.find() && mPhone.matches() && s.length() > 9)
                        sb.append(",");
                }
            }
        } catch (Exception e) {
            Utils.logException(e);
        }
        return sb.toString();
    }

}