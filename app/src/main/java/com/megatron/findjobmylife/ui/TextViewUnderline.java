package com.megatron.findjobmylife.ui;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;

public class TextViewUnderline extends androidx.appcompat.widget.AppCompatTextView {
    public TextViewUnderline(Context context) {
        super(context);
    }

    public TextViewUnderline(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewUnderline(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(), 0);

        super.setText(content, type);
    }
}
