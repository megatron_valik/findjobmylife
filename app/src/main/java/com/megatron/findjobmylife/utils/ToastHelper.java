package com.megatron.findjobmylife.utils;

import android.content.Context;
import com.google.android.material.snackbar.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.megatron.findjobmylife.App;
import com.megatron.findjobmylife.R;

public class ToastHelper {

    public static void show(String msg) {
        LayoutInflater inflater = (LayoutInflater) App.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View layout = inflater.inflate(R.layout.toast_root, null);
        TextView text = layout.findViewById(R.id.txt_toast);
        text.setText(msg);

        Toast toast = new Toast(App.getContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static void show(View view, String msg) {
        if (view == null)
            show(msg);
        else
            Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
    }
}
