package com.megatron.findjobmylife.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import androidx.core.app.ActivityCompat;
import androidx.databinding.BindingAdapter;

import com.crashlytics.android.Crashlytics;
import com.megatron.findjobmylife.App;
import com.megatron.findjobmylife.R;
import com.megatron.findjobmylife.db.utils.DbUtils;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;

public class Utils {
    public final static String UTF = "UTF-8";

    private static final String PREFERENCES_NAME = "FindJob";
    public static final String PREFERENCES_VACANCY_NAME = "VACANCY";
    public static final String PREFERENCES_CITY_NAME = "CITY";
    public static final String PREFERENCES_CITY_WORK_ID = "CITY_WORK_ID";
    public static final String PREFERENCES_CITY_RABOTA_ID = "CITY_RABOTA_ID";
    public static final String PREFERENCES_CITY_HH_ID = "CITY_HH_ID";
    public static final String PREFERENCES_CITY_OLX_ID = "CITY_OLX_ID";
    public static final String PREFERENCES_CITY_JOBS_ID = "CITY_JOBS_ID";

    public static final String SEARCH_WORK_UA = "SEARCH_WORK_UA";
    public static final String SEARCH_RABOTA_UA = "SEARCH_RABOTA_UA";
    public static final String SEARCH_HH_UA = "SEARCH_HH_UA";
    public static final String SEARCH_OLX_UA = "SEARCH_OLX_UA";
    public static final String SEARCH_JOBS_UA = "SEARCH_JOBS_UA";

    public static final String ANIMATE_LIST = "ANIMATE_LIST";

    public static final int PERMISSION_REQUEST = 1;
    private static final String[] PERMISSIONS = {
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CALL_PHONE
    };

    public static void savePreference(String key, String value) {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(
                PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String readPreference(String key) {
        SharedPreferences sharedPreferences = App.getContext().getSharedPreferences(
                PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    public static boolean stringToBool(String param, boolean defaultValue) {
        try {
            if (param == null || param.trim().equals("")) return defaultValue;
            return Boolean.parseBoolean(param);
        } catch (Exception e) {
            return false;
        }
    }

    public static void hideKeyboard(@Nullable Activity activity) {
        if (activity == null) return;
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Boolean isWiFiConnect(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public static void animateDownloadProgress(Context context, View view, boolean show) {
        Animation animation = AnimationUtils.loadAnimation(context, show ? R.anim.slide_up : R.anim.slide_down);
//        Log.v("Utils", "animateProgress = " + show);
        try {
            if (view != null && animation != null) {
                view.startAnimation(animation);
//                Log.v("Utils", "animateProgress...");
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Log.v("Utils", "animateProgress = " + e.getMessage());
        }
//        Log.v("Utils", "animateProgress complete");
    }

    public static void clearCash() {
        DbUtils.clearCash();
        ToastHelper.show(App.getContext().getString(R.string.success_clear_cash));
    }

    public static void clearFavorite() {
        DbUtils.clearFavorite();
        ToastHelper.show(App.getContext().getString(R.string.success_clear_cash));
    }

    public static void clearFastSearch() {
        DbUtils.clearFastSearch();
        ToastHelper.show(App.getContext().getString(R.string.success_clear_cash));
    }

    public static void clearRemovedVacancy() {
        DbUtils.clearRemovedCash();
        ToastHelper.show(App.getContext().getString(R.string.success_clear_cash));
    }

    public static Bitmap makeTransparent(Bitmap bit) {
        if (bit == null) return null;
        try {
            int width = bit.getWidth();
            int height = bit.getHeight();
            Bitmap myBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            int[] allPixels = new int[myBitmap.getHeight() * myBitmap.getWidth()];
            bit.getPixels(allPixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
            myBitmap.setPixels(allPixels, 0, width, 0, 0, width, height);

//            int colorTransparent = Math.abs(App.getContext().getResources().getColor(R.color.white_transparent));
            for (int i = 0; i < myBitmap.getHeight() * myBitmap.getWidth(); i++) {
//                if (allPixels[i] == Color.WHITE)
                if (Math.abs(allPixels[i]) <= 5)//40500
                    allPixels[i] = Color.alpha(Color.TRANSPARENT);
            }

            myBitmap.setPixels(allPixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
            return myBitmap;
        } catch (OutOfMemoryError e) {
            return bit;
        } catch (Exception e) {
            return bit;
        }
    }

    public static int strToInt(String str) {
        if (str == null || str.trim().equals("")) return 0;
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return 0;
        }
    }

    @BindingAdapter("android:layout_marginBottom")
    public static void setBottomMargin(View view, float bottomMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin,
                layoutParams.rightMargin, Math.round(bottomMargin));
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("android:layout_marginTop")
    public static void setTopMargin(View view, float topMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, Math.round(topMargin), layoutParams.rightMargin, layoutParams.bottomMargin);
        view.setLayoutParams(layoutParams);
    }

    public static int[] screenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        return new int[]{width, height};
    }

    public static StringBuffer removeUTFCharacters(String data) {
        Pattern p = Pattern.compile("\\\\u(\\p{XDigit}{4})");
        Matcher m = p.matcher(data);
        StringBuffer buf = new StringBuffer(data.length());
        while (m.find()) {
            String ch = String.valueOf((char) Integer.parseInt(m.group(1), 16));
            m.appendReplacement(buf, Matcher.quoteReplacement(ch));
        }
        m.appendTail(buf);
        return buf;
    }

    public static void logException(Throwable e) {
        Crashlytics.logException(e);
//        e.printStackTrace();
    }

    public static String getParameters(String... params) {
        if (params == null || params.length == 0) return "";
        StringBuilder builder = new StringBuilder();
        int pos = 0;
        for (String param : params) {
            if (param.equals("")) continue;
            if (pos == 0)
                builder.append("?").append(param);
            else builder.append("&").append(param);
            pos++;
        }
        return builder.toString();
    }

    public static Boolean hasPermission(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context instanceof Activity) {
                for (String permission : PERMISSIONS) {
                    if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, PERMISSION_REQUEST);
                        return false;
                    }
                }
            } else return false;
        }
        return true;
    }

    public static void enableSSLSocket() throws KeyManagementException, NoSuchAlgorithmException {
        HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);

        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, new X509TrustManager[]{new X509TrustManager() {
            @SuppressLint("TrustAllX509TrustManager")
            public void checkClientTrusted(X509Certificate[] chain, String authType) {
                //
            }

            @SuppressLint("TrustAllX509TrustManager")
            public void checkServerTrusted(X509Certificate[] chain, String authType) {
                //
            }

            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }}, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
    }

}
